name = "NMIH"
path = "mod/NMIH"
user_dir = "NMIH"
replace_path = "history/characters"
replace_path = "history/titles"
replace_path = "history/provinces"
replace_path = "history/wars"
replace_path = "common/dynasties"
replace_path = "common/landed_titles"
replace_path = "common/trade_routes"
replace_path = "events"
#replace_path = "decisions"
tags=
{
Map History Japan }
picture="nmihtitle.jpg"