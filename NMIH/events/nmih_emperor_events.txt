###########################################
#                                         #
# NMIH Emperor and Imperial Court events  #
#                                         #
###########################################

namespace = EMPEROR

# If Emperor has no child, he gets a new consort
character_event = {
	id = EMPEROR.1
	desc = "EVTDESCEMPEROR.1"

	is_triggered_only = yes
	hide_window = yes
	
	trigger = {
		ai = yes
		age = 16
		is_female = no
		primary_title = { title = e_tenno }
		NOT = {
			any_child = {
				is_female = no
				has_inheritance_blocker = no
				NOT = { trait = incapable }
				NOT = { trait = imbecile }
				NOT = { trait = inbred }
				NOT = { trait = lunatic }
				NOT = { trait = possessed }
				NOT = { trait = slow }
				NOT = { trait = weak }
				NOT = { trait = bastard }
				NOT = { trait = kinslayer }
				NOT = { trait = eunuch }
				NOT = { trait = blinded }
			}
		}
		NOT = {
			num_of_consorts = 3
		}
	}

	immediate = {
		if = {
			limit = {
				trait = celibate
			}
			remove_trait = celibate
		}
		create_character = {
			random_traits = yes
			dynasty = culture
			female = yes
			age = 20
			religion = ROOT
			culture = aristocrats
		}
		new_character = {
			add_consort = ROOT
		}
	}
}

# If Emperor has no same dynasty heir and passed, a new imperial family member has appear and successed imperial throne
character_event = {
	id = EMPEROR.2

	is_triggered_only = yes
	hide_window = yes
	
	trigger = {
		has_landed_title = e_tenno
		current_heir = {
			NOT = { dynasty = ROOT }
		}
	}

	immediate = {
		save_event_target_as = previousEmperor
		create_character = {
			random_traits = yes
			age = 16
			culture = imperial_family
			religion = buddhist
			dynasty = 420 #Yamato(imperial family)
			fertility = 2
			historical = yes
			attributes = {
				diplomacy = 9
				martial = 9
				stewardship = 9
				intrigue = 9
				learning = 9
			}
		}
		new_character = {
			dynasty = 420 #Yamato(imperial family)
			save_event_target_as = displaced_prince
			if = {
				limit = {
					event_target:previousEmperor = {
						could_be_parent_of = event_target:displaced_prince
					}
				}
				set_father = event_target:previousEmperor
			}
			else_if = {
				limit = {
					event_target:previousEmperor = {
						father_even_if_dead = {
							could_be_parent_of = event_target:displaced_prince
						}
					}
				}
				event_target:previousEmperor = {
					father_even_if_dead = {
						event_target:displaced_prince = {
							set_father = PREV
						}
					}
				}
			}
			else_if = {
				limit = {
					event_target:previousEmperor = {
						any_dynasty_member_even_if_dead  = {
							has_landed_title = e_tenno
							is_female = no
							could_be_parent_of = event_target:displaced_prince
						}
					}
				}
				event_target:previousEmperor = {
					random_dynasty_member_even_if_dead = {
						limit = {
							has_landed_title = e_tenno
							is_female = no
							could_be_parent_of = event_target:displaced_prince
						}
						event_target:displaced_prince = {
							set_father = PREV
						}
					}
				}
			}
			else_if = {
				limit = {
					event_target:previousEmperor = {
						any_dynasty_member_even_if_dead = {
							is_female = no
							could_be_parent_of = event_target:displaced_prince
						}
					}
				}
				event_target:previousEmperor = {
					random_dynasty_member_even_if_dead = {
						limit = {
							is_female = no
							could_be_parent_of = event_target:displaced_prince
						}
						event_target:displaced_prince = {
							set_father = PREV
						}
					}
				}
			}
			event_target:previousEmperor = {
				abdicate_to = event_target:displaced_prince
			}
		}
	}
}

#Emperor's nickname remove event
character_event = {
	id = EMPEROR.3
	desc = "EVTDESCEMPEROR.3"

	#is_triggered_only = yes
	hide_window = yes
	
	trigger = {
		any_demesne_title = { title = e_tenno }
	}
	
	mean_time_to_happen = {
		months = 3
	}

	immediate = {
		clr_character_flag = court_post
		clr_character_flag = local_post
		remove_nickname = yes
		change_imperial_court_opinion_modifier_effect = yes
		remove_imperial_court_rank_effect = yes
	}
}

###############################
# Leave a Imperial Family Events
###############################
#Notice from Emperor to Target
letter_event = {
	id = EMPEROR.10
	desc = "EVTDESCEMPEROR.10"
	
	is_triggered_only = yes

	option = { #I accept it.
		name = "EVTOPTAEMPEROR.10"
	}
}

#Notice from Target to Emperor
letter_event = {
	id = EMPEROR.11
	desc = "EVTDESCEMPEROR.11"
	
	is_triggered_only = yes

	option = { #I see it.
		name = "EVTOPTAEMPEROR.11"
	}
}


##############################################
#De jure drift between Emperor and Shogun
##############################################
character_event = {
	id = EMPEROR.50

	is_triggered_only = yes
	hide_window = yes
	
	trigger = {
		is_save_game = no
		NOT = { has_global_flag = dejure_drift }
		primary_title = { title = e_tenno }
		NOT = { year = 1192 }
	}

	immediate = {
		set_global_flag = dejure_drift
		if = {
			limit = {
				NOT = { year = 1192 }
			}
			e_japan = {
				any_direct_de_jure_vassal_title = {
					de_jure_liege = e_tenno
				}
			}
		}
		else_if = {
			limit = {
				NOT = { year = 1222 }
			}
			k_northern_kinai = {
				de_jure_liege = e_tenno
			}
			k_southern_kinai = {
				de_jure_liege = e_tenno
			}
			k_sanyo = {
				de_jure_liege = e_tenno
			}
			k_sanin = {
				de_jure_liege = e_tenno
			}
			k_shikoku = {
				de_jure_liege = e_tenno
			}
			k_western_chugoku = {
				de_jure_liege = e_tenno
			}
			k_northern_kyushu = {
				de_jure_liege = e_tenno
			}
			k_southern_kyushu = {
				de_jure_liege = e_tenno
			}
		}
	}
}


###############################
# Choteki Events
###############################
# Issue Choteki
letter_event = {
	id = EMPEROR.100
	desc = "EVTDESCEMPEROR.100"

	is_triggered_only = yes

	option = { #Allright. I will issue a Punishment Imperial decree against him.
		name = "EVTOPTAEMPEROR.100"
		event_target:choteki = {
			add_trait = choteki
			character_event = { id = EMPEROR.104 }
		}
		event_target:request_choteki = {
			letter_event = { id = EMPEROR.105 }
		}
		opinion = {
			who = event_target:request_choteki
			years = 40
			modifier = asked_for_excommunication
		}
		reverse_opinion = {
			who = event_target:request_choteki
			years = 5
			modifier = opinion_glad
		}
	}
	option = { #No. The Imperial decree is not such a light thing.
		name = "EVTOPTBEMPEROR.100"
		ai_chance = {
			factor = 0
		}
		event_target:request_choteki = {
			letter_event = { id = EMPEROR.106 }
		}
		reverse_opinion = {
			who = event_target:request_choteki
			years = 10
			modifier = opinion_disappointed
		}
	}
}

#I has been a Choteki!
character_event = {
	id = EMPEROR.104
	desc = "EVTDESCEMPEROR.104"
	picture = GFX_evt_sunset
	
	is_triggered_only = yes

	option = { #What!?
		name = "EVTOPTAEMPEROR.104"
		piety = -50
		opinion = {
			who = event_target:request_choteki
			years = 100
			modifier = opinion_hate
		}
		#log = "[Root.GetBestName] was became a Choteki at the behest of [request_choteki.GetBestName]!(EMPEROR.104)"
	}
}

#Accepted!
letter_event = {
	id = EMPEROR.105
	desc = "EVTDESCEMPEROR.105"
	
	is_triggered_only = yes

	option = { #I thank your Majesty.
		name = "EVTOPTAEMPEROR.105"
	}
}

#declined!
letter_event = {
	id = EMPEROR.106
	desc = "EVTDESCEMPEROR.106"
	picture = GFX_evt_guardian
	
	is_triggered_only = yes

	option = { #Oh...
		name = "EVTOPTABAKUFU.14"
		prestige = -50
	}
}

# Lift Choteki
letter_event = {
	id = EMPEROR.110
	desc = "EVTDESCEMPEROR.110"

	is_triggered_only = yes

	option = { #Allright. I will issue a repeal the Imperial decree of punishment against him.
		name = "EVTOPTAEMPEROR.110"
		event_target:choteki_kaijo = {
			remove_trait = choteki
			character_event = { id = EMPEROR.114 }
		}
		event_target:request_choteki_kaijo = {
			letter_event = { id = EMPEROR.115 }
		}
		opinion = {
			who = event_target:request_choteki_kaijo
			years = 40
			modifier = asked_to_lift_excommunication
		}
		reverse_opinion = {
			who = event_target:request_choteki_kaijo
			years = 5
			modifier = opinion_glad
		}
	}
	option = { #No. The punishment edict is not such a light thing.
		name = "EVTOPTBEMPEROR.100"
		ai_chance = {
			factor = 0
		}
		FROMFROM = {
			letter_event = { id = EMPEROR.116 }
		}
		reverse_opinion = {
			who = event_target:request_choteki_kaijo
			years = 10
			modifier = opinion_disappointed
		}
	}
}

#I has been no longer a Choteki!
character_event = {
	id = EMPEROR.114
	desc = "EVTDESCEMPEROR.114"
	picture = GFX_evt_sunset
	
	is_triggered_only = yes

	option = { #I thank you!
		name = "EVTOPTAEMPEROR.114"
		piety = 50
		if = {
			limit = {
				NOT = {
					event_target:request_choteki_kaijo = {
						character = ROOT
					}
				}
			}
			opinion = {
				who = event_target:request_choteki_kaijo
				years = 10
				modifier = asked_to_lift_excommunication
			}
		}
		#log = "[Root.GetBestName]  was lifted Choteki at the behest of [request_choteki_kaijo.GetBestName]!(EMPEROR.114)"
	}
}

#Accepted!
letter_event = {
	id = EMPEROR.115
	desc = "EVTDESCEMPEROR.115"
	
	is_triggered_only = yes

	option = { #EXCELLENT
		name = "EVTOPTABAKUFU.13"
	}
}

#declined!
letter_event = {
	id = EMPEROR.116
	desc = "EVTDESCEMPEROR.106"
	picture = GFX_evt_guardian
	
	is_triggered_only = yes

	option = { #Oh...
		name = "EVTOPTABAKUFU.14"
		prestige = -50
	}
}

###############################
# Imperial Rank Events
###############################
#Notice of gained imperial rank by Regent
character_event = {
	id = EMPEROR.1000
	desc = "EVTDESCEMPEROR.1000"
	
	is_triggered_only = yes

	option = { #OK.
		name = "EVTOPTABAKUFU.313"
		if = {
			limit = {
				OR = {
					any_demesne_title = { title = e_tenno }
					government = patrimonial_bureaucracy
					government = honganji_government
				}
			}
			if = {
				limit = {
					FROM = {
						trait = jugoi_ge
					}
				}
				prestige = -100
			}
			else_if = {
				limit = {
					FROM = {
						trait = jugoi_jo
					}
				}
				prestige = -200
			}
			else_if = {
				limit = {
					FROM = {
						trait = shogoi_ge
					}
				}
				prestige = -300
			}
			else_if = {
				limit = {
					FROM = {
						trait = shogoi_jo
					}
				}
				prestige = -400
			}
			else_if = {
				limit = {
					FROM = {
						trait = jushii_ge
					}
				}
				prestige = -500
			}
			else_if = {
				limit = {
					FROM = {
						trait = jushii_jo
					}
				}
				prestige = -600
			}
			else_if = {
				limit = {
					FROM = {
						trait = shoshii_ge
					}
				}
				prestige = -700
			}
			else_if = {
				limit = {
					FROM = {
						trait = shoshii_jo
					}
				}
				prestige = -800
			}
			else_if = {
				limit = {
					FROM = {
						trait = jusanmi
					}
				}
				prestige = -900
			}
			else_if = {
				limit = {
					FROM = {
						trait = shosanmi
					}
				}
				prestige = -1000
			}
			else_if = {
				limit = {
					FROM = {
						trait = junii
					}
				}
				prestige = -1100
			}
			else_if = {
				limit = {
					FROM = {
						trait = shonii
					}
				}
				prestige = -1200
			}
			else_if = {
				limit = {
					FROM = {
						trait = juichii
					}
				}
				prestige = -1300
			}
			else_if = {
				limit = {
					FROM = {
						trait = shoichii
					}
				}
				prestige = -1400
			}
		}
		else = {
			if = {
				limit = {
					FROM = {
						trait = jugoi_ge
					}
				}
				wealth = -100
			}
			else_if = {
				limit = {
					FROM = {
						trait = jugoi_jo
					}
				}
				wealth = -200
			}
			else_if = {
				limit = {
					FROM = {
						trait = shogoi_ge
					}
				}
				wealth = -300
			}
			else_if = {
				limit = {
					FROM = {
						trait = shogoi_jo
					}
				}
				wealth = -400
			}
			else_if = {
				limit = {
					FROM = {
						trait = jushii_ge
					}
				}
				wealth = -500
			}
			else_if = {
				limit = {
					FROM = {
						trait = jushii_jo
					}
				}
				wealth = -600
			}
			else_if = {
				limit = {
					FROM = {
						trait = shoshii_ge
					}
				}
				wealth = -700
			}
			else_if = {
				limit = {
					FROM = {
						trait = shoshii_jo
					}
				}
				wealth = -800
			}
			else_if = {
				limit = {
					FROM = {
						trait = jusanmi
					}
				}
				wealth = -900
			}
			else_if = {
				limit = {
					FROM = {
						trait = shosanmi
					}
				}
				wealth = -1000
			}
			else_if = {
				limit = {
					FROM = {
						trait = junii
					}
				}
				wealth = -1100
			}
			else_if = {
				limit = {
					FROM = {
						trait = shonii
					}
				}
				wealth = -1200
			}
			else_if = {
				limit = {
					FROM = {
						trait = juichii
					}
				}
				wealth = -1300
			}
			else_if = {
				limit = {
					FROM = {
						trait = shoichii
					}
				}
				wealth = -1400
			}
		}
		log = "Regent [FromFrom.GetDynName] granted [From.GetDynName] a imperial court rank, [From.GetCourtRank]."
	}
}

#Notice of be gained imperial rank by Emperor
letter_event = {
	id = EMPEROR.1001
	desc = "EVTDESCEMPEROR.1001"
	
	is_triggered_only = yes

	option = { #I thank your Majesty.
		name = "EVTOPTAEMPEROR.105"
	}
}

#Notice of be gained imperial rank by Liege
letter_event = {
	id = EMPEROR.1002
	desc = "EVTDESCEMPEROR.1002"
	
	is_triggered_only = yes

	option = { #I thank you!
		name = "EVTOPTAEMPEROR.114"
	}
}
