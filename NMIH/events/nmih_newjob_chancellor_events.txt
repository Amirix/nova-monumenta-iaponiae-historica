###########################################
#                                         #
# Chancellor Events                       #
#                                         #
###########################################

namespace = DEPLOMACY


##Regent Declares War##
letter_event = { #Inform regent declared war by his/her claim
	id = DEPLOMACY.10
	desc = "EVTDESCDEPLOMACY.10"
	picture = GFX_evt_council
	
	is_triggered_only = yes
	show_from_from = yes

	option = {	
		name = "EVTOPTADEPLOMACY.10" 
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				war = yes
			}
			modifier = {
				factor = 5.0
				trait = content
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = FROMFROM
					value = 25
				}
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = FROMFROM
					value = 50
				}
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = FROMFROM
					value = 75
				}
			}
			modifier = {
				factor = 0.5
				NOT = { 
					opinion = {
						who = FROMFROM
						value = -25
					}
				}
			}
			modifier = {
				factor = 0.5
				NOT = { 
					opinion = {
						who = FROMFROM
						value = -50
					}
				}
			}
			modifier = {
				factor = 0.5
				NOT = { 
					opinion = {
						who = FROMFROM
						value = -75
					}
				}
			}
		}
		event_target:regent_war_title = {
			if = {
				limit = { holder_scope = { NOT = { is_liege_of = ROOT } } }
				reverse_war = {
					target = FROMFROM
					casus_belli = other_claim
					thirdparty = ROOT
				}
			}	
			if = {
				limit = { holder_scope = { is_liege_of = ROOT } }
				reverse_war = {
					target = FROMFROM
					casus_belli = other_claim_on_liege
					thirdparty = ROOT
				}
			}	
		}	
	}
	option = {
		name = "EVTOPTBDEPLOMACY.10" #NO!
		trigger = {
			has_regent = no
			OR = {
				has_law = imperial_administration
				has_law = law_voting_power_0
			}
		}
		opinion = {
			modifier = opinion_refuse_suggestion
			who = ROOT
			years = 10
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 2.0
				trait = paranoid
			}
			modifier = {
				factor = 0.2
				opinion = {
					who = FROMFROM
					value = 25
				}
			}
			modifier = {
				factor = 0.2
				opinion = {
					who = FROMFROM
					value = 50
				}
			}
			modifier = {
				factor = 0.2
				opinion = {
					who = FROMFROM
					value = 75
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = FROMFROM
						value = -25
					}
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = FROMFROM
						value = -50
					}
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = FROMFROM
						value = -75
					}
				}
			}
		}
		FROMFROM = { letter_event = { id = MARSHAL.3 } }
	}
}


letter_event = { #Inform regent declared war by other claim
	id = DEPLOMACY.11
	desc = "EVTDESCDEPLOMACY.10"
	picture = GFX_evt_council
	
	is_triggered_only = yes

	option = {	
		name = "EVTOPTADEPLOMACY.10" 
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				war = yes
			}
			modifier = {
				factor = 0.1
				relative_power_including_allies_attacker = {
					who = event_target:claim_target
					power = 0.7
				}
			}
			modifier = {
				factor = 5
				relative_power_including_allies_attacker = {
					who = event_target:claim_target
					power = 1.2
				}
			}
			modifier = {
				factor = 5.0
				trait = content
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = event_target:war_maker
					value = 25
				}
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = event_target:war_maker
					value = 50
				}
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = event_target:war_maker
					value = 75
				}
			}
			modifier = {
				factor = 0.5
				NOT = { 
					opinion = {
						who = event_target:war_maker
						value = -25
					}
				}
			}
			modifier = {
				factor = 0.5
				NOT = { 
					opinion = {
						who = event_target:war_maker
						value = -50
					}
				}
			}
			modifier = {
				factor = 0.5
				NOT = { 
					opinion = {
						who = event_target:war_maker
						value = -75
					}
				}
			}
		}
		event_target:regent_war_title = {
			if = {
				limit = { holder_scope = { NOT = { is_liege_of = ROOT } } }
				reverse_war = {
					target = event_target:war_claimant
					casus_belli = other_claim
					thirdparty = ROOT
				}
			}	
			if = {
				limit = { holder_scope = { is_liege_of = ROOT } }
				reverse_war = {
					target = event_target:war_claimant
					casus_belli = other_claim_on_liege
					thirdparty = ROOT
				}
			}	
		}	
	}
	option = {
		name = "EVTOPTBDEPLOMACY.10" #NO!
		trigger = {
			has_regent = no
			OR = {
				has_law = imperial_administration
				has_law = law_voting_power_0
			}
		}
		opinion = {
			modifier = opinion_refuse_suggestion
			who = ROOT
			years = 10
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 2.0
				trait = craven
			}
			modifier = {
				factor = 0.2
				opinion = {
					who = event_target:war_maker
					value = 25
				}
			}
			modifier = {
				factor = 0.2
				opinion = {
					who = event_target:war_maker
					value = 50
				}
			}
			modifier = {
				factor = 0.2
				opinion = {
					who = event_target:war_maker
					value = 75
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = event_target:war_maker
						value = -25
					}
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = event_target:war_maker
						value = -50
					}
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = event_target:war_maker
						value = -75
					}
				}
			}
		}
		event_target:war_maker = { letter_event = { id = MARSHAL.3 } }
	}
}


character_event = {
	id = DEPLOMACY.15
	
	is_triggered_only = yes
	desc = EVTDESCDEPLOMACY.15
	picture = GFX_evt_council
	
	immediate = {
		if = {
			limit = {
				event_target:war_claimant = {
					any_claim = {
						holder_scope = {
							OR = {
								character = event_target:claim_target
								top_liege = { character = event_target:claim_target }
							}
						}
					}
				}
			}
			event_target:war_claimant = {
				random_claim = {
					limit = {
						holder_scope = {
							OR = {
								character = event_target:claim_target
								top_liege = { character = event_target:claim_target }
							}
						}
					}
					set_title_flag = is_claim_title_0
					save_event_target_as = claim_title_0
				}
			}
			set_character_flag = claim_title_0
		}
		if = {
			limit = {
				event_target:war_claimant = {
					any_claim = {
						holder_scope = {
							OR = {
								character = event_target:claim_target
								top_liege = { character = event_target:claim_target }
							}
						}
						NOT = { title = event_target:claim_title_0 }
					}
				}
			}
			event_target:war_claimant = {
				random_claim = {
					limit = {
						holder_scope = {
							OR = {
								character = event_target:claim_target
								top_liege = { character = event_target:claim_target }
							}
						}
						NOT = { title = event_target:claim_title_0 }
					}
					set_title_flag = is_claim_title_1
					save_event_target_as = claim_title_1
				}
			}
			set_character_flag = claim_title_1
		}
		if = {
			limit = {
				event_target:war_claimant = {
					any_claim = {
						holder_scope = {
							OR = {
								character = event_target:claim_target
								top_liege = { character = event_target:claim_target }
							}
						}
						NOT = { title = event_target:claim_title_0 }
						NOT = { title = event_target:claim_title_1 }
					}
				}
			}
			event_target:war_claimant = {
				random_claim = {
					limit = {
						holder_scope = {
							OR = {
								character = event_target:claim_target
								top_liege = { character = event_target:claim_target }
							}
						}
						NOT = { title = event_target:claim_title_0 }
						NOT = { title = event_target:claim_title_1 }
					}
					set_title_flag = is_claim_title_2
					save_event_target_as = claim_title_2
				}
			}
			set_character_flag = claim_title_2
		}
	}
	option = { # [claim_title_0.GetBaseName] will be good
		name = EVTOPTADEPLOMACY.15
		trigger = {
			has_character_flag = claim_title_0
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 4
				event_target:claim_title_0 = {
					tier = EMPEROR
				}
			}
			modifier = {
				factor = 3
				event_target:claim_title_0 = {
					tier = KING
				}
			}
			modifier = {
				factor = 2
				event_target:claim_title_0 = {
					tier = DUKE
				}
			}
		}
		
		event_target:claim_title_0 = {
				save_event_target_as = regent_war_title
		}
		hidden_tooltip = {
			liege = {
				letter_event = { id = DEPLOMACY.11 }
			}
		}
	}
	option = { # I choice [claim_title_1.GetBaseName]
		name = EVTOPTBDEPLOMACY.15
		trigger = {
			has_character_flag = claim_title_1
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 4
				event_target:claim_title_1 = {
					tier = EMPEROR
				}
			}
			modifier = {
				factor = 3
				event_target:claim_title_1 = {
					tier = KING
				}
			}
			modifier = {
				factor = 2
				event_target:claim_title_1 = {
					tier = DUKE
				}
			}
		}
		
		event_target:claim_title_1 = {
				save_event_target_as = regent_war_title
		}
		hidden_tooltip = {
			liege = {
				letter_event = { id = DEPLOMACY.11 }
			}
		}
	}
	option = { # I expectat [claim_title_2.GetBaseName].
		name = EVTOPTCDEPLOMACY.15
		trigger = {
			has_character_flag = claim_title_2
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 4
				event_target:claim_title_2 = {
					tier = EMPEROR
				}
			}
			modifier = {
				factor = 3
				event_target:claim_title_2 = {
					tier = KING
				}
			}
			modifier = {
				factor = 2
				event_target:claim_title_2 = {
					tier = DUKE
				}
			}
		}
		
		event_target:claim_title_2 = {
				save_event_target_as = regent_war_title
		}
		hidden_tooltip = {
			liege = {
				letter_event = { id = DEPLOMACY.11 }
			}
		}
	}
	option = { # NO, I want another title
		name = EVTOPTDDEPLOMACY.15
		ai_chance = {
			factor = 0
		}
		repeat_event = { id = DEPLOMACY.15 }
	}
	
	after = {
		clr_character_flag = claim_title_0
		clr_character_flag = claim_title_1
		clr_character_flag = claim_title_2
		event_target:war_claimant = {
			any_claim = {
				clr_title_flag = is_claim_title_0
				clr_title_flag = is_claim_title_1
				clr_title_flag = is_claim_title_2
			}
		}
	}
}
##
##Give a liege's title
##
character_event = {
	id = DEPLOMACY.20
	
	is_triggered_only = yes
	desc = EVTDESCDEPLOMACY.20
	picture = GFX_evt_council
	
	immediate = {
		if = {
			limit = {
				event_target:grant_liege = {
					any_demesne_title = {
						count = 2
						tier = COUNT
					}
				}
			}
			event_target:grant_liege = {
				random_demesne_title = {
					limit = {
						tier = COUNT
						can_be_given_away = yes
					}
					save_event_target_as = claim_title_0
				}
			}
			set_character_flag = claim_title_0
		}
		if = {
			limit = {
				event_target:grant_liege = {
					any_demesne_title = {
						can_be_given_away = yes
						NOT = { title = event_target:claim_title_0 }
					}
				}
			}
			event_target:grant_liege = {
				random_demesne_title = {
					limit = {
						can_be_given_away = yes
						NOT = { title = event_target:claim_title_0 }
					}
					save_event_target_as = claim_title_1
				}
			}
			set_character_flag = claim_title_1
		}
		if = {
			limit = {
				event_target:grant_target = {
					any_demesne_title = {
						tier = COUNT
					}
				}
				event_target:grant_liege = {
					any_demesne_title = {
						NOT = { tier = COUNT }
						can_be_given_away = yes
						NOT = { title = event_target:claim_title_0 }
						NOT = { title = event_target:claim_title_1 }
					}
				}
			}
			event_target:grant_liege = {
				any_demesne_title = {
					limit = {
						NOT = { tier = COUNT }
						can_be_given_away = yes
						NOT = { title = event_target:claim_title_0 }
						NOT = { title = event_target:claim_title_1 }
					}
					save_event_target_as = claim_title_2
				}
			}
			set_character_flag = claim_title_2
		}
	}
	option = { # [claim_title_0.GetBaseName] will be good
		name = EVTOPTADEPLOMACY.15
		trigger = {
			has_character_flag = claim_title_0
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 4
				event_target:claim_title_0 = {
					tier = EMPEROR
				}
			}
			modifier = {
				factor = 3
				event_target:claim_title_0 = {
					tier = KING
				}
			}
			modifier = {
				factor = 2
				event_target:claim_title_0 = {
					tier = DUKE
				}
			}
		}
		
		event_target:claim_title_0 = {
			save_event_target_as = regent_grant_title
		}
		hidden_tooltip = {
			liege = {
				letter_event = { id = DEPLOMACY.21 }
			}
		}
	}
	option = { # I choice [claim_title_1.GetBaseName]
		name = EVTOPTBDEPLOMACY.15
		trigger = {
			has_character_flag = claim_title_1
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 4
				event_target:claim_title_1 = {
					tier = EMPEROR
				}
			}
			modifier = {
				factor = 3
				event_target:claim_title_1 = {
					tier = KING
				}
			}
			modifier = {
				factor = 2
				event_target:claim_title_1 = {
					tier = DUKE
				}
			}
		}
		
		event_target:claim_title_1 = {
				save_event_target_as = regent_grant_title
		}
		hidden_tooltip = {
			liege = {
				letter_event = { id = DEPLOMACY.21 }
			}
		}
	}
	option = { # I expectat [claim_title_2.GetBaseName].
		name = EVTOPTCDEPLOMACY.15
		trigger = {
			has_character_flag = claim_title_2
		}
		ai_chance = {
			factor = 1
			modifier = {
				factor = 4
				event_target:claim_title_2 = {
					tier = EMPEROR
				}
			}
			modifier = {
				factor = 3
				event_target:claim_title_2 = {
					tier = KING
				}
			}
			modifier = {
				factor = 2
				event_target:claim_title_2 = {
					tier = DUKE
				}
			}
		}
		
		event_target:claim_title_2 = {
			save_event_target_as = regent_grant_title
		}
		hidden_tooltip = {
			liege = {
				letter_event = { id = DEPLOMACY.21 }
			}
		}
	}
	option = { # NO, I want another title
		name = EVTOPTDDEPLOMACY.15
		ai_chance = {
			factor = 0
		}
		repeat_event = { id = DEPLOMACY.20 }
	}
	
	after = {
		clr_character_flag = claim_title_0
		clr_character_flag = claim_title_1
		clr_character_flag = claim_title_2
	}
}

letter_event = { #Inform regent grant liege's title
	id = DEPLOMACY.21
	desc = "EVTDESCDEPLOMACY.21"
	picture = GFX_evt_council
	
	is_triggered_only = yes

	option = {	
		name = "EVTOPTADEPLOMACY.10" 
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0
				war = yes
			}
			modifier = {
				factor = 0
				trait = greedy
			}
			modifier = {
				factor = 0
				NOT = { over_max_demesne_size = 1 }
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = event_target:grant_target
					value = 25
				}
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = event_target:grant_target
					value = 50
				}
			}
			modifier = {
				factor = 2.0
				opinion = {
					who = event_target:grant_target
					value = 75
				}
			}
			modifier = {
				factor = 0.25
				NOT = { 
					opinion = {
						who = event_target:grant_target
						value = -25
					}
				}
			}
			modifier = {
				factor = 0.25
				NOT = { 
					opinion = {
						who = event_target:grant_target
						value = -50
					}
				}
			}
			modifier = {
				factor = 0.25
				NOT = { 
					opinion = {
						who = event_target:grant_target
						value = -75
					}
				}
			}
		}
		event_target:regent_grant_title = {
			grant_title = event_target:grant_target
		}
		event_target:grant_target = {
			if = {
				limit = {
					event_target:regent_grant_title = {
						tier = BARON
					}
				}
				opinion = {
					modifier = opinion_granted_barony
					who = FROM
					years = 10
				}
			}
			else_if = {
				limit = {
					event_target:regent_grant_title = {
						tier = COUNT
					}
				}
				opinion = {
					modifier = opinion_granted_county
					who = FROM
					years = 10
				}
			}
			else_if = {
				limit = {
					event_target:regent_grant_title = {
						tier = DUKE
					}
				}
				opinion = {
					modifier = opinion_granted_duchy
					who = FROM
					years = 10
				}
			}
			else_if = {
				limit = {
					event_target:regent_grant_title = {
						tier = KING
					}
				}
				opinion = {
					modifier = opinion_granted_kingdom
					who = FROM
					years = 10
				}
			}
			else = {
				opinion = {
					modifier = opinion_granted_empire
					who = FROM
					years = 10
				}
			}
		}
		FROM = { letter_event = { id = DEPLOMACY.22 } }
	}
	option = {
		name = "EVTOPTBDEPLOMACY.10" #NO!
		trigger = {
			has_regent = no
			OR = {
				has_law = imperial_administration
				has_law = law_voting_power_0
				has_law = grant_title_voting_power_0
			}
		}
		opinion = {
			modifier = opinion_refuse_suggestion
			who = ROOT
			years = 10
		}
		ai_chance = {
			factor = 100
			modifier = {
				factor = 0.9
				opinion = {
					who = event_target:grant_target
					value = 25
				}
			}
			modifier = {
				factor = 0.9
				opinion = {
					who = event_target:grant_target
					value = 50
				}
			}
			modifier = {
				factor = 0.9
				opinion = {
					who = event_target:grant_target
					value = 75
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = event_target:grant_target
						value = -25
					}
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = event_target:grant_target
						value = -50
					}
				}
			}
			modifier = {
				factor = 2
				NOT = { 
					opinion = {
						who = event_target:grant_target
						value = -75
					}
				}
			}
		}
		FROM = { letter_event = { id = MARSHAL.3 } }
	}
}

#Client recieves letter
letter_event = {
	id = DEPLOMACY.22
	desc = "EVTDESCDEPLOMACY.22"
	border = GFX_event_letter_frame_diplomacy

	is_triggered_only = yes

	option = {
		name = "EVTOPTAMARSHAL.2" #OK.
	}
}
