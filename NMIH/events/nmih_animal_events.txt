########################################
# NMIH Animal Events
########################################

namespace = NMIH_ANIMALS

## The fox

character_event = {
	id = NMIH_ANIMALS.1
	desc = EVTDESCNMIH_ANIMALS.1
	picture = GFX_evt_the_fox
	border = GFX_event_normal_frame_war
	
	trigger = {
		culture = fox_culture
	}
	
	mean_time_to_happen = {
		months = 120
	}
	
	option = {
		name = EVTOPTANMIH_ANIMALS.1
	}
	
	option = {
		name = EVTOPTBNMIH_ANIMALS.1
	}
	
	option = {
		name = EVTOPTCNMIH_ANIMALS.1
	}
	
	option = {
		name = EVTOPTDNMIH_ANIMALS.1
	}
}