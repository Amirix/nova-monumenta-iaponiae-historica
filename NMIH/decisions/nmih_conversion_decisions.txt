decisions = {

	convert_to_ikko_shu = {
		potential = {
			is_playable = yes
			religion_group = indian_group
			NOT = { religion = ikko_shu }
			is_theocracy = no
		}
		allow = {
			is_adult = yes
			prestige = 500
			NOT = { trait = zealous }
			any_realm_province = {
				religion = ikko_shu
			}
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			if = {
				limit = { higher_tier_than = BARON }
				religion_authority = {
					modifier = ruler_converted_from
				}
			}
			religion = ikko_shu
			if = {
				limit = { higher_tier_than = BARON }
				hidden_tooltip = {
					religion_authority = {
						modifier = ruler_converted_to
					}
				}
			}
			set_character_flag = india_converted
			prestige = -500
			piety = 100
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = ikko_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = ikko_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
	
	convert_to_hokke_shu = {
		potential = {
			is_playable = yes
			religion_group = indian_group
			NOT = { religion = hokke_shu }
			is_theocracy = no
		}
		allow = {
			is_adult = yes
			prestige = 500
			NOT = { trait = zealous }
			any_realm_province = {
				religion = hokke_shu
			}
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			if = {
				limit = { higher_tier_than = BARON }
				religion_authority = {
					modifier = ruler_converted_from
				}
			}
			religion = hokke_shu
			if = {
				limit = { higher_tier_than = BARON }
				hidden_tooltip = {
					religion_authority = {
						modifier = ruler_converted_to
					}
				}
			}
			set_character_flag = india_converted
			prestige = -500
			piety = 100
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = hokke_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = hokke_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
	
	convert_to_mikkyo = {
		potential = {
			is_playable = yes
			religion = buddhist
			NOT = { trait = mikkyo_buddhist }
			NOT = { government = honganji_government }
		}
		allow = {
			is_adult = yes
			prestige = 50
			NOT = { trait = zealous }
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			remove_trait = yoshida_shinto
			remove_trait = shugendo_buddhist
			remove_trait = zen_buddhist
			remove_trait = jodo_buddhist
			add_trait = mikkyo_buddhist
			set_character_flag = india_converted
			prestige = -50
			piety = 10
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = ikko_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = ikko_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
	
	convert_to_shugendo = {
		potential = {
			is_playable = yes
			religion = buddhist
			NOT = { trait = shugendo_buddhist }
		}
		allow = {
			is_adult = yes
			prestige = 50
			NOT = { trait = zealous }
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			remove_trait = yoshida_shinto
			remove_trait = mikkyo_buddhist
			remove_trait = zen_buddhist
			remove_trait = jodo_buddhist
			add_trait = shugendo_buddhist
			set_character_flag = india_converted
			prestige = -50
			piety = 10
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = ikko_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = ikko_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
	convert_to_zen = {
		potential = {
			is_playable = yes
			religion = buddhist
			NOT = { trait = zen_buddhist }
			NOT = { government = honganji_government }
		}
		allow = {
			is_adult = yes
			prestige = 50
			NOT = { trait = zealous }
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			remove_trait = yoshida_shinto
			remove_trait = mikkyo_buddhist
			remove_trait = shugendo_buddhist
			remove_trait = jodo_buddhist
			add_trait = zen_buddhist
			set_character_flag = india_converted
			prestige = -50
			piety = 10
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = ikko_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = ikko_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
	
	convert_to_yoshida = {
		potential = {
			is_playable = yes
			religion = buddhist
			NOT = { trait = yoshida_shinto }
			is_theocracy = no
		}
		allow = {
			is_adult = yes
			prestige = 50
			NOT = { trait = zealous }
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			remove_trait = zen_buddhist
			remove_trait = mikkyo_buddhist
			remove_trait = shugendo_buddhist
			remove_trait = jodo_buddhist
			add_trait = yoshida_shinto
			set_character_flag = india_converted
			prestige = -50
			piety = 10
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = ikko_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = ikko_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
	
	convert_to_jodo = {
		potential = {
			is_playable = yes
			religion = buddhist
			NOT = { trait = jodo_buddhist }
			NOT = { government = honganji_government }
		}
		allow = {
			is_adult = yes
			prestige = 50
			NOT = { trait = zealous }
			custom_tooltip = {
				text = indian_has_not_converted
				NOT = { has_character_flag = india_converted }
			}
		}
		effect = {
			remove_trait = zen_buddhist
			remove_trait = mikkyo_buddhist
			remove_trait = shugendo_buddhist
			remove_trait = yoshida_shinto
			add_trait = jodo_buddhist
			set_character_flag = india_converted
			prestige = -50
			piety = 10
#			hidden_tooltip = { character_event = { id = RoI.111 } } # Choose branch
		}
		revoke_allowed = {
			always = no
		}
		ai_will_do = {
			factor = 1

			modifier = { # Slow down
				factor = 0.1
			}
#			modifier = { # Don't convert away from liege's religion
#				factor = 0
#				liege = { religion = ROOT }
#			}
			modifier = { # Don't start rocking the boat if all your provinces are under foreign religious influence
				factor = 0
				NOT = {
					any_realm_province = {
						religion_group = indian_group
					}
				}
			}
			modifier = { # NEVER consider it unless your liege or, if independent, all your vassals are Ikko-Shu and they don't like you
				factor = 0
				NOT = {
					OR = {
						AND = {
							independent = yes
							NOT = {
								any_vassal = {
									NOT = { religion = ikko_shu }
								}
							}
							NOT = {
								any_vassal = {
									opinion = {
										who = ROOT
										value = 25
									}
								}
							}
						}
						AND = {
							liege = { religion = ikko_shu }
							opinion = {
								who = liege
								value = 25
							}
						}
					}
				}
			}
			modifier = { # More likely to do it sooner
				factor = 3
				trait = cynical
			}
			modifier = { # Sometimes you really need to hurry up
				factor = 5
				higher_tier_than = DUKE
				num_of_vassals = 5
			}
		}
	}
}