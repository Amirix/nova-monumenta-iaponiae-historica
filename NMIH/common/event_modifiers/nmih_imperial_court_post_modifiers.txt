##################################################
#
# STRATEGIC EFFECTS - A.K.A. "TRIGGERED MODIFIERS"
#
##################################################


# Imperial court post

taiko = {
	monthly_character_prestige = 0.15
	learning = 5
	global_revolt_risk = -0.15
	vassal_opinion = 25
	icon = 7
}

kampaku = {
	monthly_character_prestige = 0.10
	learning = 3
	global_revolt_risk = -0.10
	vassal_opinion = 20
	icon = 7
}

daijin = {
	monthly_character_prestige = 0.075
	learning = 3
	global_revolt_risk = -0.05
	vassal_opinion = 15
	icon = 7
}

nagon = {
	monthly_character_prestige = 0.05
	learning = 2
	global_revolt_risk = -0.04
	vassal_opinion = 10
	icon = 7
}

jiju = {
	monthly_character_prestige = 0.02
	learning = 1
	global_revolt_risk = -0.02
	vassal_opinion = 5
	icon = 7
}

hassho_kyo = {
	monthly_character_prestige = 1.0
	stewardship = 3
	global_tax_modifier = 0.08
	vassal_opinion = 15
	icon = 2
}

hassho_daihu = {
	monthly_character_prestige = 0.05
	stewardship = 2
	global_tax_modifier = 0.05
	vassal_opinion = 10
	icon = 2
}

hassho_sho = {
	monthly_character_prestige = 0.02
	stewardship = 1
	global_tax_modifier = 0.02
	vassal_opinion = 5
	icon = 2
}

taisho = {
	monthly_character_prestige = 1.0
	martial = 3
	combat_rating = 3
	vassal_opinion = 15
	icon = 13
}

chujo_kami = {
	monthly_character_prestige = 0.09
	martial = 3
	combat_rating = 3
	vassal_opinion = 15
	icon = 13
}

suke = {
	monthly_character_prestige = 0.05
	martial = 2
	combat_rating = 2
	vassal_opinion = 10
	icon = 13
}

jo = {
	monthly_character_prestige = 0.02
	martial = 1
	combat_rating = 1
	vassal_opinion = 5
	icon = 13
}

kokushi_dazai = {
	monthly_character_prestige = 1.0
	diplomacy = 3
	short_reign_length = -10
	vassal_opinion = 15
	icon = 9
}

kokushi_kami = {
	monthly_character_prestige = 0.05
	diplomacy = 2
	short_reign_length = -8
	vassal_opinion = 10
	icon = 9
}

kokushi_suke = {
	monthly_character_prestige = 0.02
	diplomacy = 1
	short_reign_length = -5
	vassal_opinion = 5
	icon = 9
}
