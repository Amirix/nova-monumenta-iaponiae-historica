### Fujiwara Clans

1000 = {
	name="Fujiwara"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 29
			emblem = 0
			color = 5
			color = 6
			color = 0
		}
	}
	used_for_random = no
}

1001 = {
	name="Konoe"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 30
			emblem = 0
			color = 6
			color = 23
			color = 0
		}
	}
	used_for_random = no
}

1002 = {
	name="Matsudono"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 31
			emblem = 0
			color = 6
			color = 27
			color = 0
		}
	}
	used_for_random = no
}

1003 = {
	name="Kujo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 32
			emblem = 0
			color = 6
			color = 32
			color = 0
		}
	}
	used_for_random = no
}

1004 = {
	name="Takatsukasa"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 33
			emblem = 0
			color = 6
			color = 26
			color = 0
		}
	}
	used_for_random = no
}

1005 = {
	name="Nijo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 34
			emblem = 0
			color = 6
			color = 18
			color = 0
		}
	}
	used_for_random = no
}

1006 = {
	name="Imanokoji"
	culture = aristocrats
	used_for_random = no
}

1007 = {
	name="Awataguchi"
	culture = aristocrats
	used_for_random = no
}

1008 = {
	name="Fujii"
	culture = aristocrats
	used_for_random = no
}

1009 = {
	name="Kinugasa"
	culture = aristocrats
	used_for_random = no
}

1010 = {
	name="Tsukinowa"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 30 # Kujo Fuji
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random=no
}

1012 = {
	name="Takatsukasa"
	culture = aristocrats
	used_for_random = no
}

1013 = {
	name="Hino"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 3 # Tsuru Maru
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1014 = {
	name="Hirohashi"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 31 # Mukai Tsuru
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1015 = {
	name="Yanagiwara"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 3 # Tsuru Maru
			emblem = 0
			color = 5 # Sable (Black)
			color = 4 # Vert(Light Green)
			color = 0
		}
	}
	used_for_random = no
}

1016 = {
	name="Karasumaru"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 3 # Tsuru Maru
			emblem = 0
			color = 5 # Sable (Black)
			color = 6 # Purple
			color = 0
		}
	}
	used_for_random = no
}

1019 = {
	name="Takeya"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 31 # Mukai Tsuru
			emblem = 0
			color = 5 # Sable (Black)
			color = 4 # Vert (Light Green)
			color = 0
		}
	}
	used_for_random = no
}

1023 = {
	name="Mushanokoji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 3 # Tsuru Maru
			emblem = 0
			color = 1 # Argent (White)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
	used_for_random = no
}

1025 = {
	name="Yoshida"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 32 # Kanshuji Sasa
			emblem = 0
			color = 1 # Argent (White)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
	used_for_random = no
}

1026 = {
	name="Kujo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 32 # Kanshuji Sasa
			emblem = 0
			color = 1 # Argent (White)
			color = 3 # Gules (Red)
			color = 0
		}
	}
	used_for_random = no
}

1027 = {
	name="Nakanomikado"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 32 # Kanshuji Sasa
			emblem = 0
			color = 5 # Sable (Black)
			color = 4 # Vert (Light Green)
			color = 0
		}
	}
	used_for_random = no
}

1028 = {
	name="Bojo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 32 # Kanshuji Sasa
			emblem = 0
			color = 1 # Argent (White)
			color = 5 # Sable (Black)
			color = 0
		}
	}
	used_for_random = no
}

1029 = {
	name="Seikanji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 32 # Kanshuji Sasa
			emblem = 0
			color = 1 # Argent (White)
			color = 5 # Sable (Black)
			color = 0
		}
	}
	used_for_random = no
}

1030={
	name="Hamuro"
	culture=aristocrats
	coat_of_arms={
		template=0
		layer={
			texture=16
			texture_internal=23 #
			emblem=0
			color=1 # Argent (White)
			color=5 # Sable (Black)
			color = 0
		}
	}
	used_for_random=no
}

1032 = {
	name="Madenokoji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 32 # Kanshuji Sasa
			emblem = 0
			color = 1 # Argent (White)
			color = 6 # Purple
			color = 0
		}
	}
	used_for_random = no
}

1040 = {
	name="Anekoji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 3 # Kanshuji Sasa
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1041 = {
	name="Bomon"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 3 # Kanshuji Sasa
			emblem = 0
			color = 1 # Argent (White)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
	used_for_random = no
}

1044 = {
	name="Sanjo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 39 # Karahanahishi
			emblem = 0
			color = 6
			color = 40
			color = 0
		}
	}
	used_for_random = no
}

1045 = {
	name="Sanjo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 7 # Wari hishi
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1046 = {
	name="Tokudaiji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 40 #
			emblem = 0
			color = 6
			color = 29
			color = 0
		}
	}
	used_for_random = no
}

1047 = {
	name="Imadegawa"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 42 #
			emblem = 0
			color = 6
			color = 44
			color = 0
		}
	}
	used_for_random = no
}

1048 = {
	name="Toin"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 1 # Hidari 3 Tomoe
			emblem = 0
			color = 1  # Argent (White)
			color = 5  # Sable (Black)
			color = 0
		}
	}
	used_for_random = no
}

1049 = {
	name="Sanjo" # Ogimachi=Sanjo
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 41 #
			emblem = 0
			color = 6
			color = 31
			color = 0
		}
	}
	used_for_random = no
}

1052 = {
	name="Shigenoi"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 33 #
			emblem = 0
			color = 1  # Argent (White)
			color = 5  # Sable (Black)
			color = 0
		}
	}
	used_for_random = no
}

1053 = {
	name="Ichijo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 7 # Wari hishi
			emblem = 0
			color = 1 # Argent (White)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
	used_for_random = no
}

1055 = {
	name="Muromachi"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 41 # Kara Hana
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1057 = {
	name="Hashimoto"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 34 # Onaga Tomoe
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1058 = {
	name="Omiya"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 1 # Tomoe
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1063 = {
	name="Anegakoji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 33 # Sanjo Hanakaku
			emblem = 0
			color = 1 # Argent (White)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
	used_for_random = no
}

1067 = {
	name="Kazanin"
	culture = aristocrats
	used_for_random = no
}

1068 = {
	name="Oinomikado"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 35 # Hishi ni Katabami
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1081 = {
	name="Jimyoin"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 42 # Gyoyo
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1090 = {
	name="Minase"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 1 # Kiku
			emblem = 0
			color = 5 # Sable (Black)
			color = 1 # Argent (White)
			color = 0
		}
	}
	used_for_random = no
}

1092 = {
	name="Imaoji"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 43 # Denjiso
			emblem = 0
			color = 6
			color = 38
			color = 0
		}
	}
	used_for_random = no
}

1093 = {
	name="Shijo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 14
			texture_internal = 43 # Denjiso
			emblem = 0
			color = 6
			color = 25
			color = 0
		}
	}
	used_for_random = no
}

1094 = {
	name="Ichijo"
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 3	# Sagari Fuji
			emblem = 0
			color = 1   # Argent (White)
			color = 9
			color = 0
		}
	}
	used_for_random = no
}

1095 = {
	name="Bomon"
	culture = aristocrats
	used_for_random = no
}
1096 = {
	name="Anekoji" # Hida=Anekoji
	culture = aristocrats
	coat_of_arms = {
		template = 0
		layer = {
			texture = 9
			texture_internal = 10
			emblem = 0
			color = 5
			color = 24
			color = 0
		}
	}
	used_for_random = no
}