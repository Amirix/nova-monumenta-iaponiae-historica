###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
# Dynasty data file for random characters.
# 
# @param culture kanto_buddhist
# @param id_range 21,600,000 - 21,799,999
# 
###############################################################################

21600000={
	name="Myohonji"
	culture=kanto_buddhist
}
21600001={
	name="Hokkekyoji"
	culture=kanto_buddhist
}
21600002={
	name="Kanasanaguji"
	culture=kanto_buddhist
}
21600003={
	name="Rinnoji"
	culture=kanto_buddhist
}
21600004={
	name="Yakushiji"
	culture=kanto_buddhist
}
21600005={
	name="Oyamadera"
	culture=kanto_buddhist
}
21600006={
	name="Fudoin"
	culture=kanto_buddhist
}
21600007={
	name="Kashozan"
	culture=kanto_buddhist
}
21600008={
	name="Mitsuminesan"
	culture=kanto_buddhist
}
21600009={
	name="Takaosan"
	culture=kanto_buddhist
}
