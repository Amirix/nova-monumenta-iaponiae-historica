#Shikoku Clans 580-
580 = {
	name="Motoyama"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 15
			emblem = 0
			color = 5
			color = 16
			color = 0
		}
	}
}
581 = {
	name="Aki"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 16
			emblem = 0
			color = 5
			color = 25
			color = 0
		}
	}
}
582 = {
	name="Chosokabe"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 17
			emblem = 0
			color = 2
			color = 28
			color = 0
		}
	}
}
583 = {
	name="Ichijo"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 18
			emblem = 0
			color = 2
			color = 33
			color = 0
		}
	}
}

584 = {
	name="Kono"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 19
			emblem = 0
			color = 5
			color = 2
			color = 0
		}
	}
}

585 = {
	name="Saionji"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 20
			emblem = 0
			color = 5
			color = 49
			color = 0
		}
	}
}

586 = {
	name="Murakami"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 21
			emblem = 0
			color = 5
			color = 30
			color = 0
		}
	}
}

587 = {
	name="Miyoshi"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 13
			texture_internal = 22
			emblem = 0
			color = 5
			color = 21
			color = 0
		}
	}
}

588 = {
	name="Akutagawa"
	culture = shikoku
}

589 = {
	name="Atagi"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 13
			texture_internal = 23
			emblem = 0
			color = 5
			color = 18
			color = 0
		}
	}
}

590 = {
	name="Sogo"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 13
			texture_internal = 24
			emblem = 0
			color = 5
			color = 26
			color = 0
		}
	}
}

592 = {
	name="Kozai" #In historical fact, Mitsu-shina-Matsu
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 11
			texture_internal = 0
			emblem = 0
			color = 5	# Sable (Black)
			color = 4   # Vert (Light Green)
			color = 0
		}
	}
}

593 = {
	name="Kagawa"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 11
			emblem = 0
			color = 5	# Sable (Black)
			color = 9	# Bleu-Celeste (Sky Blue)
			color = 0
		}
	}
}

594 = {
	name="Yasutomi" #In historical fact, Marunouchi Ishidatami
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 24
			emblem = 0
			color = 5	# Sable (Black)
			color = 3 # Gules (Red)
			color = 0
		}
	}
}

595 = {
	name="Nara" #In historical fact, Kenkara Hana
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 49
			emblem = 0
			color = 5	# Sable (Black)
			color = 4   # Vert (Light Green)
			color = 0
		}
	}
}

596 = {
	name="Kaifu"
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 23
			emblem = 0
			color = 5	# Sable (Black)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
}

597 = {
	name="Onishi" #In historical fact, Maru ni Tsugai-gan
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 29
			emblem = 0
			color = 5	# Sable (Black)
			color = 4   # Vert (Light Green)
			color = 0
		}
	}
}

598 = {
	name="Okamoto" #In historical fact, Unknown
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 1
			emblem = 0
			color = 5	# Sable (Black)
			color = 2 # Azure (Blue)
			color = 0
		}
	}
}

599 = {
	name="Shinonara" #In historical fact, Unknown
	culture = shikoku
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 22
			emblem = 0
			color = 5	# Sable (Black)
			color = 9	# Bleu-Celeste (Sky Blue)
			color = 0
		}
	}
}

787={
	name="Tsuno"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=16
			texture_internal=29
			emblem=0
			color=5
			color=24
			color=0
		}
	}
}
788={
	name="Kira"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=17
			texture_internal=31
			emblem=0
			color=5
			color=25
			color=0
		}
	}
}
789={
	name="Kosokabe"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=9
			texture_internal=27
			emblem=0
			color=5
			color=26
			color=0
		}
	}
}
790={
	name="Doi"
	culture=shikoku
}
791={
	name="Ojima"
	culture=shikoku
}
792={
	name="Sangawa"
	culture=shikoku
}
793={
	name="Shingai"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=16
			texture_internal=1
			emblem=0
			color=5
			color=27
			color=0
		}
	}
}
794={
	name="Shichijo"
	culture=shikoku
}
795={
	name="Kan"
	culture=shikoku
}
796={
	name="Tokui"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=9
			texture_internal=37
			emblem=0
			color=5
			color=28
			color=0
		}
	}
	used_for_random = no
}
797={
	name="Wada"
	culture=shikoku
}
798={
	name="Hiraoka"
	culture=shikoku
}
799={
	name="Ono"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=10
			texture_internal=16
			emblem=0
			color=5
			color=29
			color=0
		}
	}
}
800={
	name="Ishikawa"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=13
			texture_internal=17
			emblem=0
			color=5
			color=30
			color=0
		}
	}
}
801={
	name="Doi"
	culture=shikoku
	used_for_random = no
}
802={
	name="Yoshida"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=13
			texture_internal=34
			emblem=0
			color=5
			color=31
			color=0
		}
	}
}
803={
	name="Emura"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=13
			texture_internal=34
			emblem=0
			color=5
			color=32
			color=0
		}
	}
}
804={
	name="Fukudome"
	culture=shikoku
	coat_of_arms={
		template=0
		layer={
			texture=13
			texture_internal=14
			emblem=0
			color=5
			color=33
			color=0
		}
	}
}
805={
	name="Hisatake"
	culture=shikoku
}
806={
	name="Tani"
	culture=shikoku
}
807={
	name="Takimotoji"
	culture=saigoku_buddhist
	used_for_random = no
}


