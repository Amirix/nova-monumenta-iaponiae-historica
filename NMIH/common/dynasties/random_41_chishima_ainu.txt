###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
# Dynasty data file for random characters.
# 
# @param culture chishima_ainu
# @param id_range 28,000,000 - 28,199,999
# 
###############################################################################

28000335={
	name="Hurepet"
	culture="chishima_ainu"
}

28000336={
	name="Rupet"
	culture="chishima_ainu"
}

28000337={
	name="Urup"
	culture="chishima_ainu"
}

28000338={
	name="Tomari"
	culture=chishima_ainu
}
28000339={
	name="Ruiopet"
	culture=chishima_ainu
}
28000340={
	name="Hurkaomap"
	culture=chishima_ainu
}
28000341={
	name="Sannai"
	culture=chishima_ainu
}
28000342={
	name="Sipetoro"
	culture=chishima_ainu
}
28000343={
	name="Simosir"
	culture=chishima_ainu
}
28000344={
	name="Sikotan"
	culture=chishima_ainu
}
