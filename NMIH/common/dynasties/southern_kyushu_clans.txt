#Minami-Kyushu Clans
600 = {
	name="Shimazu"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 13
			emblem = 0
			color = 5
			color = 46
			color = 0
		}
	}
}

601 = {
	name="Kimotsuki"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 14
			emblem = 0
			color = 2
			color = 51
			color = 0
		}
	}
}

602 = {
	name="Tanegashima"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 9
			texture_internal = 11
			emblem = 0
			color = 36
			color = 2
			color = 0
		}
	}
}

603 = {
	name="Gamo"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 16
			emblem = 0
			color = 5
			color = 27
			color = 0
		}
	}
}

604 = {
	name="Hongo"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 17
			emblem = 0
			color = 5
			color = 16
			color = 0
		}
	}
}

605 = {
	name="Higo"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 37
			emblem = 0
			color = 5 # Sable (Black)
			color = 3 # Gules (Red)
			color = 0
		}
	}
}

606 = {
	name="Ijuin"
	culture = southern_kyushu
}

610 = {
	name="Ito"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 18
			emblem = 0
			color = 5
			color = 3
			color = 0
		}
	}
}

615 = {
	name="Tsuchimochi"
	culture = southern_kyushu
	coat_of_arms = {
		template = 0
		layer = {
			texture = 12
			texture_internal = 19
			emblem = 0
			color = 1
			color = 2
			color = 0
		}
	}
}