### Ainu Clans

1500 = {
	name="Shiripet"
	culture = donan_ainu
}

1501 = {
	name="Setanay"
	culture = donan_ainu
}

1502 = {
	name="Wokomshiri"
	culture = donan_ainu
}

1503 = {
	name="Pitoro"
	culture = donan_ainu
}

1510 = {
	name="Yamakushinai"
	culture = donan_ainu
}

1511 = {
	name="Ciroti"
	culture = donan_ainu
}

1512 = {
	name="Worikakoy"
	culture = donan_ainu
}

1513 = {
	name="Aptapet"
	culture = donan_ainu
}

1514 = {
	name="Saito"
	culture = donan_ainu
	used_for_random = no
}

1520 = {
	name="Sar"
	culture = doo_ainu
}

1521 = {
	name="Mopet"
	culture = doo_ainu
}

1522 = {
	name="Yupet"
	culture = doo_ainu
}

1523 = {
	name="Poronay"
	culture = doo_ainu
}

1524 = {
	name="Piratur"
	culture = doo_ainu
}

1525 = {
	name="Iskar"
	culture = doo_ainu
}

1530 = {
	name="Piporo"
	culture = doto_ainu
}

1531 = {
	name="Cipasir"
	culture = doto_ainu
}

1532 = {
	name="Sipicar"
	culture = doto_ainu
}

1533 = {
	name="Tokoro"
	culture = doto_ainu
}

1534 = {
	name="Sachinay"
	culture = doto_ainu
}

1535 = {
	name="Rikuunpet"
	culture = doto_ainu
}

1536 = {
	name="Yupet"
	culture = doto_ainu
}

1540 = {
	name="Kutcaro"
	culture = doto_ainu
}

1541 = {
	name="Kusiri"
	culture = doto_ainu
}

1542 = {
	name="Hartor"
	culture = doto_ainu
}

1543 = {
	name="Toya"
	culture = doto_ainu
}

1550 = {
	name="Onnepet"
	culture = doto_ainu
}

1551 = {
	name="Menashi"
	culture = doto_ainu
}

1552 = {
	name="Kusiri"
	culture = doto_ainu
}

1553 = {
	name="Shari"
	culture = doto_ainu
}

1560 = {
	name="Kinasir"
	culture = chishima_ainu
}

1561 = {
	name="Etuoroop"
	culture = chishima_ainu
}

1570 = {
	name="Kamchatka"
	culture = chishima_ainu
}

1571 = {
	name="Kamchatka"
	culture = chishima_ainu
}

1572 = {
	name="Kamchatka"
	culture = chishima_ainu
}

1580 = {
	name="Iskarpet"
	culture = doo_ainu
}

1581 = {
	name="Maske"
	culture = doo_ainu
}

1582 = {
	name="Yuparo"
	culture = doo_ainu
}

1590 = {
	name="Huranui"
	culture = dohoku_ainu
}

1591 = {
	name="Sipet"
	culture = dohoku_ainu
}

1592 = {
	name="Kamuy Kotan"
	culture = dohoku_ainu
}

1600 = {
	name="Hurpira"
	culture = doo_ainu
}

1601 = {
	name="Iwawnay"
	culture = doo_ainu
}

1602 = {
	name="Iochi"
	culture = doo_ainu
}

1603 = {
	name="Otanay"
	culture = doo_ainu
}

1610 = {
	name="Shiromkur"#Orokoti
	culture = karafuto_ainu
}

1611 = {
	name="Tabarakotan"
	culture = karafuto_ainu
}

1620 = {
	name="Pontukes"
	culture = karafuto_ainu
}

1621 = {
	name="Enrumkuomanay"
	culture = karafuto_ainu
}

1622 = {
	name="Tominay"
	culture = karafuto_ainu
}

1630 = {
	name="Yadan"#Kusunnay
	culture = karafuto_ainu
}

1631 = {
	name="Usoro"
	culture = karafuto_ainu
}

1640 = {
	name="Dowaha"
	culture = uilta
}

1650 = {
	name="Choming"#Ikutomo
	culture = nivkh
}

1651 = {
	name="Neode"#Otchishi
	culture = nivkh
}

### Clans in 1180

1700 = {
	name="Kumausi"
	culture = donan_ainu
}

1701 = {
	name="Kaminokuni"
	culture = donan_ainu
	used_for_random = no
}

1702 = {
	name="Matomai"
	culture = donan_ainu
	used_for_random = no
}

1703 = {
	name="Kameda"
	culture = donan_ainu
	used_for_random = no
}

1704 = {
	name="Setanay"
	culture = donan_ainu
	used_for_random = no
}

1705 = {
	name="Shakudo"
	culture = donan_ainu
	used_for_random = no
}

1706 = {
	name="Iwawnay"
	culture = donan_ainu
}

1707 = {
	name="Toya"
	culture = donan_ainu
}

1708 = {
	name="Taoromai"
	culture = doo_ainu
}

1709 = {
	name="Otai"
	culture = doo_ainu
	used_for_random = no
}

1710 = {
	name="Piratur"
	culture = doo_ainu
}

1711 = {
	name="Sipicar"
	culture = doo_ainu
	used_for_random = no
}

1712 = {
	name="Misoku"
	culture = doto_ainu
	used_for_random = no
}

1713 = {
	name="Sipa"
	culture = doto_ainu
	used_for_random = no
}

1714 = {
	name="Ponkinasir"
	culture = old_okhotsk
}

1715 = {
	name="Kamchatka"
	culture = old_okhotsk
	used_for_random = no
}

1716 = {
	name="Chudo"
	culture = doo_ainu
	used_for_random = no
}

1717 = {
	name="Amamsuke"
	culture = doo_ainu
}

1718 = {
	name="Ryuen"
	culture = dohoku_ainu
	used_for_random = no
}

1719 = {
	name="Hikoku"
	culture = dohoku_ainu
	used_for_random = no
}

1720 = {
	name="Okudo"
	culture = dohoku_ainu
	used_for_random = no
}

1721 = {
	name="Sumamoy"
	culture = doo_ainu
}

1722 = {
	name="Sikutut"
	culture = doo_ainu
}

1723 = {
	name="Takoi"
	culture = old_okhotsk
}

1724 = {
	name="Ruotak"
	culture = old_okhotsk
}

1725 = {
	name="Nayoro"
	culture = old_okhotsk
}

1726 = {
	name="Idoi"
	culture = old_okhotsk
}

1729 = {
	name="Usoro"
	culture = tohoku_ainu
}

1730 = {
	name="Yiliyu"
	culture = uilta
}

1731 = {
	name="Kusir"
	culture = doto_ainu
}

1750 = {
	name="Esausi"
	culture = donan_ainu
}

1751 = {
	name="Usor"
	culture = doo_ainu
}

1752 = {
	name="Satporopet"
	culture = doo_ainu
}

1753 = {
	name="Omu"
	culture = dohoku_ainu
}

1754 = {
	name="Atsam"
	culture = dohoku_ainu
}

1755 = {
	name="Nganivo"
	culture = nivkh
}

1756 = {
	name="Kuuji"
	culture = nivkh
}

1757 = {
	name="Noyausi"
	culture = nivkh
}

1758 = {
	name="Sirutor"
	culture = nivkh
}

1759 = {
	name="Cipesani"
	culture = nivkh
}
