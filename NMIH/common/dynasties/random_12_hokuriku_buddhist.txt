###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
# Dynasty data file for random characters.
# 
# @param culture hokuriku_buddhist
# @param id_range 22,200,000 - 22,399,999
# 
###############################################################################

## Honganji

22200189 = {
	name="Suzuki"
	culture = hokuriku_buddhist
}

22200190 = {
	name="Sugiura"
	culture = hokuriku_buddhist
}

22200191 = {
	name="Kubota"
	culture = hokuriku_buddhist
}

22200192 = {
	name="Shimotsuma"
	culture = hokuriku_buddhist
}

22200193 = {
	name="Uno"
	culture = hokuriku_buddhist
}

22200194 = {
	name="Shichiri"
	culture = hokuriku_buddhist
}

22200195 = {
	name="Ganchoji"
	culture = hokuriku_buddhist
}

22200196 = {
	name="Saikoji"
	culture = hokuriku_buddhist
}

22200197 = {
	name="Bukkoji"
	culture = hokuriku_buddhist
}

22200198 = {
	name="Senshuji"
	culture = hokuriku_buddhist
}

22200199 = {
	name="Zuisenji"
	culture = hokuriku_buddhist
}

22200200 = {
	name="Kinshikiji"
	culture = hokuriku_buddhist
}
