160 = {
	name="Hojo"
	culture = southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 12
			emblem = 0
			color = 5
			color = 21
			color = 0
		}
	}
}

161 = {
	name="Ise"
	culture = southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 13
			emblem = 0
			color = 5
			color = 27
			color = 0
		}
	}
}

162 = {
	name="Shimizu"
	culture = southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 14
			emblem = 0
			color = 5
			color = 30
			color = 0
		}
	}
}

163 = {
	name="Kasahara"
	culture = southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 15
			emblem = 0
			color = 5
			color = 32
			color = 0
		}
	}
}

164 = {
	name="Tominaga"
	culture = southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 10
			texture_internal = 16
			emblem = 0
			color = 5
			color = 35
			color = 0
		}
	}
}

165 = {
	name="Omori"
	culture = southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 0
			emblem = 0
			color = 5 # Black
			color = 4 # Light Green
			color = 0
		}
	}
	used_for_random=no
}

166={
	name="Daidoji"
	culture=central_kinai
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 0
			emblem = 0
			color = 5 # Black
			color = 2 # Blue
			color = 0
		}
	}
	used_for_random=no
}

167={
	name="Tame"
	culture=southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 17
			emblem = 0
			color = 5 # Black
			color = 53 #
			color = 0
		}
	}
	used_for_random=no
}

168={
	name="Araki"
	culture=southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 21
			emblem = 0
			color = 5 # Black
			color = 51 #
			color = 0
		}
	}
	used_for_random=no
}

169={
	name="Yamanaka"
	culture=southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 4
			emblem = 0
			color = 5 # Black
			color = 52 #
			color = 0
		}
	}
	used_for_random=no
}

170={
	name="Arakawa"
	culture=southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 17
			texture_internal = 31
			emblem = 0
			color = 5 # Black
			color = 50 #
			color = 0
		}
	}
	used_for_random=no
}

171={
	name="Aritake"
	culture=southern_kanto
	coat_of_arms = {
		template = 0
		layer = {
			texture = 16
			texture_internal = 73
			emblem = 0
			color = 5 # Black
			color = 49 #
			color = 0
		}
	}
	used_for_random=no
}
180={
	name="Naito"
	culture=southern_kanto
	coat_of_arms={
		template=0
		layer={
			texture=17
			texture_internal=3
			emblem=0
			color=5
			color=37
			color=0
		}
	}
}
181={
	name="Matsuda"
	culture=southern_kanto
	coat_of_arms={
		template=0
		layer={
			texture=19
			texture_internal=8
			emblem=0
			color=5
			color=39
			color=0
		}
	}
}
182={
	name="Ando"
	culture=southern_kanto
}
183={
	name="Itabeoka"
	culture=southern_kanto
	coat_of_arms={
		template=0
		layer={
			texture=16
			texture_internal=40
			emblem=0
			color=5
			color=43
			color=0
		}
	}
}