###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
# Dynasty data file for random characters.
# 
# @param culture [ANY]
# @param id_range 29,200,000 - 29,399,999
# 
###############################################################################
