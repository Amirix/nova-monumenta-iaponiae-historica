# This file holds the diseases from before The Reaper's due, with all the old values
# note the "rip = no" -- this will exclude a disease from The Reaper's Due disease database, so that we can have both versions of the disease
# if "rip" is not specified, the disease will be available in both version (with the same values in both) and doesn't need to be set twice

famine = {
	rip = no
	contagiousness = 0
	outbreak_chance = 0.01
	effect = {
#		local_tax_modifier = -0.25
		supply_limit = -7
#		levy_size = -0.5
#		max_attrition = 0.05
#		disease_defence = 0.2
	}
	icon = 1
	tooltip = FAMINE_INFO
	months = 30
	always_get_message = no
	color = { 84 171 176 }
	
	timeperiod = {
		start_date = 769.1.1
		end_date = 1452.1.1		

		one_only = no
	}
}
