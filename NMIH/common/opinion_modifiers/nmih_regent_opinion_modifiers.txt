#Regent request something to do
opinion_regent_requested = {
	opinion = -5
}
#Regent acted as tyrant
opinion_regent_tyrant = {
	opinion = -60
}
#Became a council member
opinion_became_council_member = {
	opinion = 25
}
#became commander
opinion_became_commander = {
	opinion = 10
	months = 120
}
#Fired from commander
opinion_fired_from_commander = {
	opinion = -10
	months = 120
}
#Refused a suggestion
opinion_refuse_suggestion = {
	opinion = -5
}

## Steward

#borrow money for interest
opinion_interest_money = {
	opinion = -5
}
#Gain profit by interest
opinion_gain_profit_interest = {
	opinion = 30
}
#no profit by interest
opinion_no_profit_interest = {
	opinion = -10
}
#Lose money by interest
opinion_lose_money_interest = {
	opinion = -30
}
#Saved by debt cancellation order
opinion_seve_debt_cancell = {
	opinion = 30
}
#losted by debt cancellation order
opinion_lost_debt_cancell = {
	opinion = -20
}