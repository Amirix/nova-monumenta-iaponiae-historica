###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
# Character data file for 10 - 14 century historical characters.
# 
# @param file_name 12C-02_northern_oshu_characters.txt
# @param culture northern_oshu
# @param id_range 2,030,000 - 2,059,999
# 
###############################################################################

#----------------------------------------------------------
## Oshu=Fujiwara Clan
#----------------------------------------------------------

2030000 = {
	name = "Kiyohira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 1004629	#Fujiwara Tsunekiyo
	mother = 2033026	# Abe Arika
	1056.1.1 = {
		birth = yes
	}
	1128.8.10 = {
		death = yes
	}
}

2030001 = {
	name = "Tsunemoto"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 1004629	#Fujiwara Tsunekiyo
	1058.1.1 = {
		birth = yes
	}
	1108.1.1 = {
		death = yes
	}
}

2030010 = {
	name = "Motohira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030000	#Fujiwara Kiyohira
	1086.1.1 = {
		birth = yes
	}
	1110.1.1={
		add_spouse=2033043	# Abe Ume
	}
	1157.4.29 = {
		death = yes
	}
}

2030011 = {
	name = "Iekiyo"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030000	#Fujiwara Kiyohira
	1087.1.1 = {
		birth = yes
	}
	1137.1.1 = {
		death = yes
	}
}

2030012 = {
	name = "Masahira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030000	#Fujiwara Kiyohira
	1089.1.1 = {
		birth = yes
	}
	1139.1.1 = {
		death = yes
	}
}

2030013 = {
	name = "Kiyotsuna"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030000	#Fujiwara Kiyohira
	1092.1.1 = {
		birth = yes
	}
	1142.1.1 = {
		death = yes
	}
}

2030014 = {
	name = "Kiyoko"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	female = yes
	father = 2030000	#Fujiwara Kiyohira
	1096.1.1 = {
		birth = yes
	}
	1146.1.1 = {
		death = yes
	}
}

2030015 = {
	name = "Toku"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	female = yes
	father = 2030000	#Fujiwara Kiyohira
	1090.1.1 = {
		birth = yes
	}
	1140.1.1 = {
		death = yes
	}
}

2030020 = {
	name="Hidehira"
	dynasty=1300 # Oshu Fujiwara
	dna="00vf00r0000"
	properties="aaaaaa"
	diplomacy=7
	martial=4
	stewardship=2
	intrigue=5
	learning=1
	religion="buddhist"
	culture="northern_oshu"
	add_trait="midas_touched"
	add_trait="genius"
	add_trait="gregarious"
	add_trait="charitable"
	add_trait="humble"
	add_trait="just"
	father = 2030010	#Fujiwara Motohira
	mother = 2033043	# Abe Ume
	1122.1.1={
		birth=yes
	}
	1154.1.1={
		add_spouse=1006321 # Fujiwara Tokuniko
	}
	1157.4.29={
		effect = {
			c_iwai = { ROOT = { capital = PREV } }
		}
	}
	1170.7.11={
		add_trait="jugoi_ge" # Chinjufu Shogun
	}
	1181.9.25={
		remove_trait="jugoi_ge"
		add_trait="jugoi_jo" # Mutsu no Kami
	}
	1187.11.30={
		death=yes
	}
}

2030021={
	name="Hidehisa"
	dynasty=1300 # Fujiwara
	father=2030010 # Fujiwara Motohira
#mother=
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1132.1.1={
		birth=yes
	}
	1180.1.1={
		effect = {
			c_inaka = { ROOT = { capital = PREV } }
		}
	}
	1192.1.1={
		death=yes
	}
}

2030030 = {
	name = "Kiyonobu"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030011	#Fujiwara Iekiyo
	1107.1.1 = {
		birth = yes
	}
	1157.1.1 = {
		death = yes
	}
}

2030040 = {
	name = "Toshihira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030013	#Fujiwara Kiyotsuna
	1122.1.1 = {
		birth = yes
	}
	1189.10.14 = {
		death = yes
	}
}

2030041 = {
	name = "Suehira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030013	#Fujiwara Kiyotsuna
	1125.1.1 = {
		birth = yes
	}
	1189.10.14 = {
		death = yes
	}
}

2030042 = {
	name = "Towako"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	female = yes
	father = 2030013	#Fujiwara Kiyotsuna
	1133.1.1 = {
		birth = yes
	}
	1183.1.1 = {
		death = yes
	}
}

2030050 = {
	name = "Kunihira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	add_trait = "bastard"
	add_trait="skilled_tactician"
	add_trait="brave"
	father = 2030020	#Fujiwara Hidehira
	1142.1.1 = {
		birth = yes
	}
	1189.9.21 = {
		death = {
			death_reason = death_battle
		}
	}
}

2030051 = {
	name = "Yasuhira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	add_trait="charismatic_negotiator"
	add_trait="administrator"
	add_trait="craven"
	father = 2030020 # Fujiwara Hidehira
	mother=1006321 # Fujiwara Tokuniko
	1155.1.1 = {
		birth = yes
	}
	1171.1.1={
		add_spouse=2000013 # Okawa Noriko
	}
	1189.10.14 = {
		death = {
			death_reason = death_murder
		}
	}
}

2030052 = {
	name = "Tadahira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	add_trait="tough_soldier"
	add_trait="ambitious"
	father = 2030020	#Fujiwara Hidehira
	1167.1.1 = {
		birth = yes
	}
	1179.1.1={
		effect={
			add_betrothal=2060074 # Sato Fujinoe
		}
	}
	1183.1.1={
		effect={
			break_betrothal=2060074 # Sato Fujinoe
		}
		add_spouse=2060074 # Sato Fujinoe
	}
	1189.8.9 = {
		death = {
			death_reason = death_execution
		}
	}
}

2030053 = {
	name = "Takahira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030020	#Fujiwara Hidehira
	1168.1.1 = {
		birth = yes
	}
	1201.4.4 = {
		death = {
			death_reason = death_execution
		}
	}
}

2030054 = {
	name = "Michihira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030020	#Fujiwara Hidehira
	1169.1.1 = {
		birth = yes
	}
	1189.8.1 = {
		death = {
			death_reason = death_battle
		}
	}
}

2030055 = {
	name = "Yorihira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030020	#Fujiwara Hidehira
	1170.1.1 = {
		birth = yes
	}
	1189.9.1 = {
		death = {
			death_reason=death_battle
			killer=130546 #Muto Sukeyori
		}
	}
}

2030060 = {
	name = "Yukinobu"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030030	#Fujiwara Kiyonobu
	1137.1.1 = {
		birth = yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1187.1.1 = {
		death = yes
	}
}

2030070 = {
	name = "Morohira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030040	#Fujiwara Toshihira
	1152.1.1 = {
		birth = yes
	}
	1189.10.14 = {
		death = yes
	}
}

2030071 = {
	name = "Kanehira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030040	#Fujiwara Toshihira
	1154.1.1 = {
		birth = yes
	}
	1189.10.14 = {
		death = yes
	}
}

2030072 = {
	name = "Tadahira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030040	#Fujiwara Toshihira
	1156.1.1 = {
		birth = yes
	}
	1189.10.14 = {
		death = yes
	}
}

2030080 = {
	name = "Tsunehira"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "northern_oshu"
	father = 2030041	#Fujiwara Suehira
	1145.1.1 = {
		birth = yes
	}
	1189.10.14 = {
		death = yes
	}
}

2030090 = {
	name = "Shokoku"
	dynasty = 1300	#Fujiwara
	#dna = ""
	#properties = ""
	#martial = 
	#diplomacy = 
	#stewardship = 
	#intrigue = 
	#learning = 
	religion = "buddhist"
	culture = "kinai_buddhist"
	father = 2030072	#Fujiwara Tadahira
	add_trait = "monk"
	1176.1.1 = {
		birth = yes
		effect = { set_character_flag = has_monk_name }
	}
	1226.1.1 = {
		death = yes
	}
}

2030100={
	name="Hidemoto"
	dynasty=1300 # Fujiwara
	father=2030021 # Fujiwara Hidehisa
#mother=
	religion="buddhist"
	culture="northern_oshu"
	1162.1.1={
		birth=yes
	}
	1222.1.1={
		death=yes
	}
}
2030101={
	name="Hidekiyo"
	dynasty=1300 # Fujiwara
	father=2030021 # Fujiwara Hidehisa
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1164.1.1={
		birth=yes
	}
	1220.1.1={
		death=yes
	}
}

2030110={
	name="Tokihira"
	dynasty=1300 # Fujiwara
	father=2030051 # Fujiwara Yasuhira
#mother=
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1170.1.1={
		birth=yes
	}
	1189.10.14={
		death=yes
	}
}

2030111={
	name="Hideyasu"
	dynasty=1300 # Fujiwara
	father=2030051 # Fujiwara Yasuhira
	mother=2000013 # Okawa Noriko
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1176.1.1={
		birth=yes
	}
	1189.10.14={
		death=yes
	}
}

2030112={
	name="Yasutaka"
	dynasty=1300 # Fujiwara
	father=2030051 # Fujiwara Yasuhira
	mother=2000013 # Okawa Noriko
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1179.1.1={
		birth=yes
	}
	1189.10.14={
		death=yes
	}
}
2030120={
	name="Hidekado"
	dynasty=1300 # Fujiwara
	father=2030100 # Fujiwara Hidemoto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1180.1.1={
		birth=yes
	}
	1220.1.1={
		death=yes
	}
}
2030121={
	name="Motonari"
	dynasty=1300 # Fujiwara
	father=2030100 # Fujiwara Hidemoto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1182.1.1={
		birth=yes
	}
	1220.1.1={
		death=yes
	}
}
2030122={
	name="Hidenao"
	dynasty=1300 # Fujiwara
	father=2030100 # Fujiwara Hidemoto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1184.1.1={
		birth=yes
	}
	1229.1.1={
		death=yes
	}
}
2030123={
	name="Motosue"
	dynasty=1300 # Fujiwara
	father=2030100 # Fujiwara Hidemoto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1186.1.1={
		birth=yes
	}
	1220.1.1={
		death=yes
	}
}
2030125={
	name="Yorihide"
	dynasty=1300 # Fujiwara
	father=2030122 # Fujiwara Hidenao
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1215.1.1={
		birth=yes
	}
	1275.1.1={
		death=yes
	}
}


#----------------------------------------------------------
## Retainers of the Oshu Fujiwara Clan
#----------------------------------------------------------

2032100={
	name="Sanemichi" # Fictional
	dynasty=7202 # Kiyohara
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1120.1.1={
		birth=yes
	}
	1180.1.1={
		death=yes
	}
}
2032101={
	name="Sanetoshi"
	dynasty=7202 # Kiyohara
	father=2032100 # 
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait = "fortune_builder"
	add_trait = "quick"
	1150.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1210.1.1={
		death=yes
	}
}
2032102={
	name="Sanemasa"
	dynasty=7202 # Kiyohara
	father=2032100 # 
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait = "fortune_builder"
	1155.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1215.1.1={
		death=yes
	}
}
2032200={
	name="Tohachi"
	dynasty=7203 # Tomo
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait = "tough_soldier"
	add_trait = "strong"
	1160.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1189.9.20={
		death=yes
	}
}
2032205={
	name="Jiro"
	dynasty=7253 # Akada
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="tough_soldier"
	1150.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1189.10.3={
		death=yes
	}
}
2032210={
	name="Heiroku"
	dynasty=7254 # Kon
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="tough_soldier"
	1152.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1189.10.3={
		death=yes
	}
}
2032215={
	name="Kurodayu"
	dynasty=7255 # Waka
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="tough_soldier"
	1154.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030020 # Fujiwara Hidehira
	}
	1189.10.2={
		death={
			death_reason = death_battle
		}
	}
}
2032220={
	name="Hidekazu"
	dynasty=7256 # Sato
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="tough_soldier"
	1140.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2030050 # Fujiwara Kunihira
	}
	1189.9.21={
		death={
			death_reason = death_battle
		}
	}
}
2032225={
	name="Kichiji"
	dynasty=7292 # Kaneuri
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="midas_touched"
	add_trait="quick"
	stewardship=9
	1120.1.1={
		birth=yes
	}
	1189.10.3={
		death=yes
	}
}

#----------------------------------------------------------
## Oshu=Abe Clan
#----------------------------------------------------------

2033000={
	name="Ieyoshi"
	dynasty=7252 # Abe
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	910.1.1={
		birth=yes
	}
	970.1.1={
		death=yes
	}
}
2033001={
	name="Chikayoshi"
	dynasty=7252 # Abe
	father=2033000 # Abe Ieyoshi
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	940.1.1={
		birth=yes
	}
	1000.1.1={
		death=yes
	}
}
2033005={
	name="Tadayoshi"
	dynasty=7252 # Abe
	father=2033001 # Abe Chikayoshi
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	970.1.1={
		birth=yes
	}
	1030.1.1={
		death=yes
	}
}
2033006={
	name="Tadayo"
	dynasty=7252 # Abe
	father=2033001 # Abe Chikayoshi
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	980.1.1={
		birth=yes
	}
	1040.1.1={
		death=yes
	}
}
2033010={
	name="Yoritoki"
	dynasty=7252 # Abe
	father=2033005 # Abe Tadayoshi
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="skilled_tactician"
	disallow_random_traits=yes
	1000.1.1={
		birth=yes
	}
	1057.8.1={
		death={
			death_reason = death_battle
		}
	}
}
2033011={
	name="Ryosho"
	dynasty=7252 # Abe
	father=2033005 # Abe Tadayoshi
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="scholarly_theologian"
	disallow_random_traits=yes
	1002.1.1={
		birth=yes
	}
	1070.1.1={
		death=yes
	}
}
2033012={
	name="Tamemoto"
	dynasty=7252 # Abe
	father=2033005 # Abe Tadayoshi
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1004.1.1={
		birth=yes
	}
	1070.1.1={
		death=yes
	}
}
2033020={
	name="Sadato"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="brilliant_strategist"
	add_trait="strong"
	add_trait="brave"
	disallow_random_traits=yes
	1019.1.1={
		birth=yes
	}
	1062.10.22={
		death={
			death_reason = death_battle
		}
	}
}
2033021={
	name="Muneto"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	add_trait="charismatic_negotiator"
	add_trait="poet"
	disallow_random_traits=yes
	1032.1.1={
		birth=yes
	}
	1108.3.18={
		death=yes
	}
}
2033022={
	name="Masato"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1033.1.1={
		birth=yes
	}
	1090.1.1={
		death=yes
	}
}
2033023={
	name="Ieto"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1034.1.1={
		birth=yes
	}
	1090.1.1={
		death=yes
	}
}
2033024={
	name="Shigeto"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1035.1.1={
		birth=yes
	}
	1062.10.22={
		death=yes
	}
}
2033025={
	name="Norito"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1036.1.1={
		birth=yes
	}
	1090.1.1={
		death=yes
	}
}
2033026={
	name="Arika"
	female=yes
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1037.1.1={
		birth=yes
	}
	1086.1.1={
		death=yes
	}
}
2033027={
	name="Nakaka"
	female=yes
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1038.1.1={
		birth=yes
	}
	1086.1.1={
		death=yes
	}
}
2033028={
	name="Ichika"
	female=yes
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1039.1.1={
		birth=yes
	}
	1086.1.1={
		death=yes
	}
}
2033029={
	name="Yukito"
	dynasty=7252 # Abe
	father=2033010 # Abe Yoritoki
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1040.1.1={
		birth=yes
	}
	1090.1.1={
		death=yes
	}
}
2033030={
	name="Chiyodoji"
	dynasty=7252 # Abe
	father=2033020 # Abe Sadato
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1048.1.1={
		birth=yes
	}
	1062.10.22={
		death=yes
	}
}
2033040={
	name="Muneyoshi"
	dynasty=7252 # Abe
	father=2033021 # Abe Muneto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1060.1.1={
		birth=yes
	}
	1120.1.1={
		death=yes
	}
}
2033041={
	name="Nakato"
	dynasty=7252 # Abe
	father=2033021 # Abe Muneto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1065.1.1={
		birth=yes
	}
	1125.1.1={
		death=yes
	}
}
2033042={
	name="Sueto"
	dynasty=7252 # Abe
	father=2033021 # Abe Muneto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1070.1.1={
		birth=yes
	}
	1130.1.1={
		death=yes
	}
}
2033043={
	name="Ume"
	female=yes
	dynasty=7252 # Abe
	father=2033021 # Abe Muneto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1080.1.1={
		birth=yes
	}
	1140.1.1={
		death=yes
	}
}
2033044={
	name="Hana"
	female=yes
	dynasty=7252 # Abe
	father=2033021 # Abe Muneto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1090.1.1={
		birth=yes
	}
	1150.1.1={
		death=yes
	}
}
2033050={
	name="Hideto"
	dynasty=7252 # Abe
	father=2033023 # Abe Ieto
#	mother=
	religion="buddhist"
	culture="dewa"
	disallow_random_traits=yes
	1060.1.1={
		birth=yes
	}
	1120.1.1={
		death=yes
	}
}
2033055={
	name="Suketo"
	dynasty=7252 # Abe
	father=2033050 # Abe Hideto
#	mother=
	religion="buddhist"
	culture="dewa"
	disallow_random_traits=yes
	1090.1.1={
		birth=yes
	}
	1150.1.1={
		death=yes
	}
}
2033060={
	name="Taketo"
	dynasty=7252 # Abe
	father=2033055 # Abe Suketo
#	mother=
	religion="buddhist"
	culture="dewa"
	1125.1.1={
		birth=yes
	}
	1189.9.24={
		death=yes
	}
}
2033065={
	name="Takeshige"
	dynasty=7252 # Abe
	father=2033060 # Abe Taketo
#	mother=
	religion="buddhist"
	culture="dewa"
	1155.1.1={
		birth=yes
	}
	1215.1.1={
		death=yes
	}
}
2033070={
	name="Yasuhira"
	dynasty=7252 # Abe
	father=2033025 # Abe Norito
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1070.1.1={
		birth=yes
	}
	1130.1.1={
		death=yes
	}
}
2033080={
	name="Takato"
	dynasty=7252 # Abe
	father=2033029 # Abe Yukito
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1064.1.1={
		birth=yes
	}
	1105.1.1={
		death=yes
	}
}
2033085={
	name="Kazuto"
	dynasty=7252 # Abe
	father=2033080 # Abe Takato
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	disallow_random_traits=yes
	1083.1.1={
		birth=yes
	}
	1121.1.1={
		death=yes
	}
}

#----------------------------------------------------------
## Nanbu Clan
#----------------------------------------------------------
2040100 = {
	name="Yukitomo"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="flamboyant_schemer"
	father=2360502 # Nanbu Mitsuyuki
	1180.1.1 = {
		birth=yes
		employer=120112 # Kagami Tomitsu
	}
	1250.1.1 = {
		death=yes
	}
}

2040101 = {
	name="Sanemitsu"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="flamboyant_schemer"
	father=2360502 # Nanbu Mitsuyuki
	1181.1.1 = {
		birth=yes
	}
	1255.2.3 = {
		death=yes
	}
}

2040102 = {
	name="Sanenaga"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2360502 # Nanbu Mitsuyuki
	1222.1.1 = {
		birth=yes
	}
	1297.10.19 = {
		death=yes
	}
}

2040103 = {
	name="Tomokiyo"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="flamboyant_schemer"
	father=2360502 # Nanbu Mitsuyuki
	1225.1.1 = {
		birth=yes
	}
	1275.1.1 = {
		death=yes
	}
}

2040104 = {
	name="Munekiyo"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="misguided_warrior"
	father=2360502 # Nanbu Mitsuyuki
	1228.1.1 = {
		birth=yes
	}
	1280.1.1 = {
		death=yes
	}
}

2040110 = {
	name="Tokizane"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040101 # Nanbu Sanemitsu
	1200.1.1 = {
		birth=yes
	}
	1280.1.1 = {
		death=yes
	}
}

2040120 = {
	name="Masamitsu"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040110 # Nanbu Tokizane
	1240.1.1 = {
		birth=yes
	}
	1265.9.3 = {
		death=yes
	}
}

2040121 = {
	name="Munetsune"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040110 # Nanbu Tokizane
	1246.1.1 = {
		birth=yes
	}
	1283.6.13 = {
		death=yes
	}
}

2040122 = {
	name="Masayuki"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040110 # Nanbu Tokizane
	1250.1.1 = {
		birth=yes
	}
	1300.1.1 = {
		death=yes
	}
}

2040123 = {
	name="Sanemasa"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040110 # Nanbu Tokizane
	1255.1.1 = {
		birth=yes
	}
	1310.1.1 = {
		death=yes
	}
}

2040124 = {
	name="Munezane"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040110 # Nanbu Tokizane
	1260.1.1 = {
		birth=yes
	}
	1320.1.1 = {
		death=yes
	}
}

2040125 = {
	name="Yoshimoto"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040110 # Nanbu Tokizane
	1263.1.1 = {
		birth=yes
	}
	1292.6.13 = {
		death=yes
	}
}

2040150 = {
	name="Yoshiyuki"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="misguided_warrior"
	father=2040125 # Nanbu Yoshimoto
	1280.1.1 = {
		birth=yes
	}
	1345.1.1 = {
		death=yes
	}
}

2040151={
	name="Muneyuki"
	dynasty=31 # Nanbu
	father=2040125 # Nanbu Yoshimoto
#mother=
	religion="buddhist"
	culture="koshin"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1283.1.1={
		birth=yes
	}
	1333.1.1={
		death=yes
	}
}

2040152={
	name="Sukeyuki"
	dynasty=31 # Nanbu
	father=2040125 # Nanbu Yoshimoto
#mother=
	religion="buddhist"
	culture="koshin"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1284.1.1={
		birth=yes
	}
	1334.1.1={
		death=yes
	}
}

2040154={
	name="Tokinaga"
	dynasty=31 # Nanbu
	father=2040122 # Nanbu Masayuki
#mother=
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1285.1.1={
		birth=yes
	}
	1345.1.1={
		death=yes
	}
}

2040155={
	name="Moroyuki"
	dynasty=31 # Nanbu
	father=2040122 # Nanbu Masayuki
#mother=#unknown
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1290.1.1={
		birth=yes
	}
	1338.6.10={
		death=yes
	}
}

2040156={
	name="Masanaga"
	dynasty=31 # Nanbu
	father=2040122 # Nanbu Masayuki
#mother=
	religion="buddhist"
	culture="northern_oshu"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	disallow_random_traits=yes
	1295.1.1={
		birth=yes
	}
	1360.1.1={
		death=yes
	}
}

2040160 = {
	name="Yoshihige"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="misguided_warrior"
	father=2040150 # Nanbu Yoshiyuki
	1298.1.1 = {
		birth=yes
	}
	1336.1.1 = {
		death=yes
	}
}

2040161 = {
	name="Shigetoki"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="misguided_warrior"
	father=2040150 # Nanbu Yoshiyuki
	1299.1.1 = {
		birth=yes
	}
	1333.7.4 = {
		death=yes
	}
}

2040162 = {
	name="Nobunaga"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040150 # Nanbu Yoshiyuki
	1302.1.1 = {
		birth=yes
	}
	1350.1.1 = {
		death=yes
	}
}

2040163 = {
	name="Tameshige"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040150 # Nanbu Yoshiyuki
	1305.1.1 = {
		birth=yes
	}
	1355.1.1 = {
		death=yes
	}
}

2040164 = {
	name="Sukenaga"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040150 # Nanbu Yoshiyuki
	1308.1.1 = {
		birth=yes
	}
	1360.1.1 = {
		death=yes
	}
}

2040165 = {
	name="Nobuiki"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040150 # Nanbu Yoshiyuki
	1309.1.1 = {
		birth=yes
	}
	1370.1.1 = {
		death=yes
	}
}

2040180 = {
	name="Masayuki"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="underhanded_rogue"
	father=2040162 # Nanbu Nobunaga
	1328.1.1 = {
		birth=yes
	}
	1388.11.17 = {
		death=yes
	}
}

2040190 = {
	name="Moriyuki"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="flamboyant_schemer"
	father=2040180 # Nanbu Masayuki
	1359.1.1 = {
		birth=yes
	}
	1437.5.13 = {
		death={
			death_reason=death_battle
		}
	}
}

2040200 = {
	name="Yoshimasa"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040190 # Nanbu Moriyuki
	1374.1.1 = {
		birth=yes
	}
	1440.8.9 = {
		death=yes
	}
}

2040201 = {
	name="Masamori"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040190 # Nanbu Moriyuki
	1378.1.1 = {
		birth=yes
	}
	1445.1.1 = {
		death=yes
	}
}

2040202 = {
	name="Sukemasa"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="naive_appeaser"
	father=2040190 # Nanbu Moriyuki
	1382.1.1 = {
		birth=yes
	}
	1442.1.1 = {
		death=yes
	}
}

2040203 = {
	name="Yukinaga"
	dynasty=31
	religion="buddhist"
	culture="northern_oshu"
	add_trait="amateurish_plotter"
	father=2040190 # Nanbu Moriyuki
	1385.1.1 = {
		birth=yes
	}
	1450.1.1 = {
		death=yes
	}
}

2040210={
	name="Sanetsugu"
	dynasty=31 # Nanbu
	father=2040102 # Nanbu Sanenaga
#	mother=
	religion="hokke_shu"
	culture="koshin"
	1250.1.1={
		birth=yes
	}
	1332.12.31={
		death=yes
	}
}

2040215={
	name="Nagatsugu"
	dynasty=31 # Nanbu
	father=2040210 # Nanbu Sanetsugu
#	mother=
	religion="hokke_shu"
	culture="koshin"
	1280.1.1={
		birth=yes
	}
	1352.1.1={
		death=yes
	}
}


# Nejo (Hachinohe) branch

2040301={
	name="Nobumasa"
	dynasty=31 # Nanbu
	father=2040156 # Nanbu Masanaga
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1320.1.1={
		birth=yes
	}
	1348.2.4={
		death=yes
	}
}

2040305={
	name="Nobumitsu"
	dynasty=31 # Nanbu
	father=2040301 # Nanbu Nobumasa
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1340.1.1={
		birth=yes
	}
	1376.2.13={
		death=yes
	}
}

2040306={
	name="Masamitsu"
	dynasty=31 # Nanbu
	father=2040301 # Nanbu Nobumasa
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1345.1.1={
		birth=yes
	}
	1427.3.1={
		death=yes
	}
}

2040310={
	name="Nagatsune"
	dynasty=31 # Nanbu
	father=2040305 # Nanbu Nobumitsu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1360.1.1={
		birth=yes
	}
	1410.1.1={
		death=yes
	}
}

2040311={
	name="Mitsutsune"
	dynasty=31 # Nanbu
	father=2040305 # Nanbu Nobumitsu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1362.1.1={
		birth=yes
	}
	1420.1.1={
		death=yes
	}
}

2040315={
	name="Nagayasu"
	dynasty=31 # Nanbu
	father=2040311 # Nanbu Mitsutsune
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1380.1.1={
		birth=yes
	}
	1440.1.1={
		death=yes
	}
}

2040320={
	name="Morikiyo"
	dynasty=31 # Nanbu
	father=2040315 # Nanbu Nagayasu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1400.1.1={
		birth=yes
	}
	1460.1.1={
		death=yes
	}
}

2040321={
	name="Kiyomasa"
	dynasty=49 # Niida
	father=2040315 # Nanbu Nagayasu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1402.1.1={
		birth=yes
	}
	1460.1.1={
		death=yes
	}
}

2040325={
	name="Masatsune"
	dynasty=50 # Hachinohe
	father=2040321 # Niida Kiyomasa
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1420.1.1={
		birth=yes
	}
	1480.1.1={
		death=yes
	}
}

2040326={
	name="Kiyotsugu"
	dynasty=49 # Niida
	father=2040321 # Niida Kiyomasa
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1422.1.1={
		birth=yes
	}
	1480.1.1={
		death=yes
	}
}



#----------------------------------------------------------
## Mutsu, Tono=Asonuma Clan
#----------------------------------------------------------

2050000 = {
	name="Tomokane"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2180750 # Asonuma Tomotsuna
	1335.1.1 = {
		birth="1335.1.1"
	}
	1385.1.1 = {
		death="1385.1.1"
	}
}

2050003 = {
	name="Hirotsuna"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050000 # Asonuma Tomokane
	1365.1.1 = {
		birth="1365.1.1"
	}
	1415.1.1 = {
		death="1415.1.1"
	}
}

2050006 = {
	name="Hideuji"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050003 # Asonuma Hirotsuna
	1395.1.1 = {
		birth="1395.1.1"
	}
	1445.1.1 = {
		death="1445.1.1"
	}
}

2050009 = {
	name="Mitsutsuna"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050006 # Asonuma Hideuji
	1425.1.1 = {
		birth="1425.1.1"
	}
	1475.1.1 = {
		death="1475.1.1"
	}
}

2050012 = {
	name="Morichika"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050009 # Asonuma Mitsutsuna
	1455.1.1 = {
		birth="1455.1.1"
	}
	1505.1.1 = {
		death="1505.1.1"
	}
}

2050013 = {
	name="Moritsuna"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050009 # Asonuma Mitsutsuna
	1460.1.1 = {
		birth="1460.1.1"
	}
	1510.1.1 = {
		death="1510.1.1"
	}
}

2050014 = {
	name="Moriyoshi"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050009 # Asonuma Mitsutsuna
	1465.1.1 = {
		birth="1465.1.1"
	}
	1515.1.1 = {
		death="1515.1.1"
	}
}

2050018 = {
	name="Chikasato"
	dynasty=41 # Asonuma
	religion="buddhist"
	culture="northern_oshu"
	father=2050012 # Asonuma Morichika
	1485.1.1 = {
		birth="1485.1.1"
	}
	1535.1.1 = {
		death="1535.1.1"
	}
}

#----------------------------------------------------------
## Mutsu, Hienuki Clan
#----------------------------------------------------------
2051000={
	name="Tameshige"
	dynasty=34 # Hienuki
	father=2121004 # Isa Tameie
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1205.1.1={
		birth=yes
	}
	1265.1.1={
		death=yes
	}
}

2051001={
	name="Mitsuie"
	dynasty=34 # Hienuki
	father=2051000 # Hienuki Tameshige
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1230.1.1={
		birth=yes
	}
	1290.1.1={
		death=yes
	}
}

2051005={
	name="Shigeyasu"
	dynasty=34 # Hienuki
	father=2051001 # Hienuki Mitsuie
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1255.1.1={
		birth=yes
	}
	1315.1.1={
		death=yes
	}
}

2051010={
	name="Tamemasa"
	dynasty=34 # Hienuki
	father=2051005 # Hienuki Shigeyasu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1280.1.1={
		birth=yes
	}
	1340.1.1={
		death=yes
	}
}

2051015={
	name="Ieshige"
	dynasty=34 # Hienuki
	father=2051010 # Hienuki Tamemasa
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1305.1.1={
		birth=yes
	}
	1365.1.1={
		death=yes
	}
}

2051020={
	name="Shigeto"
	dynasty=34 # Hienuki
	father=2051015 # Hienuki Ieshige
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1330.1.1={
		birth=yes
	}
	1390.1.1={
		death=yes
	}
}

2051025={
	name="Moriie"
	dynasty=34 # Hienuki
	father=2051020 # Hienuki Shigeto
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1355.1.1={
		birth=yes
	}
	1415.1.1={
		death=yes
	}
}

2051030={
	name="Tamechika"
	dynasty=34 # Hienuki
	father=2051025 # Hienuki Moriie
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1380.1.1={
		birth=yes
	}
	1440.1.1={
		death=yes
	}
}

2051035={
	name="Ietaka"
	dynasty=34 # Hienuki
	father=2051030 # Hienuki Tamechika
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1400.1.1={
		birth=yes
	}
	1460.1.1={
		death=yes
	}
}

2051040={
	name="Tamenaka"
	dynasty=34 # Hienuki
	father=2051035 # Hienuki Ietaka
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1420.1.1={
		birth=yes
	}
	1480.1.1={
		death=yes
	}
}

2051045={
	name="Tametaka"
	dynasty=34 # Hienuki
	father=2051040 # Hienuki Tamenaka
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1440.1.1={
		birth=yes
	}
	1490.1.1={
		death=yes
	}
}

#----------------------------------------------------------
## Mutsu, Hei Clan
#----------------------------------------------------------
2052000={
	name="Yukimitsu"
	dynasty=51 # Hei
#	father=
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1170.1.1={
		birth=yes
	}
	1219.1.1={
		death=yes
	}
}

2052001={
	name="Ietomo"
	dynasty=51 # Hei
	father=2052000 # Hei Yukimitsu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1200.1.1={
		birth=yes
	}
	1260.1.1={
		death=yes
	}
}

2052005={
	name="Mitsutomo"
	dynasty=51 # Hei
	father=2052001 # Hei Ietomo
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1230.1.1={
		birth=yes
	}
	1290.1.1={
		death=yes
	}
}

2052010={
	name="Mitsuyori"
	dynasty=51 # Hei
	father=2052005 # Hei Mitsutomo
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1260.1.1={
		birth=yes
	}
	1344.1.1={
		death=yes
	}
}

2052015={
	name="Chikamitsu"
	dynasty=51 # Hei
	father=2052010 # Hei Mitsuyori
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1290.1.1={
		birth=yes
	}
	1360.1.1={
		death=yes
	}
}

2052020={
	name="Tamenobu"
	dynasty=51 # Hei
	father=2052015 # Hei Chikamitsu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1330.1.1={
		birth=yes
	}
	1366.1.1={
		death=yes
	}
}

2052025={
	name="Hisatomo"
	dynasty=51 # Hei
	father=2052020 # Hei Tamenobu
#	mother=
	religion="buddhist"
	culture="northern_oshu"
	1360.1.1={
		birth=yes
	}
	1420.1.1={
		death=yes
	}
}
