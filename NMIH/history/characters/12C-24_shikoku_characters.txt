###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
# Character data file for 10 - 14 century historical characters.
# 
# @param file_name 12C-24_shikoku_characters.txt
# @param culture shikoku
# @param id_range 2690000 - 2719999
# 
###############################################################################

#----------------------------------------------------------
## Awata Clan
#----------------------------------------------------------

2690000={
	name="Shigehide"
	dynasty=7127 # Awata
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1100.1.1={
		birth=yes
	}
	1160.1.1={
		death=yes
	}
}


2690001={
	name="Shigeyoshi"
	dynasty=7127 # Awata
	father=2690000 # Awata Shigehide
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	add_trait="tough_soldier"
	add_trait="architect"
	add_trait="wroth"
	1130.1.1={
		birth=yes
	}
	1180.1.1={
		add_trait="jugoi_ge"
	}
	1185.4.25={
		death=yes
	}
}


2690002={
	name="Yoshito"
	dynasty=7127 # Awata
	father=2690000 # Awata Shigehide
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	add_trait="tough_soldier"
	1135.1.1={
		birth=yes
	}
	1185.4.25={
		death=yes
	}
}


2690003={
	name="Noriyoshii"
	dynasty=7127 # Awata
	father=2690001 # Awata Shigeyoshi
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	add_trait="tough_soldier"
	1160.1.1={
		birth=yes
	}
	1197.10.1={
		death=yes
	}
}


#----------------------------------------------------------
## Awa=Kondo Clan
#----------------------------------------------------------

2690100={
	name="Saiko"
	dynasty=7128 # Kondo
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1110.1.1={
		birth=yes
	}
	1177.6.30={
		death=yes
	}
}


2690101={
	name="Morotaka"
	dynasty=7128 # Kondo
	father=2690100 # Kondo Saiko
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1140.1.1={
		birth=yes
	}
	1177.7.6={
		death=yes
	}
}


2690102={
	name="Morotsune"
	dynasty=7128 # Kondo
	father=2690100 # Kondo Saiko
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1142.1.1={
		birth=yes
	}
	1177.7.6={
		death=yes
	}
}


2690103={
	name="Morohira"
	dynasty=7128 # Kondo
	father=2690100 # Kondo Saiko
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1144.1.1={
		birth=yes
	}
	1177.7.6={
		death=yes
	}
}


2690104={
	name="Hironaga"
	dynasty=7128 # Kondo
	father=2690100 # Kondo Saiko
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1146.1.1={
		birth=yes
	}
	1182.1.1={
		death=yes
	}
}


2690105={
	name="Chikaie"
	dynasty=7128 # Kondo
	father=2690100 # Kondo Saiko
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1150.1.1={
		birth=yes
	}
	1210.1.1={
		death=yes
	}
}


#----------------------------------------------------------
## Sanuki=Hayuka Clan
#----------------------------------------------------------

2690200={
	name="Suketaka"
	dynasty=7129 # Hayuka
	father=243537
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1120.1.1={
		birth=yes
	}
	1182.1.1={
		death=yes
	}
}


2690201={
	name="Shigetaka"
	dynasty=7129 # Hayuka
	father=2690200 # Hayuka Suketaka
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1140.1.1={
		birth=yes
	}
	1200.1.1={
		death=yes
	}
}


#----------------------------------------------------------
## Sanuki=Kozai Clan
#----------------------------------------------------------

2690300={
	name="Sukemitsu"
	dynasty=592 # Kozai
	father=2690200 # Hayuka Suketaka
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1145.1.1={
		birth=yes
	}
	1205.1.1={
		death=yes
	}
}


2690301={
	name="Nobusuke"
	dynasty=592 # Kozai
	father=2690300 # Kozai Sukemitsu
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1165.1.1={
		birth=yes
	}
	1225.1.1={
		death=yes
	}
}


#----------------------------------------------------------
## Iyo=Nii Clan
#----------------------------------------------------------

2690400={
	name="Morinobu"
	dynasty=7130 # Nii
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1130.1.1={
		birth=yes
	}
	1190.1.1={
		death=yes
	}
}


2690401={
	name="Nobuaki"
	dynasty=7130 # Nii
	father=2690400 # Nii Morinobu
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1160.1.1={
		birth=yes
	}
	1220.1.1={
		death=yes
	}
}


2690402={
	name="Nobuko"
	female=yes
	dynasty=7130 # Nii
	father=2690400 # Nii Morinobu
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1165.1.1={
		birth=yes
	}
	1225.1.1={
		death=yes
	}
}


#----------------------------------------------------------
## Iyo=Takaichi Clan
#----------------------------------------------------------

2690500={
	name="Takayoshi"
	dynasty=7131 # Takaichi
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1110.1.1={
		birth=yes
	}
	1170.1.1={
		death=yes
	}
}


2690501={
	name="Moriyoshi"
	dynasty=7131 # Takaichi
	father=2690500 # Takaichi Takayoshi
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1140.1.1={
		birth=yes
	}
	1185.1.1={
		death=yes
	}
}


2690502={
	name="Toshiyoshi"
	dynasty=7131 # Takaichi
	father=2690500 # Takaichi Takayoshi
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1145.1.1={
		birth=yes
	}
	1185.1.1={
		death=yes
	}
}


2690503={
	name="Hideyoshi"
	dynasty=7131 # Takaichi
	father=2690501 # Takaichi Moriyoshi
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1165.1.1={
		birth=yes
	}
	1185.1.1={
		death=yes
	}
}


2690504={
	name="Yasuyoshi"
	dynasty=7131 # Takaichi
	father=2690501 # Takaichi Moriyoshi
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1170.1.1={
		birth=yes
	}
	1185.1.1={
		death=yes
	}
}

2690505={
	name="Kiyonori"
	dynasty=7131 # Takechi
	father=2690501 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1160.1.1={
		birth=yes
	}
	1184.3.20={
		death=yes
	}
}


#----------------------------------------------------------
## Iyo=Tachibana Clan
#----------------------------------------------------------

2690600={
	name="Toyasu"
	dynasty=7132 # Tachibana
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	914.1.1={
		birth=yes
	}
	944.3.3={
		death=yes
	}
}


2690601={
	name="Yasuuji"
	dynasty=7132 # Tachibana
	father=2690600 # Tachibana Toyasu
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	944.1.1={
		birth=yes
	}
	1004.1.1={
		death=yes
	}
}


2690602={
	name="Morotaka"
	dynasty=7132 # Tachibana
	father=2690601 # Tachibana Yasuuji
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	980.1.1={
		birth=yes
	}
	1040.1.1={
		death=yes
	}
}


2690603={
	name="Yasumoto"
	dynasty=7132 # Tachibana
	father=2690602 # Tachibana Moronaga
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1015.1.1={
		birth=yes
	}
	1085.1.1={
		death=yes
	}
}


2690604={
	name="Kaneyuki"
	dynasty=7132 # Tachibana
	father=2690603 # Tachibana Yasumoto
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1050.1.1={
		birth=yes
	}
	1110.1.1={
		death=yes
	}
}


2690605={
	name="Yoshinori"
	dynasty=7132 # Tachibana
	father=2690604 # Tachibana Kaneyuki
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1080.1.1={
		birth=yes
	}
	1140.1.1={
		death=yes
	}
}


2690606={
	name="Kimimitsu"
	dynasty=7132 # Tachibana
	father=2690605 # Tachibana Yoshinori
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1105.1.1={
		birth=yes
	}
	1165.1.1={
		death=yes
	}
}


2690607={
	name="Kiminaga"
	dynasty=7132 # Tachibana
	father=2690606 # Tachibana Kimimitsu
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1130.1.1={
		birth=yes
	}
	1190.1.1={
		death=yes
	}
}


2690608={
	name="Kiminobu"
	dynasty=7132 # Tachibana
	father=2690606 # Tachibana Kimimitsu
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1135.1.1={
		birth=yes
	}
	1195.1.1={
		death=yes
	}
}


2690609={
	name="Kimitada"
	dynasty=7132 # Tachibana
	father=2690607 # Tachibana Kiminaga
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1160.1.1={
		birth=yes
	}
	1193.9.14={
		death=yes
	}
}


2690610={
	name="Kiminari"
	dynasty=7132 # Tachibana
	father=2690607 # Tachibana Kiminaga
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1162.1.1={
		birth=yes
	}
	1242.1.1={
		death=yes
	}
}

2690620={
	name="KusuNoUwa"
	female=yes
	dynasty=7132 # Tachibana
	father=2690610 # Tachibana Kiminari
#	mother=
	religion="buddhist"
	culture="southern_kanto"
	1190.1.1={
		birth=yes
	}
	1239.1.1={
		death=yes
	}
}

2690621={
	name="Kimiyoshi"
	dynasty=7132 # Tachibana
	father=2690610 # Tachibana Kiminari
#	mother=
	religion="buddhist"
	culture="southern_kanto"
	1210.1.1={
		birth=yes
	}
	1270.1.1={
		death=yes
	}
}

2690622={
	name="Kimikazu"
	dynasty=7132 # Tachibana
	father=2690610 # Tachibana Kiminari
#	mother=
	religion="buddhist"
	culture="southern_kanto"
	1224.1.1={
		birth=yes
	}
	1289.1.1={
		death=yes
	}
}

#----------------------------------------------------------
## Tosa=Aki Clan
#----------------------------------------------------------

2690700={
	name="Hiroyasu"
	dynasty=581 # Aki
	father=2690717
#	mother=
	religion="buddhist"
	culture="shikoku"
	1120.1.1={
		birth=yes
	}
	1190.1.1={
		death=yes
	}
}
2690701={
	name="Sanemitsu"
	dynasty=581 # Aki
	father=2690700 # Aki Hiroyasu
#	mother=
	religion="buddhist"
	culture="shikoku"
	1140.1.1={
		birth=yes
	}
	1185.4.25={
		death=yes
	}
}
2690702={
	name="Sanetoshi"
	dynasty=581 # Aki
	father=2690700 # Aki Hiroyasu
#	mother=
	religion="buddhist"
	culture="shikoku"
	1142.1.1={
		birth=yes
	}
	1185.4.25={
		death=yes
	}
}
2690703={
	name="Sanetsune"
	dynasty=581 # Aki
	father=2690700 # Aki Hiroyasu
#	mother=
	religion="buddhist"
	culture="shikoku"
	1145.1.1={
		birth=yes
	}
	1205.1.1={
		death=yes
	}
}
2690704={
	name="Sanekiyo"
	dynasty=581 # Aki
	father=2690703 # Aki Sanetsune
#	mother=
	religion="buddhist"
	culture="shikoku"
	1165.1.1={
		birth=yes
	}
	1225.1.1={
		death=yes
	}
}
2690705={
	name="Sanekuni"
	dynasty=581 # Aki
	father=2690704 # Aki Sanekiyo
#	mother=
	religion="buddhist"
	culture="shikoku"
	1185.1.1={
		birth=yes
	}
	1245.1.1={
		death=yes
	}
}
2690706={
	name="Sanetada"
	dynasty=581 # Aki
	father=2690705 # Aki Sanekuni
#	mother=
	religion="buddhist"
	culture="shikoku"
	1205.1.1={
		birth=yes
	}
	1265.1.1={
		death=yes
	}
}
2690707={
	name="Sanenobu"
	dynasty=581 # Aki
	father=2690706 # Aki Sanetada
#	mother=
	religion="buddhist"
	culture="shikoku"
	1225.1.1={
		birth=yes
	}
	1288.1.1={
		death=yes
	}
}
2690708={
	name="Tomonobu"
	dynasty=581 # Aki
	father=2690707 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1245.1.1={
		birth=yes
	}
	1305.1.1={
		death=yes
	}
}
2690709={
	name="Yasunobu"
	dynasty=581 # Aki
	father=2690707 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1247.1.1={
		birth=yes
	}
	1307.1.1={
		death=yes
	}
}

# before 1180

2690710={
	name="Yukitsune"
	dynasty=581 # Aki
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
	880.1.1={
		birth=yes
	}
	940.1.1={
		death=yes
	}
}
2690711={
	name="Yukiharu"
	dynasty=581 # Aki
	father=2690710 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	910.1.1={
		birth=yes
	}
	970.1.1={
		death=yes
	}
}
2690712={
	name="Yukikane"
	dynasty=581 # Aki
	father=2690711 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	940.1.1={
		birth=yes
	}
	1000.1.1={
		death=yes
	}
}
2690713={
	name="Kanezane"
	dynasty=581 # Aki
	father=2690712 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	970.1.1={
		birth=yes
	}
	1030.1.1={
		death=yes
	}
}
2690714={
	name="Sanechika"
	dynasty=581 # Aki
	father=2690713 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1000.1.1={
		birth=yes
	}
	1060.1.1={
		death=yes
	}
}
2690715={
	name="Tsunemichi"
	dynasty=581 # Aki
	father=2690714 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1030.1.1={
		birth=yes
	}
	1090.1.1={
		death=yes
	}
}
2690716={
	name="Sanehiro"
	dynasty=581 # Aki
	father=2690715 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1060.1.1={
		birth=yes
	}
	1120.1.1={
		death=yes
	}
}
2690717={
	name="Hirosada"
	dynasty=581 # Aki
	father=2690716 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1090.1.1={
		birth=yes
	}
	1150.1.1={
		death=yes
	}
}

# after 1300

2690720={
	name="Tomochika"
	dynasty=581 # Aki
	father=2690708 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1270.1.1={
		birth=yes
	}
	1330.1.1={
		death=yes
	}
}
2690721={
	name="Chikauji"
	dynasty=581 # Aki
	father=2690720 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1295.1.1={
		birth=yes
	}
	1355.1.1={
		death=yes
	}
}
2690722={
	name="Tadachika"
	dynasty=581 # Aki
	father=2690721 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1320.1.1={
		birth=yes
	}
	1380.1.1={
		death=yes
	}
}
2690723={
	name="Tadayuki"
	dynasty=581 # Aki
	father=2690722 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1345.1.1={
		birth=yes
	}
	1405.1.1={
		death=yes
	}
}
2690724={
	name="Motoshige"
	dynasty=581 # Aki
	father=2690723 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1370.1.1={
		birth=yes
	}
	1430.1.1={
		death=yes
	}
}
2690725={
	name="Motozane"
	dynasty=581 # Aki
	father=2690724 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1395.1.1={
		birth=yes
	}
	1439.1.1={
		death=yes
	}
}
2690730={
	name="Tadanobu"
	dynasty=581 # Aki
	father=2690709 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1262.1.1={
		birth=yes
	}
	1325.1.1={
		death=yes
	}
}
2690731={
	name="Tadahira"
	dynasty=581 # Aki
	father=2690730 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1280.1.1={
		birth=yes
	}
	1340.1.1={
		death=yes
	}
}
2690732={
	name="Yorimune"
	dynasty=581 # Aki
	father=2690731 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1297.1.1={
		birth=yes
	}
	1360.1.1={
		death=yes
	}
}
2690733={
	name="Tadayori"
	dynasty=581 # Aki
	father=2690732 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1315.1.1={
		birth=yes
	}
	1375.1.1={
		death=yes
	}
}
2690734={
	name="Munetoshi"
	dynasty=581 # Aki
	father=2690733 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1332.1.1={
		birth=yes
	}
	1395.1.1={
		death=yes
	}
}
2690735={
	name="Muneyasu"
	dynasty=581 # Aki
	father=2690734 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1350.1.1={
		birth=yes
	}
	1410.1.1={
		death=yes
	}
}
2690736={
	name="Munenaga"
	dynasty=581 # Aki
	father=2690735 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1370.1.1={
		birth=yes
	}
	1430.1.1={
		death=yes
	}
}
2690737={
	name="Yorifusa"
	dynasty=581 # Aki
	father=2690736 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1390.1.1={
		birth=yes
	}
	1435.1.1={
		death=yes
	}
}
2690738={
	name="Motofusa"
	dynasty=581 # Aki
	father=2690737 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1410.1.1={
		birth=yes
	}
	1440.1.1={
		death=yes
	}
}
2690739={
	name="Motonobu"
	dynasty=581 # Aki
	father=2690738 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	1430.1.1={
		birth=yes
	}
	1470.1.1={
		death=yes
	}
}

#----------------------------------------------------------
## Tosa=Hirata Clan
#----------------------------------------------------------

2690800={
	name="Toshito"
	dynasty=7133 # Hirata
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1132.1.1={
		birth=yes
	}
	1184.11.20={
		death=yes
	}
}


#----------------------------------------------------------
## Tosa=Hasuike Clan
#----------------------------------------------------------

2690900={
	name="Ietsuna"
	dynasty=7134 # Hasuike
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
#	martial=
#	diplomacy=
#	intrigue=
#	stewardship=
#	learning=
	1135.1.1={
		birth=yes
	}
	1184.11.20={
		death=yes
	}
}

#----------------------------------------------------------
## Kono Clan
#----------------------------------------------------------

2691000 = {
	name="Chikatsune" #42th
	dynasty=584 # Kono
	father=230259
	religion="buddhist"
	culture="shikoku"
	1080.1.1={
		birth=yes
	}
	1150.1.1={
		death=yes
	}
}

2691001 = {
	name="Chikakiyo" #43th
	dynasty=584 # Kono
	father=2691000
	religion="buddhist"
	culture="shikoku"
	1100.1.1={
		birth=yes
	}
	1170.1.1={
		death=yes
	}
}

2691002 = {
	name="Morichika"
	dynasty=584 # Kono
	father=2691000
	religion="buddhist"
	culture="shikoku"
	1105.1.1={
		birth=yes
	}
	1177.1.1={
		death=yes
	}
}

2691005 = {
	name="Michikiyo" #44th
	dynasty=584 # Kono
	father=2691001
	religion="buddhist"
	culture="shikoku"
	1130.1.1={
		birth=yes
	}
	1155.1.1={
		add_spouse=2660502
	}
	1180.9.8 = {
		add_claim="e_japan"
		effect = {
			spawn_unit = {
				province = 401 # Ochi
				owner = ROOT
				leader = ROOT
				troops = {
					light_infantry = { 600 600 }
					light_cavalry = { 200 200 }
					archers = { 300 300 }
				}
				attrition = 1.0
			}
		}
	}
	1181.2.27={
		death=yes
	}
}

2691006 = {
	name="Michisato"
	dynasty=584 # Kono
	father=2691001
	religion="buddhist"
	culture="shikoku"
	1135.1.1={
		birth=yes
	}
	1180.1.1={
		death=yes
	}
}

2691010 = {
	name="Michinobu" #45th
	dynasty=584 # Kono
	father=2691005
	mother=2660502
	religion="buddhist"
	culture="shikoku"
	1156.1.1={
		birth=yes
	}
	1179.1.1={
		add_spouse=2690402
	}
	1190.1.1={
		remove_spouse=2690402
		add_spouse=131664
	}
	1222.6.29={
		death=yes
	}
}

2691011 = {
	name="Michitsune"
	dynasty=584 # Kono
	father=2691005
	mother=2660502
	religion="buddhist"
	culture="shikoku"
	1158.1.1={
		birth=yes
	}
	1225.1.1={
		death=yes
	}
}

2691012 = {
	name="Michitaka"
	dynasty=584 # Kono
	father=2691005
	mother=2660502
	religion="buddhist"
	culture="shikoku"
	1160.1.1={
		birth=yes
	}
	1185.5.2={
		death={
		death_reason=death_battle
		}
	}
}

2691013 = {
	name="Michisuke"
	dynasty=584 # Kono
	father=2691005
	mother=2660502
	religion="buddhist"
	culture="shikoku"
	1162.1.1={
		birth=yes
	}
	1225.1.1={
		death=yes
	}
}

2691014 = {
	name="Michikazu"
	dynasty=584 # Kono
	father=2691005
	mother=2660502
	religion="buddhist"
	culture="shikoku"
	1163.1.1={
		birth=yes
	}
	1185.5.2={
		death={
		death_reason=death_battle
		}
	}
}

2691015 = {
	name="Nobuyoshi"
	dynasty=584 # Kono
	father=2691005
	mother=2660502
	religion="buddhist"
	culture="shikoku"
	1165.1.1={
		birth=yes
	}
	1225.1.1={
		death=yes
	}
}

2691020 = {
	name="Michitoshi"
	dynasty=584 # Kono
	father=2691010
	mother=2690402
	religion="buddhist"
	culture="shikoku"
	1180.1.1={
		birth=yes
	}
	1221.6.1={
		death=yes
	}
}

2691021 = {
	name="Michimasa"
	dynasty=584 # Kono
	father=2691010
	mother=131664 #hojo Tokimasa's daughter
	religion="buddhist"
	culture="shikoku"
	1190.1.1={
		birth=yes
	}
	1251.1.1={
		death=yes
	}
}

2691022 = {
	name="Michisue"
	dynasty=584 # Kono
	father=2691010
	mother=131664 #hojo Tokimasa's daughter
	religion="buddhist"
	culture="shikoku"
	1195.1.1={
		birth=yes
	}
	1221.6.1={
		death=yes
	}
}

2691023 = {
	name="Toshiko"
	dynasty=584 # Kono
	father=2691010
	mother=131664 #hojo Tokimasa's daughter
	female=yes
	religion="buddhist"
	culture="shikoku"
	1198.1.1={
		birth=yes
	}
	1221.6.1={
		death=yes
	}
}

2691024 = {
	name="Michihisa" #46th
	dynasty=584 # Kono
	father=2691010
	mother=131664 #hojo Tokimasa's daughter
	religion="buddhist"
	culture="shikoku"
	1200.1.1={
		birth=yes
	}
	1260.1.1={
		death=yes
	}
}

2691025 = {
	name="Michihiro"
	dynasty=584 # Kono
	father=2691010
	mother=131664 #hojo Tokimasa's daughter
	religion="buddhist"
	culture="shikoku"
	1202.1.1={
		birth=yes
	}
	1263.7.1={
		add_trait="monk"
		effect = { set_character_flag = has_monk_name }
		death=yes
	}
}

2691026 = {
	name="Michimune"
	dynasty=584 # Kono
	father=2691010
	mother=131664 #hojo Tokimasa's daughter
	religion="buddhist"
	culture="shikoku"
	1205.1.1={
		birth=yes
	}
	1221.6.1={
		death=yes
	}
}

2691030 = {
	name="Michitoki"
	dynasty=584 # Kono
	father=2691024
	religion="buddhist"
	culture="shikoku"
	1216.1.1={
		birth=yes
	}
	1281.8.14={
		death=yes
	}
}

2691031 = {
	name="Michitsugu" #47th
	dynasty=584 # Kono
	father=2691024
	religion="buddhist"
	culture="shikoku"
	1220.1.1={
		birth=yes
	}
	1270.1.1={
		death=yes
	}
}

2691032 = {
	name="Michiyuki"
	dynasty=584 # Kono
	father=2691024
	religion="buddhist"
	culture="shikoku"
	1235.1.1={
		birth=yes
	}
	1290.1.1={
		death=yes
	}
}

2691033 = {
	name="Michimori" #shuri no suke
	dynasty=584 # Kono
	father=2691024
	religion="buddhist"
	culture="shikoku"
	1245.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691034 = {
	name="Yukiko" #Kono Michiari's wife
	dynasty=584 # Kono
	father=2691024
	female=yes
	religion="buddhist"
	culture="shikoku"
	1259.1.1={
		birth=yes
	}
	1330.1.1={
		death=yes
	}
}

2691040 = {
	name="Michiari" #48th
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1250.1.1={
		birth=yes
	}
	1280.1.1={
		add_spouse=2691034
	}
	1320.1.1={
		death=yes
	}
}

2691041 = {
	name="Michiuji"
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1252.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691042 = {
	name="Michiyasu"
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1254.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691043 = {
	name="Michimasu"
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1256.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691044 = {
	name="Michinari"
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1258.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691045 = {
	name="Michiyasu"
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1260.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691046 = {
	name="Tokimichi"
	dynasty=584 # Kono
	father=2691031
	religion="buddhist"
	culture="shikoku"
	1262.1.1={
		birth=yes
	}
	1320.1.1={
		death=yes
	}
}

2691050 = {
	name="Michitomo"
	dynasty=584 # Kono
	father=2691025
	religion="buddhist"
	culture="shikoku"
	1230.1.1={
		birth=yes
	}
	1273.1.1={
		death=yes
	}
}

2691051 = {
	name="Michiuji" #Ippen, Patriarch of Jishu sect
	dynasty=584 # Kono
	learning=14
	father=2691025
	religion="buddhist"
	culture="shikoku"
	1239.3.21={
		birth=yes
	}
	1249.1.1={
		add_trait="monk"
		effect = { set_character_flag = has_monk_name }
		name="Chishin"
	}
	1289.9.9={
		death=yes
		name="Ippen"
	}
}

2691060 = {
	name="Michiuji"
	dynasty=584 # Kono
	learning=14
	father=2691050
	religion="buddhist"
	culture="shikoku"
	1261.1.1={
		birth=yes
	}
	1270.1.1={
		add_trait="monk"
		effect = { set_character_flag = has_monk_name }
		name="Shokai"
	}
	1323.3.22={
		death=yes
	}
}


#----------------------------------------------------------
## Awaji=Ama Clan
#----------------------------------------------------------

2691100={
	name="Tadakage"
	dynasty=7242 # Ama
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1140.1.1={
		birth=yes
	}
	1180.1.1={
		employer=120380 # Minamoto Yoshitsugu
	}
	1200.1.1={
		death=yes
	}
}

#----------------------------------------------------------
## Former Iyo=Murakami Clan
#----------------------------------------------------------

2691200={
	name="Sadakuni"
	dynasty=7243 # Murakami
	father=120222 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1090.1.1={
		birth=yes
	}
	1150.1.1={
		death=yes
	}
}
2691205={
	name="Kiyonaga"
	dynasty=7243 # Murakami
	father=2691200 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1125.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2691005 # Kono Michikiyo
	}
	1181.2.27={
		death=yes
	}
}
2691210={
	name="Yorifuyu"
	dynasty=7243 # Murakami
	father=2691205 # 
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1160.1.1={
		birth=yes
	}
	1180.1.1={
		employer=2691005 # Kono Michikiyo
	}
	1210.1.1={
		death=yes
	}
}

#----------------------------------------------------------
## Nii Clan, Chikakiyo branch
#----------------------------------------------------------

2691300={
	name="Chikakiyo"
	dynasty=7244 # Nii
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1150.1.1={
		birth=yes
	}
	1179.12.18={
		employer=110042 # Taira Munemori
	}
	1185.4.25={
		death=yes
	}
}

#----------------------------------------------------------
## Tosa=Yasu Clan
#----------------------------------------------------------

2691400={
	name="Yukimune"
	dynasty=7245 # Yasu
#	father=
#	mother=
	religion="buddhist"
	culture="shikoku"
	add_trait = "tough_soldier"
	1150.1.1={
		birth=yes
	}
	1166.1.1={
		give_nickname="nick_shichiro"
	}
	1180.1.1={
		employer=120077 # Minamoto Mareyoshi
	}
	1180.9.8={
		employer = 120075 # Minamoto Yoritomo
	}
	1212.1.1={
		death=yes
	}
}

