name = "Chokyo War"

casus_belli = {
	actor=17024 # Uesugi [Yamauchi] Akisada
	recipient=17043 # Uesugi [Ogigaya] Tomoyoshi
	casus_belli = sengoku_invasion
	landed_title=d_musashi
	date=1494.11.20
}

1494.11.20 = {
	add_attacker=17024 # Uesugi [Yamauchi] Akisada
	add_defender=17043 # Uesugi [Ogigaya] Tomoyoshi
}
1498.1.1 = {
	add_defender=16001 # Hojo Soun
}
1505.3.1 = {
	rem_attacker=17024 # Uesugi [Yamauchi] Akisada
	rem_defender=17043 # Uesugi [Ogigaya] Tomoyoshi
	rem_defender=16001 # Hojo Soun
}