name = "Revolt of Enya Okihisa"

casus_belli = {
	actor=51511 # Enya Okihisa
	recipient=51501 # Amago Tsunehisa
	casus_belli = claim
	landed_title=k_sanin
	date=1530.1.1
}

1530.1.1 = {
	add_attacker=51511 # Enya Okihisa
	add_defender=51501 # Amago Tsunehisa
}

1534.8.10 = {
	rem_attacker=51511 # Enya Okihisa
	rem_defender=51501 # Amago Tsunehisa
}