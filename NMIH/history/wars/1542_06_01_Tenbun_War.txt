name = "Tenbun War"

casus_belli = {
	actor=1001 # Date Tanemune
	recipient=1002 # Date Harumune
	casus_belli = claim
	landed_title=d_oshu_shugo
	date=1542.6.1
}

1542.6.1 = {
	add_attacker=1001 # Date Tanemune

	add_attacker=6902 # Tamura Takaaki
	add_attacker=7302 # Nihonmatsu Ieyasu
	add_defender=1002 # Date Harumune
}
1546.5.22 = {
	rem_attacker=7302 # Nihonmatsu Ieyasu
	add_attacker=7303 # Nihonmatsu Yoshiuji
}
1548.9.1 = {
	rem_attacker=1001 # Date Tanemune

	rem_attacker=6902 # Tamura Takaaki
	rem_attacker=7303 # Nihonmatsu Yoshiuji
	rem_defender=1002 # Date Harumune
}