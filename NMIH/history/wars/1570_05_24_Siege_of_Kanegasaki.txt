name = "Siege of Kanegasaki"

casus_belli = {
	actor = 29120 # Oda Nobunaga
	recipient = 30011 # Asakura Yoshikage
	casus_belli=sengoku_invasion
	landed_title=d_echizen
	date=1570.5.24
}

1570.5.24 = {
	add_attacker = 29120 # Oda Nobunaga
	add_defender = 30011 # Asakura Yoshikage
}
1570.6.3 ={
	rem_attacker = 29120 # Oda Nobunaga
	rem_defender = 30011 # Asakura Yoshikage
}