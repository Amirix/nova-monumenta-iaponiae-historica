name = "Genki War"

casus_belli = {
	actor = 29120 # Oda Nobunaga
	recipient = 43015 # Azai Nagamasa
	casus_belli=minor_sengoku_conquest
	landed_title=c_azai
	date=1570.6.14
}

1570.6.14 = {
	add_attacker = 29120 # Oda Nobunaga
	add_attacker = 24006 # Tokugawa Ieyasu
	add_defender = 43015 # Azai Nagamasa
	add_defender = 30011 # Asakura Yoshikage
}
1573.9.16 ={
	rem_defender = 30011 # Asakura Yoshikage
}
1573.10.6 ={
	rem_attacker = 29120 # Oda Nobunaga
	rem_attacker = 24006 # Tokugawa Ieyasu
	rem_defender = 43015 # Azai Nagamasa
}