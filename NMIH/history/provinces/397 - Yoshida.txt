# 397 - Yoshida
# Echizen

# County Title
title = c_yoshida

# Settlements
max_settlements = 3
#b_yoshida1 = castle # Muraoka
#b_yoshida2 = temple # Heisenji

#b_yoshida3 = castle # Shii
b_yoshida4 = temple # Eiheiji
#b_yoshida5 = castle # Kawai
#b_yoshida6 = temple # Johoji
#b_yoshida7 = castle # Morita
#b_yoshida8 = castle # Katsuyama

b_yoshida9 = castle # Fujishima


# Misc
culture = western_hokuriku
religion = buddhist


# History
1100.1.1={
	capital=b_yoshida9
}
1400.1.1={
	remove_settlement=b_yoshida9 # Fujishima
	b_yoshida1 = castle # Muraoka
	capital=b_yoshida1 # Muraoka
}
1460.1.1 = { religion = ikko_shu }
