# 291 - Toyoura
# Nagato

# County Title
title = c_toyoura

# Settlements
max_settlements = 7

b_toyoura1 = castle # Chofu
b_toyoura2 = city # Akamagaseki

#b_toyoura3 = castle # Katsuyama
#b_toyoura4 = castle # Yata
#b_toyoura5 = city # Hiju
#b_toyoura6 = city # Senzaki
#b_toyoura7 = temple # Sumiyoshi
#b_toyoura8 = temple # Fukawa

# Misc
culture = western_chugoku
religion = buddhist

# History
1520.1.1 = { religion = ikko_shu }
