# 332 - Kuwahara
# Osumi

# County Title
title = c_kuwahara

# Settlements
max_settlements = 4
b_kuwahara1 = castle 		# Hishikari
#b_kuwahara2 = city	 		# Kajiki

#b_kuwahara3 = temple		# Kurino
#b_kuwahara4 = castle 		# Tara
#b_kuwahara5 = castle 		# Yoshimatsu
#b_kuwahara6 = castle		# Makizono
#b_kuwahara7 = castle 		# Mizobe
#b_kuwahara8 = castle	 	# Yokogawa

b_kuwahara9 = castle 		# Kamo


# Misc
culture = southern_kyushu
religion = buddhist


# History
1100.1.1={
	capital=b_kuwahara9
}