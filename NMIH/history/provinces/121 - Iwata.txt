# 121 - Iwata
# Totomi

# County Title
title = c_iwata

# Settlements
max_settlements = 4
#b_iwata1 = castle 		# Horikoshi
#b_iwata2 = castle 		# Kuno
b_iwata3 = city		# Mitsuke

#b_iwata4 = castle 		# Kinosaki
#b_iwata5 = castle 		# Yashiroyama
#b_iwata6 = castle		# Sagisaka
b_iwata7 = castle 		# Asaba
#b_iwata8 = castle	 	# Mukasa


# Misc
culture = tokai
religion = buddhist


# History
1100.1.1={
	capital=b_iwata7
}
1331.1.1={
	b_iwata3 = castle	# Mitsuke
	b_iwata6 = castle	# Sagisaka
	capital=b_iwata3
	remove_settlement=b_iwata7
}
1493.1.1={
	b_iwata2=castle # Kuno
}

