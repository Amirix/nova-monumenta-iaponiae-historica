# 685 - Furibetsu
# Chishima

# County Title
title = c_furibetsu

# Settlements
max_settlements = 1
b_furibetsu1 = tribal 		# Furibetsu

#b_furibetsu2 = city		# Oito
#b_furibetsu3 = city		# Tennei
#b_furibetsu4 = city		# Toyohama
#b_furibetsu5 = city		# Guya
#b_furibetsu6 = city		# Tomakarausu
#b_furibetsu7 = city		# Kankekarausu
#b_furibetsu8 = city		# Matoro

# Misc
culture = doto_ainu
religion = ainu_animism

# History
100.1.1 = {
	culture = old_okhotsk
}

1200.1.1 = {
	culture = doto_ainu
}
