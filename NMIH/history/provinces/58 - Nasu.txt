# 58 - Nasu

# Shimotsuke

# County Title
title = c_nasu

# Settlements
max_settlements = 6

b_nasu1 = castle		# Karasuyama
b_nasu2 = castle		# Nasu
b_nasu3 = castle		# Fukuwara

#b_nasu4 = castle		# Otawara
#b_nasu5 = city			# Kanda
#b_nasu6 = city			# Kurobane
#b_nasu7 = temple		# Unganji
#b_nasu8 = temple		# Genshoji

# Misc
culture = northern_kanto
religion = buddhist

# History
1100.1.1={
	capital=b_nasu3
}
1428.1.1={
	capital=b_nasu1
}