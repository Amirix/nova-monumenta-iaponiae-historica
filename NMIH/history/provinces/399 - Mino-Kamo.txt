# 399 - Mino-Kamo
# Mino

# County Title
title = c_mino_kamo

# Settlements
max_settlements = 3
#b_mino_kamo1 = castle 			# Kawabe
#b_mino_kamo2 = castle 			# Yaotsu

#b_mino_kamo3 = castle			# Kajita
#b_mino_kamo4 = castle 			# Yoneda
b_mino_kamo5 = castle 			# Dohora
#b_mino_kamo6 = castle 			# Shirakawa
#b_mino_kamo7 = castle 			# Sakahogi
#b_mino_kamo8 = castle	 		# Magushi

b_mino_kamo9 = castle 			# Hachiya

# Misc
culture = western_chubu
religion = buddhist


# History
1100.1.1={
	capital=b_mino_kamo9
}
1331.1.1={
	remove_settlement=b_mino_kamo9 # Hachiya
	b_mino_kamo3 = castle # Kajita
	capital=b_mino_kamo3 # Kajita
}