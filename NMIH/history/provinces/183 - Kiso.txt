# 183 - Kiso
# Shinano

# County Title
title = c_kiso

# Settlements
max_settlements = 2
#b_kiso1 = castle 		# Kisofukushima

#b_kiso2 = castle 		# Suhara
#b_kiso3 = castle		# Tsumagome
#b_kiso4 = castle 		# Komaruyama
#b_kiso5 = castle 		# Otaki
#b_kiso6 = castle		# Okuwa
b_kiso7 = castle 		# Kiso
#b_kiso8 = castle	 	# Azuma


# Misc
culture = koshin
religion = buddhist


# History
1100.1.1={
	capital=b_kiso7
}
1532.1.1={
	b_kiso1 = castle # Kisofukushima
	capital=b_kiso1 # Kisofukushima
	remove_settlement=b_kiso7 # Kiso
}
