# 88 - Hinai

# Ugo

# County Title
title = c_hinai

# Settlements
max_settlements = 3

#b_hinai1 = castle		# Tokko
#b_hinai2 = castle		# Odate
#b_hinai3 = castle		# Sasadate
#b_hinai4 = castle		# Yagihashi
#b_hinai5 = castle		# Hanaoka
#b_hinai6 = city		# Hinai
#b_hinai7 = temple		# Ouda
#b_hinai8 = temple		# Houshuin

b_hinai9 = castle		# Nie

# Misc
culture = dewa
religion = buddhist

# History
1100.1.1={
	capital=b_hinai9
}
1189.10.14={
	b_hinai1 = castle # Tokko
	capital=b_hinai1 # Tokko
	remove_settlement=b_hinai9 # Nie
}