# 625 - Otaru
# Shiribeshi

# County Title
title = c_otaru

# Settlements
max_settlements = 3
b_otaru1 = tribal 		# Otarunai

#b_otaru2 = city		# Asari
#b_otaru3 = city 		# Zenibako
#b_otaru4 = city 		# Hariusu
#b_otaru5 = city 		# Kumausu
#b_otaru6 = city		# Nyusen
#b_otaru7 = city		# Shiomidai
#b_otaru8 = city		# Wakatake


# Misc
culture = doo_ainu
religion = ainu_animism


# History
100.1.1 = {
	culture = hokkaido_ainu
}

900.1.1 = {
	culture = doo_ainu
}
1600.1.1 = {
	b_otaru1 = city
}
1690.1.1 = {
	culture = hokkaido_wajin
	religion = buddhist
}

