# 369 - Namegata

#  Hitachi

# County Title
title = c_namegata

# Settlements
max_settlements = 3

#b_namegata1 = castle	# Tamatsukuri
#b_namegata2 = castle	# Aso

b_namegata3 = castle	# Shimazaki
#b_namegata4 = castle	# Tonagi
b_namegata5 = castle		# Namegata
#b_namegata6 = city		# Odaka
#b_namegata7 = temple	# Choshoji
#b_namegata8 = temple	# Oonama

# Misc
culture = northern_kanto
religion = buddhist

# History
1100.1.1={
	capital=b_namegata5 # Namegata
}
1300.1.1={
	capital=b_namegata3 # Shimazaki
	b_namegata1 = castle	# Tamatsukuri
	remove_settlement=b_namegata5 # Namegata
}
1591.3.1={
	b_namegata6 = castle # Odaka
	capital=b_namegata6
	remove_settlement=b_namegata1 # Tamatsukuri
	remove_settlement=b_namegata3 # Shimazaki
}