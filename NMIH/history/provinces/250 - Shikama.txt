# 250 - Shikama
# Harima

# County Title
title = c_shikama

# Settlements
max_settlements = 7

#b_shikama1 = castle # Himeji
#b_shikama2 = castle # Gochaku
b_shikama3 = temple # Aga
b_shikama4 = city # Shikama
b_shikama5 = castle # Shikata
#b_shikama6 = temple # Kanki
#b_shikama7 = city # Fuku
#b_shikama8 = castle # Chudoshi

b_shikama9 = castle # Kokufu


# Misc
culture = sanyo
religion = buddhist

# History
1100.1.1={
	capital=b_shikama9
}
1331.1.1={
	remove_settlement=b_shikama9 # Kokufu
	b_shikama2 = castle # Gochaku
	capital=b_shikama2 # Gochaku
}
1490.1.1 = { religion = ikko_shu }
