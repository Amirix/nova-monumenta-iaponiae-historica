# 367 - Nihonmatsu

# Iwashiro

# County Title
title = c_nihonmatsu

# Settlements
max_settlements = 4
b_nihonmatsu1 = castle		# Nihonmatsu
#b_nihonmatsu2 = castle		# Obama
#b_nihonmatsu3 = castle		# Kunugiyama
#b_nihonmatsu4 = castle		# Shionomatsu
b_nihonmatsu5 = castle		# Adachi
#b_nihonmatsu6 = city		# Motomiya
#b_nihonmatsu7 = temple		# Okitsushima
#b_nihonmatsu8 = temple		# Adatara

# Misc
culture = southern_oshu
religion = buddhist

# History
1100.1.1={
	capital=b_nihonmatsu5
}
1331.1.1={
	b_nihonmatsu5 = city		# Adachi
	capital=b_nihonmatsu1
}
1534.1.1 = { culture = southern_oshu }
