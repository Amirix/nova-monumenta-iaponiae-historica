# 311 - Mizuma
# Chikugo

# County Title
title = c_mizuma

# Settlements
max_settlements = 6
#b_mizuma1 = castle 			# Okawa
b_mizuma2 = city 			# Enokizu
b_mizuma3 = temple			# Mimata

#b_mizuma4 = castle 		# Kawaguchi
#b_mizuma5 = castle 		# Omizo
#b_mizuma6 = castle			# Kisaki
#b_mizuma7 = castle 		# Torikai
#b_mizuma8 = castle	 		# Jojima

b_mizuma9 = castle 			# Mizuma

# Misc
culture = central_kyushu
religion = buddhist


# History
1100.1.1={
	capital=b_mizuma9
}
1331.1.1={
	remove_settlement=b_mizuma9 # Mizuma
	b_mizuma1 = castle # Okawa
	capital=b_mizuma1 # Okawa
}
1570.1.1 = { religion = ikko_shu }
