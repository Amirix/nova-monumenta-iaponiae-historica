# 343 - Izumi
# Satsuma

# County Title
title = c_izumi

# Settlements
max_settlements = 3
b_izumi1 = castle 		# Akune
b_izumi2 = castle		# Izumi

#b_izumi3 = temple		# Nagashima
#b_izumi4 = castle 		# Akasegawa
#b_izumi5 = castle 		# Noda
#b_izumi6 = castle		# Takaono
#b_izumi7 = castle 		# Wakimoto
#b_izumi8 = castle	 	# Euchi


# Misc
culture = southern_kyushu
religion = buddhist


# History
1100.1.1={
	capital=b_izumi2
}
1331.1.1={
	b_izumi2 = city # Izumi
	capital=b_izumi1 # Akune
}