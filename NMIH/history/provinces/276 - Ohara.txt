# 276 - Ohara
# Izumo

# County Title
title = c_ohara

# Settlements
max_settlements = 4

b_ohara1 = temple # Yokota
#b_ohara2 = castle # Daisai

#b_ohara3 = castle # Mitoya
#b_ohara4 = castle # Ushio
#b_ohara5 = city # Kisuki
#b_ohara6 = castle # Maki
#b_ohara7 = castle # Akana
#b_ohara8 = castle # Sase

b_ohara9 = castle # Asayama

# Misc
culture = sanin
religion = buddhist

# History

1100.1.1={
	capital=b_ohara9
}
1331.1.1={
	remove_settlement=b_ohara9 # Asayama
	b_ohara2 = castle # Daisai
	capital=b_ohara2 # Daisai
}