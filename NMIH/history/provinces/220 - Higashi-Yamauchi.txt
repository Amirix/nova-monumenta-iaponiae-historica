# 220 - Yamabe # Higashi-Yamauchi
# Yamato

# County Title
title = c_yamabe

# Settlements
max_settlements = 2
#b_yamabe1 = castle # Imaichi
#b_yamabe2 = castle # Yagyu

#b_yamabe3 = castle # Tamonyama
b_yamabe4 = castle # Furuichi
#b_yamabe5 = castle # Tsubakio
#b_yamabe6 = castle # Kubonosho
#b_yamabe7 = castle # Mikadani
#b_yamabe8 = castle # Kiriyama
b_yamabe9 = castle # Tai


# Misc
culture = southern_kinai
religion = buddhist


# History
1180.1.1={
	capital=b_yamabe9 # Tai
}
1331.1.1={
	remove_settlement=b_yamabe9 # Tai
	b_yamabe2=castle # Yagyu
	capital=b_yamabe4 # Furuichi
}
