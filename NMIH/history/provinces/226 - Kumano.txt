# 226 - Kumano
# Kii

# County Title
title = c_kumano

# Settlements
max_settlements = 3
b_kumano1 = temple 		# Kumano

#b_kumano2 =  castle		# Akagi
#b_kumano3 = city			# Shingu
#b_kumano4 = castle 		# Arima
#b_kumano5 = castle 		# Shimosato
#b_kumano6 = castle		# Katsuyama
#b_kumano7 = castle 		# Sabe
#b_kumano8 = castle	 	# Ukui


# Misc
culture = southern_kinai
religion = buddhist


# History
1331.1.1={
	b_kumano3 = castle # Shingu
	capital=b_kumano3 # Shingu
	remove_settlement=b_kumano1 # Kumano
}