# 37 - Tsukui
# Sagami

# County Title
title = c_tsukui

# Settlements
max_settlements = 2
b_tsukui1 = castle 			# Tsukui

#b_tsukui2 = city	 		# Nakano
#b_tsukui3 = city			# Yose
#b_tsukui4 = castle 		# Fusumada
#b_tsukui5 = city	 		# Yoshino
#b_tsukui6 = temple			# Kounji
#b_tsukui7 = castle 		# Nakazawa
#b_tsukui8 = castle	 		# Sawai


# Misc
culture = southern_kanto
religion = buddhist


# History
