# 215 - Yoshino
# Yamato

# County Title
title = c_yoshino

# Settlements
max_settlements = 3
b_yoshino1 = castle # Kamiichi
b_yoshino2 = castle # Shimoichi

#b_yoshino3 = castle # Kitayama
#b_yoshino4 = castle # Totsukawa
#b_yoshino5 = castle # Ogawa
#b_yoshino6 = castle # Oyodo
#b_yoshino7 = castle # Akino
#b_yoshino8 = castle # Shirogane
#b_yoshino9 = castle # Ryumon


# Misc
culture = southern_kinai
religion = buddhist


# History
1100.1.1={
	b_yoshino9=castle # Ryumon
	capital=b_yoshino9 # Ryumon
}
1331.1.1={
	capital=b_yoshino1 # Kamiichi
}