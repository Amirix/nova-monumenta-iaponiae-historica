# 112 - Saku
# Shinano

# County Title
title = c_saku

# Settlements
max_settlements = 5
#b_saku1 = castle 		# Odai
#b_saku2 = castle 		# Uminokuchi

#b_saku3 = castle		# Komuro
#b_saku4 = castle 		# Ari
b_saku5 = castle 		# Hiraka
#b_saku6 = castle		# Iwao
#b_saku7 = castle 		# Oi
b_saku8 = castle	 	# Nenoi

b_saku9 = castle	 	# Mochizuki


# Misc
culture = koshin
religion = buddhist


# History
1000.1.1={
	capital=b_saku8 # Nenoi/Hiraka
}
1184.3.4={
	b_saku7 = castle 		# Oi
	remove_settlement=b_saku5	# Hiraka
}
1285.12.15={
	capital=b_saku7 		# Oi
}
1436.1.1={
	b_saku5 = castle 		# Hiraka/Ashida
}
