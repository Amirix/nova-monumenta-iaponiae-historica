# 698 - Honto
# Maoka

# County Title
title = c_honto

# Settlements
max_settlements = 2
b_honto1 = tribal 		# Honto

#b_honto2 = city		# Naihoro
#b_honto3 = city		# Koni
#b_honto4 = city		# Kaiba
#b_honto5 = city		# Minaminayoshi
#b_honto6 = city		# Oko
#b_honto7 = city		# Tobushi
#b_honto8 = city		# Kenushi

# Misc
culture = karafuto_ainu
religion = ainu_animism

# History
100.1.1 = {
	culture = old_okhotsk
}

1200.1.1 = {
	culture = karafuto_ainu
}
