# 607 - Kayabe
# Oshima

# County Title
title = c_kayabe

# Settlements
max_settlements = 3
b_kayabe1 = tribal 		# Kayabe

#b_kayabe2 = city 		# Koyasu
#b_kayabe3 = city 		# Toi
#b_kayabe4 = city 		# Shirikinai
#b_kayabe5 = city 		# Osatsube
#b_kayabe6 = city		# Nodaoi
#b_kayabe7 = city		# Mori
#b_kayabe8 = city		# Shikabe


# Misc
culture = donan_ainu
religion = ainu_animism


# History
100.1.1 = {
	culture = hokkaido_ainu
}

900.1.1 = {
	culture = donan_ainu
}

1600.1.1 = {
	culture = hokkaido_wajin
	religion = buddhist
	b_kayabe1 = city
}