# 251 - Akashi
# Harima

# County Title
title = c_akashi

# Settlements
max_settlements = 5

#b_akashi1 = castle # Akashi
b_akashi2 = city # Takasago
#b_akashi3 = castle # Kakogawa
b_akashi4 = castle # Uozumi
#b_akashi5 = temple # Taisan
#b_akashi6 = castle # Hashitani
#b_akashi7 = castle # Funage
#b_akashi8 = city # Tarumi

# Misc
culture = sanyo
religion = buddhist

# History
1100.1.1={
	capital=b_akashi4
}
1331.1.1={
	remove_settlement=b_akashi4 # Uozumi
	b_akashi1 = castle # Akashi
	capital=b_akashi1 # Akashi
}
