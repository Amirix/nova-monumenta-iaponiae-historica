# 202 - Ikaruga
# Tanba

# County Title
title = c_ikaruga

# Settlements
max_settlements = 3
#b_ikaruga1 = castle 		# Iden
#b_ikaruga2 = castle 		# Kanbayashi

#b_ikaruga3 = city			# Mononobe
#b_ikaruga4 = castle 		# Ayabe
b_ikaruga5 = castle 		# Uesugi
#b_ikaruga6 = temple		# Ankokuji
#b_ikaruga7 = castle 		# Kurimura
#b_ikaruga8 = castle	 	# Miyasaka


# Misc
culture = northern_kinai
religion = buddhist


# History
1100.1.1={
	capital=b_ikaruga5
}
1331.1.1={
	b_ikaruga1 = castle # Iden
	b_ikaruga2 = castle # Kanbayashi
	capital=b_ikaruga1 # Iden
	remove_settlement=b_ikaruga5 # Uesugi
}
1565.1.1={
	b_ikaruga4 = castle # Ayabe
	capital=b_ikaruga4 # Ayabe
	remove_settlement=b_ikaruga1 # Iden
}

