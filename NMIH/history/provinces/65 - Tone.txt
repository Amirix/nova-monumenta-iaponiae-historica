# 65 - Tone

# Kozuke

# County Title
title = c_tone

# Settlements
max_settlements = 2

#b_tone1 = castle		# Numata

#b_tone2 = castle		# Nagurumi
#b_tone3 = castle		# Minakami
#b_tone4 = castle		# Katashina
#b_tone5 = castle		# Ogawa
#b_tone6 = castle		# Ishikura
#b_tone7 = castle		# Myotokuji
#b_tone8 = castle		# Nagaizaka

b_tone9 = castle		# Tone


# Misc
culture = western_kanto
religion = buddhist

# History
1100.1.1={
	capital=b_tone9
}
1185.1.1={
	b_tone1 = castle # Numata
	capital=b_tone1 # Numata
	remove_settlement=b_tone9 # Tone
}