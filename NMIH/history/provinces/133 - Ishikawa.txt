# 133 - Ishikawa

# Kaga

# County Title
title = c_ishikawa

# Settlements
max_settlements = 7

#b_ishikawa1 = castle		# Togashi
b_ishikawa2 = castle			# Nonoichi
#b_ishikawa3 = castle		# Matsuto
#b_ishikawa4 = city			# Yokoe
#b_ishikawa5 = castle	# Tokumaru

#b_ishikawa6 = castle		# Kurita
#b_ishikawa7 = castle		# Oshino
#b_ishikawa8 = castle		# Yamakawa

b_ishikawa9 = castle		# Hayashi/Motoyoshi

# Misc
culture = western_hokuriku
religion = buddhist

# History
1100.1.1={
	capital=b_ishikawa9
}
1221.7.6={
	capital=b_ishikawa2 		# Nonoichi
	b_ishikawa9 = city		# Motoyoshi
}
1331.1.1={
	b_ishikawa3 = castle 		# Matsuto
	b_ishikawa8 = castle		# Yamakawa
}
1460.1.1 = { religion = ikko_shu }

1580.5.2={
	capital=b_ishikawa3		# Matsuto
}

