# 695 - Nagahama
# Toyohara

# County Title
title = c_nagahama

# Settlements
max_settlements = 2
b_nagahama1 = tribal 		# Nagahama

#b_nagahama2 = city		# Shiretoko
#b_nagahama3 = city		# Tobuchi
#b_nagahama4 = city		# Rebunbetsu
#b_nagahama5 = city		# Yaman
#b_nagahama6 = city		# Okobachi
#b_nagahama7 = city		# Arakuri
#b_nagahama8 = city		# Sotoshiretoko

# Misc
culture = karafuto_ainu
religion = ainu_animism

# History
100.1.1 = {
	culture = old_okhotsk
}

1200.1.1 = {
	culture = karafuto_ainu
}
