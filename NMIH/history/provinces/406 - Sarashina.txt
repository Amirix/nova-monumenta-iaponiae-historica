# 406 - Sarashina
# Shinano

# County Title
title = c_sarashina

# Settlements
max_settlements = 2
#b_sarashina1 = castle 		# Sarashina

b_sarashina2 = castle 		# Murakami
#b_sarashina3 = castle		# Yokota
#b_sarashina4 = castle 		# Ooka
#b_sarashina5 = castle 		# Kawanakajima
#b_sarashina6 = castle		# Nakatsu
#b_sarashina7 = castle 		# Mashima
#b_sarashina8 = castle	 	# Yahata


# Misc
culture = koshin
religion = buddhist


# History
1260.1.1={
	b_sarashina1 = castle 		# Sarashina/Makinoshima
}
1460.1.1 = { religion = ikko_shu }
1556.1.1={
	capital = b_sarashina1 # Makinoshima
	remove_settlement=b_sarashina2	# Murakami
}

