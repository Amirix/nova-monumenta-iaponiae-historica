# 395 - Hakusan

# Kaga

# County Title
title = c_hakusan

# Settlements
max_settlements = 3

b_hakusan1 = temple		# Hakusan

#b_hakusan2 = castle	# Torigoe
#b_hakusan3 = castle	# Agehara
#b_hakusan4 = castle	# Takakura
#b_hakusan5 = castle	# Matsuo
#b_hakusan6 = temple	# Seisewa
#b_hakusan7 = castle	# Funaoka
#b_hakusan8 = castle	# Yamauchi

# Misc
culture = western_hokuriku
religion = buddhist

# History
1360.1.1={
	b_hakusan8 = castle	# Yamauchi
	capital= b_hakusan8	# Yamauchi
	remove_settlement= b_hakusan1
}
1460.1.1 = { religion = ikko_shu }

1488.1.1={
	b_hakusan2 = castle	# Torigoe
	capital= b_hakusan2	# Torigoe
	remove_settlement= b_hakusan8
}