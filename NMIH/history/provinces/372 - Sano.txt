# 372 - Sano

# Shimotsuke

# County Title
title = c_sano

# Settlements
max_settlements = 4

#b_sano1 = castle		# Karasawayama
b_sano3 = castle		# Asonuma
b_sano2 = castle		# Sano

#b_sano4 = castle		# Tsubakida
#b_sano5 = city			# Akami
#b_sano6 = city			# Aso
#b_sano7 = temple		# Soshuji
#b_sano8 = temple		# Asahimori

# Misc
culture = northern_kanto
religion = buddhist

# History
1100.1.1={
	capital=b_sano2
}
1450.1.1={
	b_sano1 = castle # Karasawayama
	capital=b_sano1 # Karasawayama
	remove_settlement=b_sano2 # Sano
}