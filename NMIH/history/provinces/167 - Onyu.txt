# 167 - Onyu
# Wakasa

# County Title
title = c_onyu

# Settlements
max_settlements = 5
#b_onyu1 = castle # Nochise
b_onyu2 = city # Obama
b_onyu3 = castle # Awaya
b_onyu4 = city # Takahama

#b_onyu5 = castle # Aoi
#b_onyu6 = castle # Nata
#b_onyu7 = city # Sehama
#b_onyu8 = castle # Nishizu

b_onyu9 = castle # Tara

# Misc
culture = northern_kinai
religion = buddhist


# History
1100.1.1={
	capital=b_onyu9
}
1331.1.1={
	remove_settlement=b_onyu9 # Tara
	b_onyu1 = castle # Nochise
	capital=b_onyu1 # Nochise
}