# 644 - Kabato
# Ishikari

# County Title
title = c_kabato

# Settlements
max_settlements = 2
b_kabato1 = tribal 		# Kami-Kabato
b_kabato2 = tribal		# Shimo-Kabato

#b_kabato3 = city		# Tsukigata
#b_kabato4 = city		# Toppu
#b_kabato5 = city		# Satsubinai
#b_kabato6 = city		# Tsurunuma
#b_kabato7 = city		# Osokinai
#b_kabato8 = city		# Hashimoto


# Misc
culture = doo_ainu
religion = ainu_animism


# History
100.1.1 = {
	culture = hokkaido_ainu
}

900.1.1 = {
	culture = doo_ainu
}

1700.1.1 = {
	b_kabato1 = city
	b_kabato2 = city
}
