# 4 - Kita

# Mutsu

# County Title
title = c_kita

# Settlements
max_settlements = 3

#b_kita1 = castle	# Shichinohe
#b_kita2 = castle	# Kakizaki
#b_kita3 = castle	# Rokunohe
#b_kita4 = castle	# Tsukinoki
#b_kita5 = city		# Noheji
#b_kita6 = city		# Tanabe
#b_kita7 = temple	# Osoreyama
#b_kita8 = temple	# Chibiki

b_kita9 = tribal	# Usori

# Misc
culture = northern_oshu
religion = buddhist

# History
1100.1.1={
	capital=b_kita9
}
1189.10.3={
	b_kita1 = castle # Shichinohe
	b_kita7 = temple	# Osoreyama
	capital=b_kita1 # Shichinohe
	culture = northern_oshu
	religion = buddhist
	remove_settlement=b_kita9 # Usori
}
1534.1.1 = { culture = northern_oshu }
