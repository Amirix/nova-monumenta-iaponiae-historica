# 404 - Ihara
# Suruga

# County Title
title = c_ihara

# Settlements
max_settlements = 3
b_ihara1 = castle 		# Kanbara
#b_ihara2 = castle 		# Ejiri

#b_ihara3 = castle		# Okitsu
#b_ihara4 = temple 		# Seikenji
#b_ihara5 = castle 		# Shimizu
#b_ihara6 = castle		# Yui
#b_ihara7 = castle 		# Ojima
#b_ihara8 = castle	 	# Utsubusa
b_ihara9 = castle	 	# Kikkawa


# Misc
culture = tokai
religion = buddhist


# History
1100.1.1={
#	b_ihara9=castle # Kikkawa
	capital=b_ihara9 # Kikkawa / Ihara
}
1331.1.1={
	capital=b_ihara1
}
1570.1.1={
	remove_settlement=b_ihara9
	b_ihara2=castle # Ejiri
	b_ihara5 = city 		# Shimizu
	capital=b_ihara2
	remove_settlement=b_ihara1
}