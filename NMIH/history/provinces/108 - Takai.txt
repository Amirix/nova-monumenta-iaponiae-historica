# 108 - Takai
# Shinano

# County Title
title = c_takai

# Settlements
max_settlements = 3
#b_takai1 = castle 		# Suzaka

#b_takai2 = castle 		# Suda
#b_takai3 = castle		# Hoshina
#b_takai4 = castle 		# Obuse
b_takai5 = castle 		# Inoue
#b_takai6 = castle		# Takai
#b_takai7 = castle 		# Yamada
#b_takai8 = castle	 	# Hino


# Misc
culture = koshin
religion = buddhist


# History
1100.1.1={
	capital=b_takai5
}
