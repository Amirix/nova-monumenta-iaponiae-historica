# 593 - Kunigami
# Okinawa

# County Title
title = c_kunigami

# Settlements
max_settlements = 3
b_kunigami1 = castle 		# Nagijin
#b_kunigami2 = castle 		# Nago

#b_kunigami3 = city		# Haneji
#b_kunigami4 = city 		# Motobu
b_kunigami5 = castle 		# Kunigami
#b_kunigami6 = castle		# Kin
#b_kunigami7 = city 		# Ogimi
#b_kunigami8 = city	 	# Kushi


# Misc
culture = okinawa
religion = buddhist

# History
1322.1.1={
	b_kunigami2 = castle 		# Nago
}