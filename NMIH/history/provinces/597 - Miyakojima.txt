# 597 - Miyakojima
# Sakishima

# County Title
title = c_miyakojima

# Settlements
max_settlements = 3
b_miyakojima1 = castle 		# Miyako

#b_miyakojima2 = castle 	# Irabu
#b_miyakojima3 = castle 	# Tarama
#b_miyakojima4 = castle 	# Shimoji
#b_miyakojima5 = castle 	# Sunagawa
#b_miyakojima6 = castle		# Ikema
#b_miyakojima7 = castle		# Kurima
#b_miyakojima8 = castle		# Ogami


# Misc
culture = sakishima
religion = buddhist


# History
1360.1.1={
	b_miyakojima4 = castle 	# Shimoji
}
1532.1.1={
	remove_settlement = b_miyakojima4
}