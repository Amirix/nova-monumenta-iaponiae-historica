# 62 - Ashikaga

# Shimotsuke

# County Title
title = c_ashikaga

# Settlements
max_settlements = 4

b_ashikaga1 = castle	# Ashikaga
#b_ashikaga2 = castle	# Kanno

#b_ashikaga3 = castle	# Ryogaizan
#b_ashikaga4 = castle	# Yaba
#b_ashikaga5 = city		# Nakasato
b_ashikaga6 = castle		# Yanada
#b_ashikaga7 = temple	# Bannaji
#b_ashikaga8 = temple	# Kabasakiji

# Misc
culture = northern_kanto
religion = buddhist

# History
1100.1.1={
	capital=b_ashikaga1
}
1465.1.1={
	b_ashikaga2 = castle # Kanno
	capital=b_ashikaga2 # Kanno
	remove_settlement=b_ashikaga6 # Yanada
}
1500.1.1={
	capital=b_ashikaga1 # Ashikaga
}
#1534.1.1 = { culture = northern_oshu }

