# 386 - Kami-Ina
# Shinano

# County Title
title = c_kami_ina

# Settlements
max_settlements = 4
#b_kami_ina1 = castle 		# Takato
b_kami_ina2 = castle 		# Fujisawa

#b_kami_ina3 = castle		# Minowa
#b_kami_ina4 = castle 		# Fukuyo
#b_kami_ina5 = castle 		# Oide
#b_kami_ina6 = castle		# Iijima
b_kami_ina7 = castle 		# Odagiri
#b_kami_ina8 = castle	 	# Matsushima


# Misc
culture = koshin
religion = buddhist


# History
1100.1.1={
	capital=b_kami_ina7
}
1374.1.1={
	remove_settlement=b_kami_ina7 # Odagiri/Ogawara
	b_kami_ina1 = castle # Takato
	capital=b_kami_ina1 # Takato
}
1400.1.1={
	remove_settlement=b_kami_ina2 # Fujisawa
	b_kami_ina4 = castle 		# Fukuyo
}