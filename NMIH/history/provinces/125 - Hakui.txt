# 125 - Hakui

# Noto

# County Title
title = c_hakui

# Settlements
max_settlements = 4

#b_hakui1 = castle		# Tsuboyama
#b_hakui2 = city			# Shio

#b_hakui3 = castle		# Suemori
#b_hakui4 = castle		# Horimatsu
#b_hakui5 = castle		# Sugawara
b_hakui6 = castle		# Tokuda
#b_hakui7 = castle		# Sueyoshi
#b_hakui8 = city		# Jike

b_hakui9 = temple		# Keta


# Misc
culture = western_hokuriku
religion = buddhist

# History
1100.1.1={
	capital=b_hakui6
}
1331.1.1={
	b_hakui7 = castle # Sueyoshi
	capital=b_hakui7 # Sueyoshi
	remove_settlement=b_hakui6 # Tokuda
}
1460.1.1 = { religion = ikko_shu }
