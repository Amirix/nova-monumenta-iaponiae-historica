# 642 - Sapporo
# Ishikari

# County Title
title = c_sapporo

# Settlements
max_settlements = 7
b_sapporo1 = tribal 		# Kami-Sapporo
b_sapporo2 = tribal		# Shimo-Sapporo
b_sapporo3 = tribal		# Naiho
b_sapporo4 = tribal		# Shinoro
b_sapporo5 = tribal		# Shumamappu
b_sapporo6 = tribal		# Shimo-Tsuishikari
b_sapporo7 = tribal		# Kami-Tsuishikari

#b_sapporo8 = city		# Teine


# Misc
culture = doo_ainu
religion = ainu_animism


# History
100.1.1 = {
	culture = hokkaido_ainu
}
900.1.1 = {
	culture = doo_ainu
}
1600.1.1 = {
	b_sapporo1 = city
	b_sapporo2 = city
}
1650.1.1 = {
	b_sapporo3 = city
	b_sapporo4 = city
}
1700.1.1 = {
	b_sapporo5 = city
	b_sapporo6 = city
}
1730.1.1 = {
	b_sapporo7 = city
}
1855.1.1 = {
	culture = hokkaido_wajin
	religion = buddhist
}
