# 373 - Kawachi

# Shimotsuke

# County Title
title = c_kawachi

# Settlements
max_settlements = 6

b_kawachi1 = castle		# Utsunomiya
b_kawachi2 = castle		# Okamoto

#b_kawachi3 = castle	# Tagesan
#b_kawachi4 = castle	# Takou
#b_kawachi5 = city		# Kaminokawa
#b_kawachi6 = city		# Shirasawa
#b_kawachi7 = temple	# Futaarayama
#b_kawachi8 = temple	# Seiganji

# Misc
culture = northern_kanto
religion = buddhist

# History