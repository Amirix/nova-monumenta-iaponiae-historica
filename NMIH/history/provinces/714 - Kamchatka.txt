# 714 - Kamchatka
# Kamchatka

# County Title
title = c_kamchatka

# Settlements
max_settlements = 1
b_kamchatka1 = tribal 		# Kamchatka

#b_kamchatka2 = city		# Kamchatka
#b_kamchatka3 = city		# Kamchatka
#b_kamchatka4 = city		# Kamchatka
#b_kamchatka5 = city		# Kamchatka
#b_kamchatka6 = city		# Kamchatka
#b_kamchatka7 = city		# Kamchatka
#b_kamchatka8 = city		# Kamchatka

# Misc
culture = chishima_ainu
religion = ainu_animism

# History