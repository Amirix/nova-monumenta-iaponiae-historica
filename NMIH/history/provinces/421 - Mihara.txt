# 421 - Mihara
# Awaji

# County Title
title = c_mihara

# Settlements
max_settlements = 3

#b_mihara1 = castle 	# Ichimura

#b_mihara2 = castle # Ama
#b_mihara3 = castle # Gasyu
#b_mihara4 = castle # Shitori
b_mihara5 = castle 	# Fukura
#b_mihara6 = castle # Hirota
#b_mihara7 = temple # Nushima
#b_mihara8 = castle # Enami

# Misc
culture = shikoku
religion = buddhist

# History
1100.1.1={
	capital=b_mihara5
}
1186.1.1={
	b_mihara1 = castle 	# Ichimura
	capital=b_mihara1 # Ichimura
	remove_settlement=b_mihara5 # Fukura
}