# 405 - Shimo-Minochi
# Shinano

# County Title
title = c_shimo_minochi

# Settlements
max_settlements = 2
#b_shimo_minochi1 = castle 		# Iiyama

#b_shimo_minochi2 = castle 		# Minochi
#b_shimo_minochi3 = castle		# Sento
#b_shimo_minochi4 = castle 		# Shirosaka
#b_shimo_minochi5 = castle 		# Tokiwa
#b_shimo_minochi6 = castle		# Sakai
#b_shimo_minochi7 = castle 		# Yanagihara
#b_shimo_minochi8 = castle	 	# Akitsu

b_shimo_minochi9 = castle 		# Kasahara
#b_shimo_minochi10 = castle 		# Nakano


# Misc
culture = koshin
religion = buddhist


# History
1100.1.1={
	capital=b_shimo_minochi9 # Kasahara/Shikumi
}

1400.9.1={
	b_shimo_minochi1 = castle # Iiyama
	capital=b_shimo_minochi1 # Iiyama
	remove_settlement=b_shimo_minochi9 # Kasahara/Shikumi
}