# 285 - Saeki
# Aki

# County Title
title = c_saeki

# Settlements
max_settlements = 6

b_saeki1 = castle # Sakurao
b_saeki2 = city # Sato
b_saeki3 = temple # Itsukushima

#b_saeki4 = castle # Kusatsu
#b_saeki5 = city # Ogata
#b_saeki6 = city # Hotate
#b_saeki7 = castle # Nomi
#b_saeki8 = temple # Butsugoji

# Misc
culture = western_chugoku
religion = buddhist

# History
1100.1.1={
	capital=b_saeki3
}
1331.1.1={
	capital=b_saeki1 # Sakurao
}
1549.1.1 = { religion = ikko_shu }
