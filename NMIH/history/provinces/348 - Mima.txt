# 348 - Mima
# Awa

# County Title
title = c_mima

# Settlements
max_settlements = 3

#b_mima1 = castle # Miyoshi

#b_mima2 = city # Mima
#b_mima3 = temple # Sadamitsu
#b_mima4 = castle # Waki
#b_mima5 = castle # Anabuki
#b_mima6 = castle # Handa
#b_mima7 = castle # Iwakura
#b_mima8 = castle # Ichiu

b_mima9 = castle # Kanamaru

# Misc
culture = shikoku
religion = buddhist

# History
1100.1.1={
	capital=b_mima9
}
