# 235 - Higashinari
# Settsu

# County Title
title = c_higashinari

# Settlements
max_settlements = 6
#b_higashinari1 = temple # Ishiyama

#b_higashinari2 = city # Tennoji
b_higashinari3 = castle # Hirano
b_higashinari4 = temple # Sumiyoshi
#b_higashinari5 = caslte # Enami
#b_higashinari6 = castle # Kire
#b_higashinari7 = caslte # Osaka
#b_higashinari8 = city # Sakaikita


# Misc
culture = central_kinai
religion = buddhist


# History
1331.1.1={
	b_higashinari5 = castle # Enami
	b_higashinari3 = city # Hirano
	capital=b_higashinari5
}
1460.1.1={
	religion=ikko_shu
	b_higashinari7=temple # Osaka
	b_higashinari2 = city # Tennoji
	capital=b_higashinari7
	remove_settlement=b_higashinari4 # Sumiyoshi
}
1549.7.18={
	remove_settlement=b_higashinari5 # Enami
}
1580.9.20={
	b_higashinari7=castle # Osaka
}