# 383 - Mouda

# Kazusa

# County Title
title = c_mouda

# Settlements
max_settlements = 4

#b_mouda1 = castle		# Mariyatsu
#b_mouda2 = castle		# Kururi
#b_mouda3 = castle		# Odo

#b_mouda4 = castle		# Senbon
#b_mouda5 = city		# Abiru
#b_mouda6 = city		# Kisarazu
#b_mouda7 = temple		# Akitomi
#b_mouda8 = temple		# Senchakuji
b_mouda9 = castle		# Obu

# Misc
culture = eastern_kanto
religion = buddhist

# History
1456.1.1={
	b_mouda1=castle # Mariyatsu
	capital=b_mouda1
	remove_settlement=b_mouda9
}
1457.1.1={
	b_mouda2=castle # Kururi
}
1460.1.1={
	b_mouda3=castle # Odo
}
1541.1.1={
	capital=b_mouda2
}