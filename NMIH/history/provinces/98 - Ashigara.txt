# 98 - Ashigara

# Sagami

# County Title
title = c_ashigara

# Settlements
max_settlements = 6

#b_ashigara1 = castle		# Odawara
#b_ashigara2 = castle		# Takanosu
#b_ashigara3 = castle		# Iwahara/Matsuda
b_ashigara4 = city			# Hakone

#b_ashigara5 = temple		# Shofuku
#b_ashigara6 = temple		# Daiyuzan
#b_ashigara7 = city			# Sakawa
#b_ashigara8 = city			# Hongou
b_ashigara9 = castle			# Dohi

# Misc
culture = southern_kanto
religion = buddhist

# History
1100.1.1={
	capital=b_ashigara9 # Dohi
}
#1200.1.1={
#	b_ashigara1=castle # Odawara
#}
#1237.9.25={
#	capital=b_ashigara1 # Odawara
#}
1374.1.1={
	b_ashigara3=castle # Iwahara
}
1417.2.1={
	b_ashigara1=castle # Odawara
	capital=b_ashigara1 # Odawara
	remove_settlement=b_ashigara9 # Dohi
}
1570.1.1={
	b_ashigara2=castle # Takanosu
}
1591.1.1={
	remove_settlement=b_ashigara2 # Takanosu
}