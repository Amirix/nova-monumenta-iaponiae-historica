# 181 - Kadono
# Yamashiro

# County Title
title = c_kadono

# Settlements
max_settlements = 7
#b_kadono1 = castle # Nijo
#b_kadono2 = city # Katsura
b_kadono3 = city # Saga
b_kadono4 = temple # Kitano
b_kadono5 = temple # Takao

#b_kadono6 = temple # Matsuo
#b_kadono7 = temple # Umezu
#b_kadono8 = temple # Kinugasa
#b_kadono9 = castle # Sai

# Misc
culture = central_kinai
religion = buddhist


# History
0794.1.1={
	b_kadono9=castle
	capital=b_kadono9
}

