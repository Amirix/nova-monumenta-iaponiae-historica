# 265 - Oku
# Bizen

# County Title
title = c_oku

# Settlements
max_settlements = 5

b_oku1 = castle # Toyohara
b_oku2 = castle # Ushimado
b_oku3 = castle # Katakami
#b_oku4 = city # Osafune
#b_oku5 = castle # Mushiake
#b_oku6 = city # Imbe
#b_oku7 = temple # Kashino
#b_oku8 = castle # Otogo

# Misc
culture = sanyo
religion = buddhist

# History
1100.1.1={
	capital=b_oku2
}
1331.1.1={
	b_oku2 = city # Ushimado
	capital=b_oku1 # Toyohara
}
1460.1.1 = { religion = hokke_shu }
