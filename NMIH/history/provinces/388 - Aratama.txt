# 388 - Aratama
# Totomi

# County Title
title = c_aratama

# Settlements
max_settlements = 3
b_aratama1 = castle 		# Inoya
#b_aratama2 = temple 		# Ryotanji

#b_aratama3 = castle		# Mitake
#b_aratama4 = castle 		# Okuyama
#b_aratama5 = castle 		# Idaira
#b_aratama6 = castle		# Kanasashi
#b_aratama7 = castle 		# Kega
#b_aratama8 = castle	 	# Miyakoda


# Misc
culture = tokai
religion = buddhist


# History
0733.1.1={
	b_aratama2=temple # Ryotan-ji
}