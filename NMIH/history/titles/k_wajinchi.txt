###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name k_wajinchi.txt
# @param title k_wajinchi
# @param title_name Oshima
# @param e_ezochi
#
###############################################################################

0100.1.1={
	law = agnatic_succession
	law = succ_primogeniture
}

1453.1.1={
	holder=106 #Ando Masasue
}
1488.9.15={
	holder=105 #Ando Tadasue
}
1511.8.19={
	holder=101 #Ando Hirosue
}
1547.2.27={
	holder=102 #Ando Kiyosue
}
1553.9.18={
	holder=136 # Ando Chikasue
}