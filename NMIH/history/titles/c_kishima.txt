###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kishima.txt
# @param title c_kishima
# @param title_name Kishima
# @param d_hizen
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
200.1.1={
	holder=290001 # Himiko
}
250.1.1={
	holder=290005 # Toyo
}
290.1.1={
	holder=290120 # Yasoomina
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege="d_hizen"
	holder=130879	# Goto Muneaki
}
##
## Kamakura shogunate
##
1185.1.1={
	holder=130880	# Goto Kiyoaki
}
1205.1.1={
	holder=2751000	# Goto Tsuneakira
}
1240.1.1={
	holder=2751005	# Goto Masaakira
}
1265.1.1={
	holder=2751010	# Goto Naoakira
}
1290.1.1={
	holder=2751015	# Goto Ujiakira
}
1310.1.1={
	holder=2751020	# Goto Yukiakira
}
1335.1.1={
	holder=2751025	# Goto Goro
}
###
### Ashikaga shogunate [Muromachi shogunate]
###
1355.1.1={
	holder=2751030	# Goto Tomoakira
}
1380.1.1={
	holder=2751045	# Goto Sukeakira
}
1410.1.1={
	holder=2751050	# Goto Hideakira
}
1425.1.1={
	holder=2751055	# Goto Masaakira
}
1429.1.1={
	holder=2751060 # Goto Shikiakira
}
1493.1.1={
	liege=0
}
1500.1.1={
	holder=2751070 # Goto Sumiakira
}
1553.4.30={
	holder=68402 # Goto Takaakira
}
1575.1.1={
	liege="d_hizen" # 
}
