# Shinano, Muroga castle

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1490.1.1={
	liege="c_hanishina"
	holder = 21190 # Muroga Munekuni
}
1510.1.1={
	holder = 21191 # Muroga Morikiyo
}
1540.1.1={
	holder = 21192 # Muroga Nobutoshi
}
1575.6.29={
	holder = 21194 # Muroga Mitsumasa
}
1582.4.1={
	holder = 21195 # Muroga Masatake
}
1582.6.21={
	liege="c_chiisagata"
#	holder = 21195 # Muroga Masatake
}
1584.7.1={
#	liege="c_chiisagata"
	holder = 21150 # Sanada Masayuki
}
