###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_naoiri.txt
# @param title c_naoiri
# @param title_name Naoiri
# @param d_bungo
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
240.1.1={
	holder=290060 # Uchisaru
}
295.1.1={
	holder=290062 # Kunimaro
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege="d_bungo"
	holder=2780100 # Ogata Koreyoshi
}
##
## Kamakura shogunate
##
1185.12.16={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.1={
	liege=d_bungo
}
1460.1.1={
	holder=64107 # Shiga Chikayasu
}
1470.1.1={
	holder=64106 # Shiga Mitsunobu
}
1490.1.1={
	holder=64101 # Shiga Chikakazu
}
1517.1.1={
	holder=64100 # Shiga Chikamasu
}
1547.1.1={
	holder=64102 # Shiga Chikamori
}