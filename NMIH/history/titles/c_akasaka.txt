###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_akasaka.txt
# @param title c_akasaka
# @param title_name Akasaka
# @param d_bizen
# @param k_sanyo
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege = "d_bizen"
	holder=2630401 # Naniwa Tsuneto
}
1183.8.17={
	liege=0
	holder=0 # Taira's escape to the west
}
##
## Kamakura shogunate
##


###
### Ashikaga shogunate [Muromachi shogunate]
###
1340.1.1={
	liege="d_bizen"
	holder=54415 # Uragami Tamekage
}
1360.1.1={
	holder=54414 # Uragami Yukikage
}
1380.1.1={
	holder=54413 # Uragami Sukekage
}
1408.1.1={
	holder=54412 # Uragami Munetaka
}
1420.1.1={
	holder=54400 # Uragami Muneyasu
}
1450.1.1={
	holder=54401 # Uragami Norimune
}
1460.1.1={
	holder=54405 # Uragami Norimune
}
1502.7.15={
	holder=54403 # Uragami Muramune
}
1531.7.17={
	holder=54404 # Uragami Masamune
}
1554.1.1={
	holder=54407 # Uragami Munekage
}
1575.10.1={
	holder=55100 # Ukita Naoie
}
1579.10.1={
	liege="e_japan"
}
1582.2.1={
	holder=55101 # Ukita Hideie
}

