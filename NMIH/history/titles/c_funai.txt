###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_funai.txt
# @param title c_funai
# @param title_name Funai
# @param d_tanba
# @param k_northern_kinai
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_tanba"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.1.1={
	liege="d_settsu" # Minamoto Yorimasa
	holder=120250 # Minamoto Yorimasa
}
##
## the Taira clan Government
##
1180.6.20={
	liege="d_tanba"
	holder=110141 # Taira Kiyokuni / Battle of Uji
}
1183.8.17={
	liege=0
	holder=0 # Taira's escape to the west
}
##
## Kamakura shogunate
##



###
### Ashikaga shogunate [Muromachi shogunate]
###
1397.6.2={
	liege="d_tanba"
	holder=40536 # Hosokawa Mitsumoto
}
1422.1.1={
#	liege="d_tanba"
	holder=40730 # Kozai Motosuke
}
1431.1.1={
#	liege="d_tanba"
	holder=42055 # Naito Shinsho
}
1440.1.1={
#	liege="d_tanba"
	holder=42056 # Naito Yukisada
}
1452.1.1={
#	liege="d_tanba"
	holder=42050 # Naito Motosada
}
1503.1.1={
#	liege="d_tanba"
	holder=42051 # Naito Sadamasa
}
1508.1.1={
	liege="k_northern_kinai"
#	holder=42051 # Naito Sadamasa
}
1508.7.18={
	liege="d_tanba"
#	holder=42051 # Naito Sadamasa
}
1525.6.23={
#	liege="d_tanba"
	holder=42052 # Naito Kunisada
}
1527.3.14={
	liege="k_northern_kinai"
#	holder=42052 # Naito Kunisada
}
1536.9.14={
	liege=0
#	holder=42052 # Naito Kunisada
}
1543.1.1={
	liege="k_northern_kinai"
#	holder=42052 # Naito Kunisada
}
1548.10.28={
	liege="k_southern_kinai"
#	holder=42052 # Naito Kunisada
}
1553.11.4={
	liege="d_tanba"
	holder=40740 # Kozai Motoshige
}
1554.1.1={
	liege="k_southern_kinai"
	holder=45402 # Naito Munekatsu
}
1557.1.1={
	liege="d_tanba"
#	holder=45402 # Naito Munekatsu
}
1565.8.27={
	liege=0
	holder=42053 # Naito Sadakatsu
}
1568.11.7={
	liege="e_japan"
#	holder=42053 # Naito Sadakatsu
}

###
### The Oda government 
###
1573.8.25={
	liege=0
#	holder=42053 # Naito Sadakatsu
}
1576.2.10={
	liege="d_tanba"
#	holder=42053 # Naito Sadakatsu
}
1579.7.20={
	liege="d_tanba"
	holder=25632 # Akechi Mitsuhide
}
1582.6.21={
	liege="d_tanba"
	holder=45730 # Kimura Yoshikiyo
}
1582.7.2={
	liege="d_tanba"
	holder=29400 # Hashiba Hideyoshi
}
1582.7.16={
#	liege="d_tanba"
	holder=29163 # Hashiba Hidekatsu
}
