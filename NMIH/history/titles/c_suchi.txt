###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_suchi.txt
# @param title c_suchi
# @param title_name Suchi
# @param d_totomi
# @param k_tokai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_totomi"
}

#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

##
## the Taira clan Government
##
1180.1.1={
	liege="d_suruga" # Tachibana Tomochi
	holder=130153 # Sagara Yorikage
}

1180.11.9={
	liege="d_totomi" # Battle of Fujikawa
	holder=130153 # Sagara Yorikage
}
1181.4.28={
	liege="d_totomi" # Yasuda Yoshisada
	holder=120113 # Yasuda Yoshisada
}
1193.11.28={
	liege=0
	holder=0
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1336.1.1={
	liege="d_totomi"
	holder=2395214 # Imagawa Norikuni
}
1339.10.1={
#	liege="d_totomi"
	holder=2396021 # Nikki Yoshinaga
}
1352.2.1={
#	liege="d_totomi"
	holder=2395214 # Imagawa Norikuni
}
1384.6.8={
#	liege="d_totomi"
	holder=2395221 # Imagawa Sadayo
}
1400.1.17={
	liege="d_suruga"
#	holder=2395221 # Imagawa Sadayo
}
1415.1.1={
#	liege="d_suruga"
	holder=2395270 # Imagawa Sadasuke
}
1430.1.1={
#	liege="d_suruga"
	holder=2395273 # Imagawa Norimasa
}
1459.10.1={
#	liege="d_suruga"
	holder=2395276 # Imagawa Sadanobu
}
1475.1.1={
#	liege="d_suruga"
	holder=23101 # Asahina Yasuhiro
}
1508.8.1={
	liege="d_totomi"
#	holder=23101 # Asahina Yasuhiro
}
1512.1.1={
#	liege="d_totomi"
	holder=23100 # Asahina Yasuyoshi
}
1557.9.22={
	# liege="d_totomi" # 23002 Imagawa Yoshimoto
	holder=23102 # Asahina Yasutomo
}
1569.5.17={
	# liege="d_totomi" # 24006 Tokugawa Ieyasu
	holder=24153 # Ishikawa Ienari
}
