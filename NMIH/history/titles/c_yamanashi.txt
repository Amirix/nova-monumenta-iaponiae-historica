###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_yamanashi.txt
# @param title c_yamanashi
# @param title_name Yamanashi
# @param d_kai
# @param k_tokai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_kai"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.1.1={
	liege="d_kai" # Takeda Nobuyoshi
	holder=120113 # Yasuda Yoshisada
}
1180.11.9={
	liege="d_totomi" # Yasuda Yoshisada / Battle of Fujikawa 
#	holder=120113 # Yasuda Yoshisada
}

##
## Kamakura shogunate
##
1193.11.28={
	liege="d_kai"
	holder=2360103 # Takeda Nobumitsu
}
1248.12.21={
#	liege="d_kai"
	holder=2360112 # Takeda Nobumasa
}
1265.1.6={
#	liege="d_kai"
	holder=2360121 # Takeda Masatsuna
}
1293.1.1={
#	liege="d_kai"
	holder=2360140 # Takeda Nobuie
}
1305.1.1={
#	liege="d_kai"
	holder=2360145 # Takeda Sadanobu
}
1331.1.1={
#	liege="d_kai"
	holder=2360150 # Takeda Masayoshi
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1343.1.1={
	liege="d_kai"
	holder=2360160 # Takeda Nobutake
}
1359.8.7={
#	liege="d_kai"
	holder=2360170 # Takeda Nobunari
}
1394.7.11={
#	liege="d_kai"
	holder=2360180 # Takeda Nobuharu
}
1413.11.16={
#	liege="d_kai"
	holder=2360190 # Takeda Nobumitsu
}
1417.2.22={
	liege="k_kamakura_kubo"
	holder=2361520 # Itsumi Arinao
}
1425.1.1={
	liege=0
	holder=2361300 # Atobe Myokai
}
1438.9.1={
	liege="d_kai"
	holder=2361300 # Atobe Myokai
}
1464.1.1={
#	liege="d_kai"
	holder=2361301 # Atobe Kageie
}
1465.1.1={
#	liege="d_kai"
	holder=2360220 # Takeda Nobumasa
}
1492.1.1={
	liege=0
	holder=2360231 # Aburakawa Nobuyoshi
}
1508.10.27={
	liege="d_kai"
	holder=20000 # Takeda Nobutora
}
1541.6.14={
	holder=20010 # Takeda Shingen
}
1573.05.13={
	holder=20033 # Takeda Katsuyori
}
1582.4.3={
	holder=29120 # Oda Nobunaga
}
1582.04.15={
	holder=29251 # Kawajiri Hidetaka
}
1582.7.7={
	liege="d_kai"
	holder=24006 # Tokugawa Ieyasu
}
1582.8.1={
	holder=24480 # Hiraiwa Chikayoshi
}