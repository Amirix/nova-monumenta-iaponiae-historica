###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kiku.txt
# @param title c_kiku
# @param title_name Kiku
# @param d_buzen
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
180.1.1={
	holder=290001 # Himiko
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_buzen"
	holder=2750500 # Itai Taneto
}
1185.3.4={
	liege="k_western_chugoku" # Battle of Ashiya-no-ura
}
1185.4.25={
	liege=0
	holder=0 # Battle of Dan-no-ura
}
##
## Kamakura shogunate
##

###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.1={
	liege=k_western_chugoku
}
1480.1.1={
	holder=67103 # Nagano Yoshifusa
}
1510.1.1={
	holder=67101 # Nagano Yoshiaki
}
1532.1.1={
	holder=67100 # Nagano Fusamori
}
1559.1.1={
	holder=67506 # Akisuki Tanefuyu
	liege=d_aki
}
1565.1.1={
	liege=d_bungo
}
1567.1.1={
	liege=d_aki # Mori Motonari
}
1568.8.1={
	liege=d_bungo # Otomo Yoshishige
}
1579.1.1={
	liege=d_hizen # Ryuzoji Takanobu
}