###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_yasuna.txt
# @param title c_yasuna
# @param title_name Yasuna
# @param d_bingo
# @param k_sanyo
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}

##
## the Taira clan Government
##
1180.1.1={
	liege = "d_bingo"
	holder=2660301 # Nuka Takanobu
}
1184.10.1={
	liege=0
	holder=0 #
}

##
## Kamakura shogunate
##
1221.8.1={
	liege="e_japan"
}

##
## Ashikaga shogunate [Muromachi shogunate]
##
1336.1.1={
	liege="d_bingo" #　Asayama Kagetsura
	holder=2601006 #　Asayama Kagetsura
}

1338.3.1={
#	liege="d_bingo"
	holder=0
}

1349.1.1={
	holder=40036 #　Ashikaga Tadafuyu
}

1363.10.1={
	liege="d_bingo"
	holder=0
}

1433.10.1={
#	liege="d_bingo"
	holder=50039 #　Yamana Mochitoyo
}

1454.12.1={
#	liege="d_bingo"
	holder=50040 # Yamana Noritoyo
}

1460.1.1={
#	liege="d_bingo"
	holder=50041 #　Yamana Koretoyo
}

1475.11.1={
#	liege="d_bingo"
	holder=50044 #　Yamana Masatoyo
}

1488.1.1={
#	liege="d_bingo"
	holder=50059 # Yamana Toshitoyo
}

1495.1.1={
	holder = 56701 # Yamana Tokioki
}
1523.1.1={
	holder = 56700 # Yamana Tadaoki
}

1538.8.1={
	liege="k_western_chugoku"
	holder=56700 # Yamana Tadaoki
}

1543.1.1={
	liege="k_sanin"
#	holder=56700 # Yamana Tadaoki
}

1549.9.24={
	liege="k_western_chugoku" #　Ouchi Yoshitaka
	holder=57002 #　Ouchi Yoshitaka
}

1549.11.21={
	liege="k_western_chugoku" #　Ouchi Yoshitaka
	holder=57500 #　Aokage Takaakira / castle keeper
}

1554.1.1={
	liege="d_aki"
	holder=56700 # Yamana Tadaoki
}

1557.1.1={
#	liege="d_aki"
	holder = 56750 # Sugihara Morishige / Yamana Tadaoki died
}
1582.1.19={
	holder = 56751 # Sugihara Motomori
}
1582.7.1={
	holder = 56752 # Sugihara Kagemori
}
1582.9.1={
	holder=56015 # Kikkawa Motoharu
}
