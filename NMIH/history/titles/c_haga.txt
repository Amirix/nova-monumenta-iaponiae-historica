###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_haga.txt
# @param title c_haga
# @param title_name Haga
# @param d_shimotsuke
# @param k_western_kanto
# @param e_japan
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_shimotsuke"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.1.1={
	liege="d_utsunomiya"
	holder=2130500 # Haga Takachika
}

##
## Kamakura shogunate
##
1198.1.1={
#	liege="d_utsunomiya"
	holder=2130505 # Haga Takahide
}
1211.1.1={
#	liege="d_utsunomiya"
	holder=2130510 # Haga Takayuki
}
1233.1.1={
#	liege="d_utsunomiya"
	holder=2130515 # Haga Takanobu
}
1259.1.1={
#	liege="d_utsunomiya"
	holder=2130520 # Haga Takatoshi
}
1298.1.1={
#	liege="d_utsunomiya"
	holder=2130521 # Haga Takanao
}
1300.1.1={
#	liege="d_utsunomiya"
	holder=2130525 # Haga Takahisa
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1334.1.1={
#	liege="d_utsunomiya"
	holder=2130530 # Haga Takana
}
1372.1.1={
#	liege="d_utsunomiya"
	holder=2130535 # Haga Takatomo
}
1390.1.1={
#	liege="d_utsunomiya"
	holder=2130536 # Haga Shigetaka
}
1400.1.1={
#	liege="d_utsunomiya"
	holder=12000 # Haga Takamasu
}
1436.1.1={
#	liege="d_utsunomiya"
	holder=12001 # Haga Kagetaka
}
1456.1.1={
	liege="d_oyama"
#	holder=12001 # Haga Kagetaka
}
1471.1.1={
	liege="d_shimotsuke"
#	holder=12001 # Haga Kagetaka
}
1472.1.1={
#	liege="d_shimotsuke"
	holder=12003 # Haga Takakatsu
}
1494.1.1={
	holder=12004 # Haga Takataka
}
1536.1.1={
	holder=12005 # Haga Takatsune
}
1541.10.24={
	holder=12006 # Haga Takasada
}
1565.1.1={
	holder=12008 # Haga Takatsugu
}

