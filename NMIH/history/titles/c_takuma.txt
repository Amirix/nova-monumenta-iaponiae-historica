###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_takuma.txt
# @param title c_takuma
# @param title_name Takuma
# @param d_higo
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=41352 # Takekukuchi a.k.a Kukuchihiko
}
250.1.1={
	holder=41353 # Takemorokumi
}
290.1.1={
	holder=41357 # Takekunitake
}
330.1.1={
	holder=41358 # Takeoshihiko
}
350.1.1={
	holder=41359 # Takekawana
}
390.1.1={
	holder=41360 # Takekuwae
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1170.1.1={
	liege="d_higo"
	holder=2780645 # Kikuchi Takanao
}
##
## Kamakura shogunate
##
1185.4.25={
	liege=0
	holder=0 # Battle of Dan-no-ura
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1488.11.26={
	holder=66005 # Kikuchi Shigetomo
}
1493.12.7={
	holder=66004 # Kikuchi Yoshiyuki
}
1504.3.1={
	holder=66003 # Kikuchi Masataka
}
1507.1.1={
	holder=66203 # Kikuchi Taketsune
}
1513.1.1={
	holder=66001 # Kikuchi Takekane
}
1532.1.1={
	holder=66000 # Kikuchi Yoshitake
}
1550.1.1={
	holder=64002 # Otomo Yoshishige
}
1578.1.1={
	holder=68320 # Ryuzoji Takanobu
}