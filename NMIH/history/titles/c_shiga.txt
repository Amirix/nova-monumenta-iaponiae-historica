###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_shiga.txt
# @param title c_shiga
# @param title_name Shiga
# @param d_omi
# @param k_northern_kinai
#
###############################################################################

0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_omi"
}

#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

0788.1.1={
	law=succ_open_elective
	law=succ_primogeniture
	liege="d_enryakuji"
}
#1014.12.26={
#	liege=0
#	holder=1005099 # Keien
#}
1015.1.1={
	holder=1003600 # Keimyo
}
1038.1.1={
	holder=0
}
1105.4.7={
	holder=1006265 # [39] Zoyo
}
1110.6.1={
	holder=1006565 # [42] Ningo
}
1121.11.24={
	holder=1006456 # [43] Kankei
}
1138.12.10={
	holder=1000149 # [48] Gyogen
}
1162.3.25={
	holder=1000152 # [50] Kakuchu
}
1179.12.16={
#	liege="d_enryakuji"
	holder=2570100 # Myoun
}
1184.1.3={
	liege=0
	holder=0 # Siege of Hojujidono
}
1193.1.11={
	holder=1000165 # Jien
}
1206.1.30={
	holder=1000462 # Ryoken
}
1212.2.27={
	holder=1000165 # Jien
}
1214.1.8={
	holder=1000165 # Jien
}
#1214.7.27={
#	holder=1000712 # Ryoken
#}
1221.9.21={
	holder=1000209 # Enki
}
1277.1.7={
	holder=1000636 # Dogen
}
1278.5.1={
	holder=1001358 # Kingo
}
1288.5.6={
	holder=1000637 # Kujo
}
1293.6.21={
	holder=1000403 # Jiki
}
1297.1.22={
	holder=1001246 # Sonkyo
}
1302.6.21={
	holder=1000666 # Dojun
}
1303.5.17={
	holder=1000636 # Dogen
}
#1325.6.27={
#	holder=1000817 # Shoshu
#}
1377.5.29={
	holder=1000734 # Jizai
}
1393.2.19={
	holder=1000280 # Jiben
}
1402.4.8={
	holder=1000754 # Dogo
}
1404.12.5={
	holder=1000756 # Kankyo
}
1409.4.11={
	holder=1000757 # Ryojun
}
1412.6.5={
	holder=1000756 # Kankyo
}
1413.12.18={
	holder=1001461 # Jitsuen
}
1421.5.12={
	holder=40018 # [156] Ashikaga Jiben
}
1428.6.4={
	holder=40023 # [157] Ashikaga Gisho
}
1431.7.1={
	holder=1000770 # [158] Ichijo Ryoju
}
1435.5.21={
	holder=40023 # [159] Ashikaga Gisho
}
1447.2.21={
	holder=1001467 # [160] Kinsho
}
1471.5.9={
	holder=1000790 # [162] Sonou
}
1493.5.15={
	holder=41756 # Gyoin, 163th
}
1515.4.13={
	holder=41759 # Kakuin, 164th
}
1530.4.23={
	holder=41701 # Sonchin, 165th
}
1550.1.1={
	holder=41748 # Gyoson, 166th
}
1559.9.5={
	holder=41743 # Ouin, 167th
}
#1559.9.5={
#	holder=41712 # Kakujo, 168th
#}
1571.9.30={
	law = agnatic_succession
	law = succ_primogeniture
	liege="k_chubu" # 29120 Oda Nobunaga
	holder=25632 # Akechi Mitsuhide
}
1575.12.9={
	liege="e_japan"
}
1582.6.21={
	liege="k_northern_kinai"
#	holder=25632 # Akechi Mitsuhide
}
1582.7.2={
	liege=0
	holder=25639 # Akechi Hidemitsu
}
1582.7.4={
	liege="k_northern_kinai"
	holder=29400 # Hashiba Hideyoshi
}
1582.7.16={
	liege="d_wakasa"
	holder=29310 # Niwa Nagahide
}