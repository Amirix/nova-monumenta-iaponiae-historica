###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_yakami.txt
# @param title c_yakami
# @param title_name Yakami
# @param d_inaba
# @param k_sanin
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="k_sanin"
	holder=2600100 # Saji Michisada
}
1183.8.17={
	liege="k_western_chugoku" # Taira's escape to the west
}
1185.1.1={
	liege=0
	holder=0 # 
}
##
## Kamakura shogunate
##


###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.1={
	liege="d_inaba"
}
1475.1.1={
	holder = 50032 # Yamana Toyotoki
}
1488.1.1={
	holder = 50056 # Yamana Masazane
}
1489.12.18={
	holder = 50032 # Yamana Toyotoki
}
1504.1.1={
	holder = 50057 # Yamana Toyoshige
}	
1512.12.1={
	holder = 50033 # Yamana Toyoyori
}
1521.1.1={
	holder = 50034 # Yamana Toyoharu
}
1527.1.1={
	holder = 50035 # Yamana Nobumichi
}
1546.1.1={
	holder = 50048 # Yamana Toyosada
}
1560.3.29={
	holder = 50070 # Yamana Munetoyo
}
1561.5.1={
	holder = 50051 # Yamana Toyokazu
}
1564.1.1={
	holder = 50060 # Yamana Toyokuni
}
1573.11.1={
	liege="k_western_chugoku" # Mori Terumoto
}
1580.9.1={
	liege="e_japan"
	holder = 29400 # Hashiba Hideyoshi
}

