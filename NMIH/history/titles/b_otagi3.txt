# Yamashiro
# Shijo / Shimogyo

0794.11.22={
	law=succ_open_elective
	law=succ_primogeniture
	liege=0
}
1378.1.1={
	name=B_OTAGI3_1378 # "Shimogyo"
	adjective=B_OTAGI3_1378_adj # "Shimogyo"
}
1459.1.1={
	liege="c_otagi"
	holder=1004304 # Hino Tomiko
}
1496.6.30={
	holder=45919 # Kameya Goijo
}
1544.1.1 ={
	holder=45920 # Chaya Akinobu
}
1570.1.1={
	holder=45921 # Chaya Kiyonobu
}