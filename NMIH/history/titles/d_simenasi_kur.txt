###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_simenasi_kur.txt
# @param title d_simenasi_kur
# @param title_name Si-menas-un-kur
# @param Titular Duchy
#
###############################################################################

1170.1.1={
	holder=152100
}
1210.1.1={
	holder=152101 
}
1230.1.1={
	holder=152091
}
1280.1.1={
	holder=152092
}
1300.1.1={
	holder=152093
}
1330.1.1={
	holder=152094
}
1360.1.1={
	holder=152095
}
1400.1.1={
	holder=152096
}
1420.1.1={
	holder=152097
}
1450.1.1={
	holder=152098
}
1490.1.1={
	holder=152000
}
1520.1.1={
	holder=152003
}

1570.1.1={
	holder=152004
}