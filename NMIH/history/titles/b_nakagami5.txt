###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name b_nakagami5.txt
# @param title b_nakagami5
# @param title_name Katsuren
# @param c_nakagami
# @param d_okinawa
# @param k_ryukyu
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}

##
## Eiso Dynasty
##
1290.1.1={
	liege="c_nakagami"
	holder=142214 # Eiso Katchin
}
1330.1.1={
	holder=142240 # Eiso Katchin
}

##
## Satto Dynasty
##
1350.1.1={
	holder=142242 # Eiso Katchin
}
1360.1.1={
	holder=142243 # Eiso Katchin
}
1370.1.1={
	holder=142244 # Eiso Katchin
}
1380.1.1={
	liege=0
	holder=141205 # Ifa Katchin
}
1410.1.1={
	holder=141215 # Ifa Hamagaa
}

##
## 1st Sho Dynasty
##
1415.1.1={
	liege="c_nakagami"
}
1420.1.1={
	holder=141216 # Ifa Hamagaa
}
1430.1.1={
	holder=141250 # Mochiduki Mochiduki
}
1440.1.1={
	holder=141251 # Katchin Amawari
}
1444.1.1={
	liege="c_kunigami"
}
