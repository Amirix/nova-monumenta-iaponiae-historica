###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_imadachi.txt
# @param title c_imadachi
# @param title_name Imadachi
# @param d_echizen
# @param k_hokuriku
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_echizen"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege=0
	holder=130898 # Shindo Tamenori
}
1181.7.26={
	liege="d_kaga" # Battle of Yokota-gawara
}
1183.5.20={
	liege="d_echizen" # Siege of Hiuchi Castle
	holder=110032 # Taira Norimori
}
1183.6.22={
	liege=0
	holder=0 # Battle of Shinohara
}

##
## Kamakura shogunate
##

###
### Ashikaga shogunate [Muromachi shogunate]
###
1481.8.21={
	holder=30008 # Asakura Ujikage
}
1486.8.3={
	holder=30010 # Asakura Sadakage
}
1512.4.11={
	holder=30000 # Asakura Takakage
}
1548.4.30={
	holder=30011 # Asakura Yoshikage
}
1573.9.16={
	holder=29120 # Oda Nobunaga
}
1573.9.24={
	liege="d_echizen" # 30100 Maeba Yoshitsugu
	holder=30100 # Maeba Yoshitsugu
}
1574.2.10={
	liege=0
	holder=30120 # Toda Nagashige
}
1574.3.11={
	holder=42324 # Shimotsuma Raisho
}
1574.9.1={
	liege="d_kaga" # 42324 Shimotsuma Raisho
	holder=42324 # Shimotsuma Raisho
}
1575.9.25={
	liege=0
	holder=29120 # Oda Nobunaga
}
1575.10.1={
	liege="d_echizen" # 29280 Shibata Katsuie
	holder=29350 # Sassa Narimasa
}
1581.1.1={
	# liege="d_echizen" # 
	holder=29280 # Shibata Katsuie
}
