###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_ibusuki.txt
# @param title c_ibusuki
# @param title_name Ibusuki
# @param d_satsuma
# @param k_southern_kyushu
#
###############################################################################

10.1.1={
law = agnatic_succession
law = succ_primogeniture
}
20.1.1={
	holder=290300 # Sori
}
70.1.1={
	holder=290301 # Kibe
}
100.1.1={
	holder=290302 # Toki
}
150.1.1={
	holder=290307 # Ishikori
}
170.1.1={
	holder=290310 # Morohito
}
220.1.1={
	holder=290311 # Imashiri
}
250.1.1={
	holder=290312 # Yamatoko
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_satsuma"
	holder=2810508 # Taira Tadamitsu
}
##
## Kamakura shogunate
##
1210.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1300.1.1={
	liege="d_satsuma"
}
1387.6.20={
	holder=60004 # Shimazu Hisatoyo
}
1425.2.18={
	holder=60005 # Shimazu Tadakuni
}
1470.2.21={
	holder=60008 # Shimazu Tatsuhisa
}
1474.4.26={
	holder=60011 # Shimazu Tadamasa
}
1508.3.16={
	holder=60002 # Shimazu Katsuhisa
}
1526.11.1={
	liege=0
}
1535.8.1={
	liege="d_satsuma"
	holder=60000 # Shimazu Takahisa
}
1571.7.15={
	holder=60024 # Shimazu Yoshihisa
}