###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_tsuzuki.txt
# @param title c_tsuzuki
# @param title_name Tsuzuki
# @param d_yamashiro
# @param k_northern_kinai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_yamashiro"
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.6.15={
	liege="k_yamashiro" # Prince Mochihito
	holder=41091 # Prince Mochihito
}

##
## the Taira clan Government
##
1180.6.20={
	liege="e_japan" # Taira Kiyomori
	holder=110025 # Taira Kiyomori. Battle of Uji
}
1181.3.20={
#	liege="e_japan" # Taira Munemori
	holder=110042 # Taira Munemori
}
1183.8.17={
#	liege="e_japan" # Minamoto Yoshinaka
	holder=120084 # Minamoto Yoshinaka. Taira's escape to the west
}

##
## Kamakura shogunate
##
1184.3.4={
	liege=0
	holder=0 # Battle of Awazu
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1442.9.8={
	liege="d_settsu"
	holder=40543 # Hosokawa Katsumoto
}
1470.7.1={
	liege="k_western_chugoku"
	holder=57007 # Ouchi Masahiro
}
1477.12.16={
	liege="d_yamashiro"
	holder=48121 # Hatakeyama Masanaga
}
1482.12.1={
	liege="d_kawachi"
	holder=48115 # Hatakeyama Yoshinari
}
1485.12.11={
	liege=0 # Yamashiro Kuni-Ikki
	holder=45860 # Koma YamashiroNoKami
}
1493.8.18={
	liege="d_kawachi"
	holder=48902 # Furuichi Choin
}
1496.1.1={
	liege="k_kanrei"
	holder=40800 # Akazawa Tomotsune
}
1507.8.4={
	holder=40802 # Akazawa Nagatsune
}
1508.9.7={
	liege="d_yamashiro"
	holder=57001 # Ouchi Yoshioki
}
1518.9.6={
	liege="k_kanrei"
	holder=40566 # Hosokawa Takakuni
}
1527.3.14={
#	liege="k_kanrei"
	holder=42082 # Yanagimoto Kataharu
}
1530.8.2={
#	liege="k_kanrei"
	holder=40901 # Yanagimoto Jinjiro
}
1532.1.1={
#	liege="k_kanrei"
	holder=40500 # Hosokawa Harumoto
}
1534.1.1={
#	liege="k_kanrei"
	holder=40831 # Kizawa Nagamasa
}
1542.4.2={
#	liege="k_kanrei"
	holder=40912 # Takabatake Naganao
}
1543.1.1={
	liege="k_northern_kinai"
	holder=40554 # Hosokawa Ujitsuna
}
1548.10.28={
	liege="k_southern_kinai"
#	holder=40554 # Hosokawa Ujitsuna
}
1555.1.1={
#	liege="k_southern_kinai"
	holder=59006 # Miyoshi Nagayasu
}
1567.5.24={
	liege="d_yamashiro"
#	holder=59006 # Miyoshi Nagayasu
}
1568.10.19={
	liege="d_yamashiro"
	holder=29120 # Oda Nobunaga
}
1568.11.7={
	liege="e_japan"
	holder=40231 # Makishima Akimitsu
}

###
### The Oda government 
###
1573.8.25={
	liege="k_chubu"
	holder=29120 # Oda Nobunaga
}
1574.5.1={
#	liege="k_chubu"
	holder=29390 # Ban Naomasa
}
1575.12.9={
	liege="e_japan"
#	holder=29390 # Ban Naomasa
}
1576.5.30={
#	liege="e_japan"
	holder=25632 # Akechi Mitsuhide
}
1582.6.21={
	liege="d_yamashiro"
#	holder=25632 # Akechi Mitsuhide
}
1582.7.2={
	liege="d_yamashiro"
	holder=29400 # Hashiba Hideyoshi
}
1582.8.1={
#	liege="d_yamashiro"
	holder=29439 # Asano Nagayoshi
}
