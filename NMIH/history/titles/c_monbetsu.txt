###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_monbetsu.txt
# @param title c_monbetsu
# @param title_name Monbetsu
# @param d_kitami
# @param k_nishi_ezochi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1170.1.1={
	liege=0
	holder=154601 # Bungi
}
1210.1.1={
	liege=0
	holder=0 # 
}

1270.1.1={
	holder=157203
}
1320.1.1={
	liege=0
	holder=0 # 
}

1450.1.1={
	holder=151303
}
1470.1.1={
	holder=151300
}
1500.1.1={
	holder=151301
}
1560.1.1={
	holder=151302
}

