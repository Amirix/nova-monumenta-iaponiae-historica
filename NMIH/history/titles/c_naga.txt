###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_naga.txt
# @param title c_naga
# @param title_name Naga
# @param d_kii
# @param k_southern_kinai
#
###############################################################################

0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_kii"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege=0
	holder=130172 # Sato Yoshikiyo
}
##
## Kamakura shogunate
##
1195.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.17={
	liege="d_kii"
	holder=48110 # Hatakeyama Motokuni
}
1406.2.5={
	holder=31001 # Hatakeyama Mitsunori
}
1408.10.1={
	holder=48111 # Hatakeyama Mitsuie
}
1433.10.31={
	holder=48114 # Hatakeyama Mochikuni
}
1441.1.1={
	holder=48113 # Hatakeyama Mochinaga
}
1441.7.1={
	holder=48114 # Hatakeyama Mochikuni
}
1455.4.12 ={
	holder=48115 # Hatakeyama Yoshihiro
}
1460.10.4={
	holder=48121 # Hatakeyama Masanaga
}
1493.6.9={
	holder=48123 # Hatakeyama Hisanobu
}
1510.1.1={
	liege=0
	holder=48686 # Tsuda Kazunaga
}
1568.1.22={
#	liege=0
	holder=48689 # Tsuda Kazumasa
}

###
### The Oda government 
###
1577.3.1={
	liege="e_japan"
#	holder=48689 # Tsuda Kazumasa
}
1582.6.21={
	liege=0
#	holder=48689 # Tsuda Kazumasa
}
