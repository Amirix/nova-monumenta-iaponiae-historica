###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kamikawa2.txt
# @param title c_kamikawa2
# @param title_name Kamikawa
# @param d_tokachi
# @param k_higashi_ezochi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1170.1.1={
	liege="d_menasi_kur"
	holder=151600 # Tanroin
}
1210.1.1={
	liege=0
	holder=0 # 
}

1370.1.1={
	liege=d_menasi_kur
	holder=151550
}
1400.1.1={
	holder=151551
}
1450.1.1={
	holder=151556
}
1470.1.1={
	holder=151500
}
1530.1.1={
	holder=151501
}
1550.1.1={
	holder=151502
}
1590.1.1={
	holder=151503
}
