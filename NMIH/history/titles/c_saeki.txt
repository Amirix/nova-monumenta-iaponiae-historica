###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_saeki.txt
# @param title c_saeki
# @param title_name Saeki
# @param d_aki
# @param k_western_chugoku
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_aki"
}

##
## the Taira clan Government
##
1180.1.1={
#	liege="d_aki"
	holder=2660401 # Saeki Kagehiro
}
##
## Kamakura shogunate
##
1200.1.1={
	liege=0
	holder=0 #
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.1={
	liege="k_western_chugoku"
}
1403.1.1={
	holder = 56106 # Takeda Nobumori
}
1418.1.1={
	holder = 56105 # Takeda Nobushige
}
1465.11.28={
	holder = 56104 # Takeda Nobukata
}
1471.6.20={
	holder = 56103 # Takeda Mototsuna
}
1505.3.30={
	holder = 56101 # Takeda Motoshige
}
1515.1.1={
	liege="k_sanin"
}
1517.11.5={
	holder = 56100 # Takeda Mitsukazu
}
1540.6.9={
	holder = 56102 # Takeda Nobuzane
}
1541.5.1={
	liege="k_western_chugoku"
	holder=56004 # Mori Motonari
}
1571.7.6={
	holder=56025 # Mori Terumoto
}
