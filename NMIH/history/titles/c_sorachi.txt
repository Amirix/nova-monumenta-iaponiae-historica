###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_sorachi.txt
# @param title c_sorachi
# @param title_name Sorachi
# @param d_ishikari
# @param k_nishi_ezochi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1066.1.1={
	holder=150201 # Ponyaunpet
}
1130.1.1={
	liege=0
	holder=0 # 
}

1170.1.1={
	liege="d_ishikari"
	holder=154100 # Oko
}
1210.1.1={
	liege=0
	holder=0 # 
}

1270.1.1={
	holder=157202
}
1310.1.1={
	liege=0
	holder=0 # 
}

1450.1.1={
	liege="d_ishikari"
	holder=154002
}
1490.1.1={
	holder=154000
}
1540.1.1={
	holder=154001
}
