###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_nabari.txt
# @param title c_nabari
# @param title_name Nabari
# @param d_iga
# @param k_southern_kinai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_iga"
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1167.5.29={
	liege="d_iga" # Taira Ietsugu
	holder=110220 # Taira Ietsugu
}
1184.8.14={
	liege=0
	holder=0 # Failure of Taira's Three-day rebellion
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1429.1.25={
	liege="d_shima"
	holder=26006 # Kitabatake Noritomo
}
1471.4.13={
	holder=26005 # Kitabatake Masasato
}
1480.1.1={
	liege=0
	holder=28801 # Momochi Masanaga
}
1540.1.1={
	liege=0
	holder=28800 # Momochi Tanba
}
1581.11.06={
	liege="d_iga" # Oda Nobukatsu
	holder=29161 # Oda Nobukatsu
}
