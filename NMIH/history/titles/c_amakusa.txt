###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_amakusa.txt
# @param title c_amakusa
# @param title_name Amakusa
# @param d_higo
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=41352 # Takekukuchi a.k.a Kukuchihiko
}
250.1.1={
	holder=41353 # Takemorokumi
}
290.1.1={
	holder=41357 # Takekunitake
}
330.1.1={
	holder=41358 # Takeoshihiko
}
350.1.1={
	holder=41359 # Takekawana
}
390.1.1={
	holder=41360 # Takekuwae
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_chikuzen"
	holder=2781000 # Amakusa Tanesada
}
##
## Kamakura shogunate
##
1205.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1488.11.26={
	holder=66005 # Kikuchi Shigetomo
}
1493.12.7={
	holder=66004 # Kikuchi Yoshiyuki
}
1504.3.1={
	holder=66003 # Kikuchi Masataka
}
1507.1.1={
	holder=66203 # Kikuchi Taketsune
}
1513.1.1={
	holder=66001 # Kikuchi Takekane
}
1532.1.1={
	holder=66000 # Kikuchi Yoshitake
}
1550.1.1={
	holder=68600 # Arima Haruzumi
}
1566.3.19={
	holder=68602 # Arima Yoshisada
}
1577.1.15={
	holder=68603 # Arima Sumisada
	liege=d_hizen
}