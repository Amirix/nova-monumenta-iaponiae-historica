###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_shikama.txt
# @param title c_shikama
# @param title_name Shikama
# @param d_harima
# @param k_sanyo
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1179.12.18={
	liege = "d_harima"
	holder=110070 # Taira Yukimori
}
1183.8.17={
	liege=0
	holder=0 # Taira's escape to the west
}
##
## Kamakura shogunate
##


###
### Ashikaga shogunate [Muromachi shogunate]
###
1480.1.1={
	holder = 53701 # Kodera Masataka
	liege="d_harima"
}
1519.1.1={
	holder = 53702 # Kodera Norimoto
}
1545.1.1={
	holder = 53703 # Kodera Masamoto
}
1575.12.2={
	liege="e_japan"
}
1579.10.1={
	liege=0
}
1580.2.2={
	liege="e_japan"
	holder = 29400 # Hashiba Hideyoshi
}
