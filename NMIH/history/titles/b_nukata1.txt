# Okazaki, Nukata, Mikawa
# k_tokai > d_mikawa > c_nukata > b_nukata1 = Okazaki

1531.1.1={
	law = agnatic_succession
	law = succ_primogeniture
	liege="c_nukata"
	holder=24000 # Matsudaira Kiyoyasu
}
1535.12.29={
	holder=24008 # Matsudaira Nobusada
}
1537.6.8={
	holder=24002 # Matsudaira Hirotada
}
1549.4.3={
	holder=23010 # Imagawa Yoshimoto
}
1558.3.1={
	holder=24006 # Matsudaira Motonobu (Tokugawa Ieyasu)
}
1570.1.1={
	holder=24027 # Matsudaira Nobuyasu
}
1579.10.5={
	holder=24006 # Tokugawa Ieyasu
}
1579.11.1={
	holder=24158 # Ishikawa Kazumasa
}