###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_ikuha.txt
# @param title c_ikuha
# @param title_name Ikuha
# @param d_chikugo
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
180.1.1={
	holder=290001 # Himiko
}
248.1.1={
	holder=290005 # Toyo
}
270.1.1={
	holder=290006 # Nukate
}
310.1.1={
	holder=290007 # Semoko
}
340.1.1={
	holder=290009 # Natsuha
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_chikuzen"
	holder=2750200 # Mike Atsutane
}
1185.3.4={
	liege=0
	holder=0 # Battle of Ashiya-no-ura
}
##
## Kamakura shogunate
##

###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.1={
	liege=d_bungo
}
1480.1.1={
	holder=64603 # Monchusho Chikayasu
}
1500.1.1={
	holder=64601 # Monchusho Chikateru
}
1531.1.1={
	holder=64600 # Monchusho Akitoyo
}
1561.1.1={
	holder=64602 # Monchusho Shigetsura
}
1575.1.1={
	holder=64621 # Monchusho Munekage
}
1580.1.1={
	liege=d_hizen # Ryuzoji Takanobu
}