###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_Ama.txt
# @param title c_Ama
# @param title_name Ama
# @param d_bungo
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
230.1.1={
	holder=290055 # Ao
	liege=d_bungo
}
300.1.1={
	holder=290056 # Shiro
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege="d_bungo"
	holder=2780002 # Usuki Koretaka
}
##
## Kamakura shogunate
##
1185.12.16={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1400.1.1={
	liege=d_bungo
}
1470.1.1={
	holder=64203 # Saeki Koreyasu
}
1500.1.1={
	holder=64201 # Saeki Koretsune
}
1519.1.1={
	holder=64200 # Saeki Koremasu
}
1546.1.1={
	holder=64202 # Saeki Korenori
}
1578.12.17={
	holder=64212 # Saeki Koresada
}