###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kami_adachi.txt
# @param title c_kami_adachi
# @param title_name Kami=Adachi
# @param d_musashi
# @param k_western_kanto
#
###############################################################################
0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_musashi"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1140.1.1={
#	liege="d_musashi"
	holder=2202005 # Adachi Tokane
}
1170.1.1={
#	liege="d_musashi"
	holder=2202010 # Adachi Tomoto
}

##
## Kamakura shogunate
##
1180.6.15={
	liege="k_western_kanto"
#	holder=2202010 # Adachi Tomoto
}
1192.7.12={
	liege="e_japan"
#	holder=2202010 # Adachi Tomoto
}
1210.1.1={
#	liege="e_japan"
	holder=2202020 # Adachi Motoharu
}
1220.1.1={
	liege=0
	holder=0
}

##
## the Ashikaga (Muromachi) shogunate
##
1430.1.1={
	liege="d_sagami"
	holder=18350 # Ota Sukekiyo
}
1456.1.1={
#	liege="d_sagami"
	holder=18351 # Ota Sukenaga(Dokan)
}
1457.1.1={
	liege="d_musashi"
#	holder=18351 # Ota Sukenaga(Dokan)
}
1486.8.25={
#	liege="d_musashi"
	holder=18361 # Ota Eigan
}
1524.2.1={
	holder=18363 # Ota Sukeyori
}
1536.5.10={
	holder=18364 # Ota Sukeaki
}
1546.5.19={
	liege=0
}
1547.11.21={
	holder=18365 # Ota Sukemasa
}
1548.1.1={
	liege="d_musashi"
}
1561.1.1={
	liege="d_echigo"
}
1561.5.10={
	liege="k_kanto_kanrei"
}
1564.1.1={
	liege="d_musashi"
	holder=18366 # Ota Ujisuke
}
1567.9.25={
	holder=16004 # Hojo Ujiyasu
}
1571.10.21={
	liege=0
	holder=16014 # Hojo Ujimasa
}
1580.1.1={
	liege="d_musashi"
	holder=16030 # Hojo gengoro
}
1582.7.27={
	holder=16031 # Hojo (Ota) Ujifusa
}