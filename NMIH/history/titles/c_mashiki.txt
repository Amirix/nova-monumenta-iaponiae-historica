###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_mashiki.txt
# @param title c_mashiki
# @param title_name Mashiki
# @param d_higo
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=290080 # Uchisatu
	liege=d_higo
}
280.1.1={
	liege=0
}
295.1.1={
	holder=290081 # Kubisatsu
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege="d_higo"
	holder=2780652 # Kikuchi Hidenao
}
##
## Kamakura shogunate
##
1185.4.25={
	liege=0
	holder=0 # Battle of Dan-no-ura
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1484.1.1={
	holder=66202 # Aso Korenori
}
1500.1.1={
	holder=66203 # Aso Korenaga
}
1505.1.1={
	holder=66201 # Aso Koretoyo
}
1513.1.1={
	holder=66200 # Aso Koresaki
}
1543.1.1={
	holder=66201 # Aso Koretoyo
}
1559.1.1={
	holder=66204 # Aso Koremasa
}
1579.1.1={
	liege=d_bungo
}
