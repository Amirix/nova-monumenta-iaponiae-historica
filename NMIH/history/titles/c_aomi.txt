###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_aomi.txt
# @param title c_aomi
# @param title_name Aomi
# @param d_mikawa
# @param k_tokai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_mikawa"
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1179.12.18={
	liege="d_mikawa" # Taira Tomonori
	holder=110049 # Taira Tomonori
}

1180.11.9={
#	liege="d_mikawa" # Minamoto Yukiie
	holder=120072 # Minamoto Yukiie / Battle of Fujikawa
}

1181.4.25={
	liege="k_western_kanto"
	holder=120075 # Minamoto Yoritomo / Battle of Sunomata
}
1184.6.1={
	liege=0
	holder=0
}


###
### Ashikaga shogunate [Muromachi shogunate]
###
1379.4.1={
	liege="d_mikawa"
	holder=49505 # Isshiki Norimitsu
}
1388.3.4={
	holder=49507 # Isshiki Akinori
}
1406.6.22={
	holder=49509 # Isshiki Mitsunori
}
1409.1.25={
	holder=49512 # Isshiki Yoshitsura
}
1440.6.14={
	liege="d_mikawa"
	holder=40526 # Hosokawa Mochitsune
}
1450.1.28={
	holder=40528 # Hosokawa Shigeyuki
}
1460.1.1={
	holder=2410200 # Tojo Kuniuji
}
1476.9.1={
	liege=0
	holder=24518 # Mizuno Sadamori
}
1487.1.1={
	holder=24505 # Mizuno Katamasa
}
1514.1.1={
	holder=24500 # Mizuno Tadamasa
}
1543.8.22={
	liege=0 # 
	holder=24502 # Mizuno Nobumoto
}
1568.11.17={
	liege="e_japan" # 40042 Ashikaga Yoshiaki
	holder=24502 # Mizuno Nobumoto
}
1576.1.27={
	liege="e_japan" # 29120 Oda Nobunaga
	holder=29510 # Sakuma Nobumori
}
1576.9.1={
	liege="d_owari"
	holder=24517 # Mizuno Tadashige
}
1582.6.21={
	liege="d_mikawa"
}
