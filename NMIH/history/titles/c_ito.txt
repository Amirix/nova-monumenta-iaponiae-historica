###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_ito.txt
# @param title c_ito
# @param title_name Ito
# @param d_chikuzen
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
160.1.1={
	holder=290011 # Ubako
	liege=d_chikuzen
}
220.1.1={
	holder=290012 # Iguro
}
250.1.1={
	holder=290013 # Ateri
}
280.1.1={
	holder=290014 # Fuku
}
310.1.1={
	holder=290015 # Itote
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_chikuzen"
	holder=2750100 # Harada Taneto
}
1185.3.4={
	liege=0 # Battle of Ashiya-no-ura
}
##
## Kamakura shogunate
##
1213.3.26={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###



1350.1.1={
	holder=67616 # Harada Taneharu
}
1380.1.1={
	holder=67614 # Harada Tanehisa
}
1400.1.1={
	liege=k_western_chugoku
}
1400.1.1={
	holder=67634 # Harada Taneyoshi
}
1430.1.1={
	holder=67613 # Harada Taneyasu
}
1460.1.1={
	holder=67603 # Harada Tanechika
}
1490.1.1={
	holder=67602 # Harada Hirotane
}
1510.1.1={
	holder=67601 # Harada Okitane
}
1529.1.1={
	holder=67600 # Harada Takatane
}
1551.11.1={
	liege=0
}
1557.5.1={
	liege=d_aki # Mori Motonari
}
1557.8.6={
	liege=d_bungo
}
1559.1.1={
	liege=d_aki
}
1563.1.1={
	liege=d_bungo # Otomo Yoshishige
}
1567.1.1={
	liege=d_aki # Mori Motonari
}
1568.8.1={
	liege=d_bungo # Otomo Yoshishige
}
1579.1.1={
	liege=d_hizen # Ryuzoji Takanobu
}