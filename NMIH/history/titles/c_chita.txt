###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_chita.txt
# @param title c_chita
# @param title_name Chita
# @param d_owari
# @param k_chubu
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_owari"
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.1.1={
	liege="d_owari"
	holder=2432011 # Osada Tadamune
}
1183.8.17={
	liege="k_western_kanto" # Taira's escape to the west
	holder=2432011 # Osada Tadamune
}
1190.1.1={
	liege=0
	holder=0
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1392.1.24={
	liege="d_mikawa"
	holder=49507 # Isshiki Akinori
}
1406.6.22={
	holder=49509 # Isshiki Mitsunori
}
1409.1.25={
	holder=49512 # Isshiki Yoshitsura
}
1440.6.14={
	holder=42001 # Takeda Nobuhide
}
1440.8.20={
	holder=56104 # Takeda Nobukata
}
1441.9.25={
	liege="d_tango"
	holder=49514 # Isshiki Norichika
}
1451.12.21={
	holder=49518 # Isshiki Yoshito
}
1478.1.1={
	liege="d_owari" 
	holder=29490 # Saji Tametsuna
}
1500.1.1={
#	liege="d_owari" 
	holder=29491 # Saji Tamekatsu
}
1525.1.1={
#	liege="d_owari" 
	holder=29492 # Saji Tametsugu
}
1550.1.1={
#	liege="d_owari" 
	holder=29493 # Saji Tamehira
}
#1555.01.01={
#	liege="d_owari" # 29120 Oda Nobunaga
#	#holder=29493 # Saji Tamehira
#}
1575.1.1={
	liege="d_owari" # 29120 Oda Nobunaga
	holder=29495 # Saji Kazunari
}
#1576.1.9={
#	liege="d_owari" # 29160 Oda Nobutada
#	holder=29495 # Saji Kazunari
#}
