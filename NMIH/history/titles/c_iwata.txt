###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_iwata.txt
# @param title c_iwata
# @param title_name Iwata
# @param d_totomi
# @param k_tokai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_totomi"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

##
## the Taira clan Government
##
1180.1.1={
	liege="d_suruga" # Tachibana Tomochi
	holder=2390300 # Asaba Munenobu
}

1180.11.9={
	liege="d_totomi" # Battle of Fujikawa
#	holder=2390300 # Asaba Munenobu
}
1181.4.28={
#	liege="d_totomi" # Yasuda Yoshisada
	holder=120113 # Yasuda Yoshisada
}
1185.4.25={
	liege=0
	holder=0
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1336.1.1={
#	liege=
	holder=2395214 # Imagawa Norikuni
}
1339.10.1={
#	liege=
	holder=2396021 # Nikki Yoshinaga
}
1352.2.1={
#	liege=
	holder=2395214 # Imagawa Norikuni
}
1384.6.8={
#	liege=
	holder=2395221 # Imagawa Sadayo
}
1400.1.17={
	liege="d_suruga"
	holder=2395260 # Imagawa Sadaomi
}
1410.1.1={
#	liege="d_suruga"
	holder=2395270 # Imagawa Sadasuke
}
1430.1.1={
#	liege="d_suruga"
	holder=2395273 # Imagawa Norimasa
}
1459.10.1={
#	liege="d_totomi"
	holder=29019 # Shiba Yoshitoshi
}
1460.1.1={
#	liege="d_totomi"
	holder=29021 # Shiba Yoshihiro
}
1461.9.6={
#	liege="d_totomi"
	holder=57621 # Shiba Yoshikado
}
1466.9.2={
#	liege="d_totomi"
	holder=29019 # Shiba Yoshitoshi
}
1466.10.23={
#	liege="d_totomi"
	holder=57621 # Shiba Yoshikado
}
1467.6.27={
#	liege="d_totomi"
	holder=29019 # Shiba Yoshitoshi
}
1474.1.1={
#	liege="d_suruga"
	holder=2395276 # Imagawa Sadanobu
}
1475.1.1={
#	liege="d_suruga"
	holder=2395500 # Imagawa Sadamoto
}
1508.8.1={
	liege="d_totomi"
#	holder=2395500 # Imagawa Sadamoto
}
1536.4.7={
	liege=0
#	holder=2395500 # Imagawa Sadamoto
}
1537.1.1={
	liege="d_totomi"
	holder=23010 # Imagawa Yoshimoto
}
1560.6.12={
#	liege="d_totomi"
	holder=23017 # Imagawa Ujizane
}
1569.1.1={
	liege="d_mikawa"
	holder=24006 # Tokugawa Ieyasu
}
1569.5.17={
	liege="d_totomi"
#	holder=24006 # Tokugawa Ieyasu
}

