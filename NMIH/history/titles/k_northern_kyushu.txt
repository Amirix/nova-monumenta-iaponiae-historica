###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name k_northern_kyushu.txt
# @param title k_northern_kyushu
# @param title_name Northern Kyushu
# @param e_japan
#
###############################################################################
0001.1.1={
	law = agnatic_succession
	law = succ_primogeniture
#	liege=0
#	holder=0
}
1179.12.18={
	liege="e_japan"
	holder=110025 # Taira Kiyomori
}
1181.3.20={
	holder=0
}
1185.4.25={
	law = succ_tanistry
}