###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_tomarioru.txt
# @param title c_tomarioru
# @param title_name Tomarioru
# @param d_maoka
# @param k_kita_ezochi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1170.1.1={
	liege="d_esutoru"
	holder=156600 # Chiuere
}
1210.1.1={
	liege=0
	holder=0 # 
}

1280.1.1={
	holder=157223
}
1340.1.1={
	liege=0
	holder=0 # 
}

1435.1.1={
	holder=156500
}
1495.1.1={
	holder=156501
}
1535.1.1={
	holder=156502
}
1555.1.1={
	holder=156503
}