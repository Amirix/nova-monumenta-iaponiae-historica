###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_yoshino.txt
# @param title c_yoshino
# @param title_name Yoshino
# @param d_yamato
# @param k_southern_kinai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_yamato"
}

#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.1.1={
	liege="k_yamashiro"
	holder=2540105 # Uno Chikaharu
}
1180.6.20={
	liege=0
#	holder=2540105 # Uno Chikaharu. Battle of Uji
}
1186.9.21={
#	liege=0
	holder=0
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1443.10.19={
#	liege=
	holder=41943 # Yamato Takahide
}
1457.12.18={
	liege="d_kawachi"
	holder=48115 # Hatakeyama Yoshihiro
}
1460.10.4={
	liege=0
#	holder=48115 # Hatakeyama Yoshihiro
}
1466.10.1={
	liege="d_kawachi"
	holder=48200 # Ochi Iehide
}
1500.4.4={
#	liege="d_kawachi"
	holder=48201 # Ochi Ienori
}
1507.1.1={
#	liege="d_kawachi"
	holder=48202 # Ochi Ienori
}
1517.5.7={
#	liege="d_kawachi"
	holder=48203 # Ochi Iehide
}
1521.1.1={
	liege="d_kii"
#	holder=48203 # Ochi Iehide
}
1530.1.1={
#	liege="d_kii"
	holder=48204 # Ochi Iehiro
}
1534.1.1={
	liege=0
#	holder=48204 # Ochi Iehiro
}
1550.1.1={
	liege="d_shima"
	holder=26000 # Kitabatake Harutomo
}
1553.1.1={
#	liege="d_shima"
	holder=26003 # Kitabatake Tomonori
}
1560.1.1={
	liege=0
	holder=48204 # Ochi Iehiro
}

1569.1.1={
	holder=48209 # Ochi Ietaka
}
1571.1.1={
	holder=48208 # Ochi Iemasu
}
1576.8.24={
	holder=48210 # Ochi Iehide
}
1577.1.1={
	liege="d_yamato" # 48413 Tsutsui Junkei
}
