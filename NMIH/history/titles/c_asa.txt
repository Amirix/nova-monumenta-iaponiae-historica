###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_asa.txt
# @param title c_asa
# @param title_name Asa
# @param d_nagato
# @param k_western_chugoku
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_nagato"
}

##
## the Taira clan Government
##
1180.1.1={
	liege="d_nagato"
	holder=2661000 # Koto Takemitsu
}
##
## Kamakura shogunate
##
1190.1.1={
	liege=0
	holder=0 # 
}

##
## Ashikaga shogunate [Muromachi shogunate]
##
1397.1.1={
	liege="d_nagato" #　Ouchi Hiroshige
	holder=57011 #　Ouchi Hiroshige
}
1402.2.1={
	#liege="d_nagato"
	holder=57010 #　Ouchi Moriharu
}
1465.9.23={
	#liege="d_nagato"
	holder=57007 #　Ouchi Masahiro
}
1470.3.6={
	#liege="d_nagato"
	holder=57009 #　Ouchi Doton [Noriyuki]
}
1472.1.1={
	#liege="d_nagato"
	holder=57007 #　Ouchi Masahiro
}
1495.10.6={
	#liege="d_nagato"
	holder=57001 #　Ouchi Yoshioki
}
1529.1.29={
	#liege="d_nagato"
	holder=57002 #　Ouchi Yoshitaka
}
1551.9.30={
	#liege="d_nagato"
	holder=57200 # Naito Okimori
}
1555.1.7={
	holder=57251 # Naito Takayo
}
1557.4.30={
	liege="d_aki"
	holder = 57402 # Yoshimi Masayori
}
