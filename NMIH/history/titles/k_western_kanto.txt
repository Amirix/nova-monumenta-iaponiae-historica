###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name k_western_kanto.txt
# @param title k_western_kanto
# @param title_name Western Kanto
# @param e_japan
#
###############################################################################

0001.1.1={
	law = agnatic_succession
	law = succ_primogeniture
}
##
## Kamakura shogunate
##
1180.6.15={
	liege=0
	holder=120075 # Minamoto Yoritomo
}
1192.7.12={
	holder=0 # Become Shogun
}

1546.5.19={
	holder=16004 # Hojo Ujiyasu (battle of Kawagoe)
}
1571.10.21={
	holder=16014 # Hojo Ujimasa
}