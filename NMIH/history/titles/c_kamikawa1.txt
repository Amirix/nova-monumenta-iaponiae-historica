###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kamikawa1.txt
# @param title c_kamikawa1
# @param title_name Kamikawa
# @param d_teshio
# @param k_nishi_ezochi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1066.1.1={
	holder=150202 # Samayunkur
}
1130.1.1={
	liege=0
	holder=0 # 
}

1170.1.1={
	liege=0
	holder=154600 # Momo
}
1210.1.1={
	liege=0
	holder=0 # 
}

1270.1.1={
	holder=157204
}
1310.1.1={
	liege=0
	holder=0 # 
}

1450.1.1={
	holder=154502
}
1490.1.1={
	holder=154500
}
1520.1.1={
	holder=154501
}