###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_ichishi.txt
# @param title c_ichishi
# @param title_name Ichishi
# @param d_ise
# @param k_chubu
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_ise"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

##
## the Taira clan Government
##
1180.1.1={
	liege="d_ise" # Taira Moritoshi
	holder=110250 # Taira Moritoshi
}
1183.8.17={
	liege="d_shima" # Ito Tadakiyo / Taira's escape to the west
	holder=131576 # Ito Tadakiyo
}

##
## Kamakura shogunate
##
1184.9.23={
	liege=0
	holder=0 # Failure of Taira's Three-day rebellion
}


1383.7.1={
	holder=26050 # Kitabatake Akiyasu
}
1414.1.1={
	holder=26052 # Kitabatake Mitsumasa
}
1429.1.25={
	holder=26006 # Kitabatake Noritomo
}
1471.4.13={
	holder=26005 # Kitabatake Masasato
}
1508.12.15={
	holder=26001 # Kitabatake Kichika
}
1517.1.1={
	holder=26000 # Kitabatake Harutomo
}
1553.1.1={
	holder=26003 # Kitabatake Tomonori
}
1563.10.4={
	holder=26007 # Kitabatake Tomofusa
}
1569.11.21={
	liege="d_shima" # 29120 Oda Nobunaga
	holder=29120 # Oda Nobunaga
}
1575.1.1={
	liege="d_shima" # 29161 Oda Nobukatsu
	holder=29161 # Oda Nobukatsu
}
