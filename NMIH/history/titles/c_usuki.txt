###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_usuki.txt
# @param title c_usuki
# @param title_name Usuki
# @param d_hyuga
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=41352 # Takekukuchi a.k.a Kukuchihiko
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege="d_hyuga"
	holder=2810720 # Tsuchimochi Eimyo
}
##
## Kamakura shogunate
##
1223.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1380.1.1={
	holder=60756 # Tsuchimochi Kanenobu
}
1410.1.1={
	holder=60755 # Tsuchimochi Nobutsuna
}
1440.1.1={
	holder=60755 # Tsuchimochi Kaneshige
}
1470.1.1={
	holder=60753 # Tsuchimochi Tsunetsuna
}
1500.1.1={
	holder=60751 # Tsuchimochi Chikahide
}
1531.1.1={
	holder=60750 # Tsuchimochi Chikasuke
}
1554.1.1={
	holder=60752 # Tsuchimochi Chikashige
}
1578.4.10={
	holder=64002 # Otomo Yoshishige(Sorin)  Annexed by Otomo (Ito)
}
1578.11.14={
	holder=60760 # Tsuchimochi Takanobu
}