###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_hoki.txt
# @param title d_hoki
# @param title_name Hoki
# @param k_sanin
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="e_tenno"
}
0878.9.18={
	holder=1004544 # Fujiwara no Yasutsugu
}
0886.2.27={
	holder=0 #  
}
0993.1.1={
	holder=220040 # Oe no Kiyomichi
}
0997.7.9={
	holder=0
}
1016.1.24={
	holder=1003338 # Fujiwara no Takasuke
}
1021.1.1={
	holder=0
}
1091.1.1={
	holder=1006695 # Fujiwara no Takatada
}
1094.2.22={
	holder=0
}
1117.11.26={
	holder=110026 # Taira no Tadamori
}
1120.11.25={
	holder=0
}

##
## the Taira clan Government
##
1180.1.1={
	liege=0
}
#1180.3.3={
#	holder=1000158 # Fujiwara Kanezane
#}
#1184.1.10={
#	holder=0 #  
#}
##
## Kamakura shogunate
##
1264.10.9={
	holder=131997 # Hojo Tokisuke
}
1272.3.15={
	holder=0 #  
}
1330.1.1={
	holder=132083 # Hojo Tokimasu
}
1333.6.21={
	holder=0 #  
}
###
### Ashikaga shogunate [Muromachi shogunate]
###
1336.1.1={
	holder=0 #  
}
1337.3.1={
	holder=2186256 # Yamana Tokiuji
}
1351.8.1={
	holder=0 #  
}
1363.9.1={
	holder=2186256 # Yamana Tokiuji
}
1371.2.1={
	holder=50011 # Yamana Tokiyoshi
}
1389.5.29={
	holder=50013 # Yamana Ujiyuki
}
1390.1.1={
	holder=50014 # Yamana Mitsuyuki
}
1391.12.1={
	holder=50013 # Yamana Ujiyuki
}
1424.11.1={
	holder=50017 # Yamana Noriyuki
}
1453.1.1={
	holder=50018 # Yamana Toyoyuki
}
1471.10.31={
	holder=50023 # Yamana Masayuki
}
1473.4.15={
	liege=0
	#holder=50023 # Yamana Masayuki
}
1476.1.1={
	holder=50021 # Yamana Motoyuki
}
1489.1.1={
	holder=50023 # Yamana Masayuki
}
1491.1.1={
	holder=50024 # Yamana Hisayuki
}
1494.9.24={
	holder=50026 # Yamana Sumiyuki
}
1524.1.1={
	holder=50058 # Yamana Toyooki
}
1533.9.15={
	holder=0
}
