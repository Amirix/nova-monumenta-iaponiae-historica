###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_etorofu.txt
# @param title c_etorofu
# @param title_name etorofu
# @param d_chishima
# @param k_higashi_ezochi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1170.1.1={
	liege="d_chishima"
	holder=153100 # Atoi
}
1210.1.1={
	liege=0
	holder=0 # 
}


1450.1.1={
	liege="d_chishima"
	holder=153000
}
1510.1.1={
	holder=153001
}
