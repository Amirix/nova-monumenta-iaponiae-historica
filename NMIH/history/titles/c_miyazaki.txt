###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_miyazaki.txt
# @param title c_miyazaki
# @param title_name Miyazaki
# @param d_hyuga
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=41352 # Takekukuchi a.k.a Kukuchihiko
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege="d_hyuga"
	holder=2810001 # Kusakabe Morihira
}
##
## Kamakura shogunate
##
1190.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1300.1.1={
	liege="d_hyuga"
}
1387.6.20={
	holder=60004 # Shimazu Hisatoyo
}
1425.2.18={
	holder=60007 # Shimazu Suehisa
}
1477.8.6={
	holder=60018 # Shimazu Tadakado
}
1492.1.1={
	holder=60003 # Shimazu Tadatomo
}
1500.1.1={
	liege=0
}
1543.1.1={
	holder=60019 # Shimazu Tadahiro
	liege="d_hyuga"
}
1549.1.1={
	holder=60020 # Shimazu Tadachika
}
1571.7.4={
	holder=60048 # Shimazu Tomochika
}