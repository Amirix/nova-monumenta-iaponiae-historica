###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_higashi_kubiki.txt
# @param title c_higashi_kubiki
# @param title_name Higashi-Kubiki
# @param d_echigo
# @param k_hokuriku
#
###############################################################################

0697.1.1={
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_echigo"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_echigo"
	holder=2270130 # Jo Sukenaga
}
1181.3.11={
	holder=2270131 # Jo Nagamochi
}
1181.7.25={
	liege="k_shinano"
	holder=120084 # Minamoto [Kiso] Yoshinaka / Battle of Yokota-gawara
}
1183.6.22={
	liege="k_hokuriku"
#	holder=120084 # Minamoto Yoshinaka / Battle of Shinohara
}
1183.8.17={
	liege="e_japan"
#	holder=120084 # Minamoto Yoshinaka / Taira's escape to the west
}
##
## Kamakura shogunate
##
1184.3.4={
	liege=0
	holder=0 # Battle of Awazu
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1300.1.1={
	liege="d_echigo"
}
1360.1.1={
	holder=2215326 # Nagao Kagetsune
}
1367.1.1={
	holder=32003 # Nagao Takakage
}
1389.3.26={
	holder=32004 # Nagao Kunikage
}
1450.12.16={
	holder=32006 # Nagao Yorikage
}
1469.10.6={
	holder=32007 # Nagao Shigekage
}
1482.3.14={
	holder=32008 # Nagao Yoshikage
}
1506.10.5={
	holder=32000 # Nagao Tamekage
}
1543.1.29={
	#liege="d_echigo" # 32009 Nagao Harukage
	holder=32009 # Nagao Harukage
}
1548.12.30={
	#liege="d_echigo" # 32011 Uesugi Kenshin
	holder=32011 # Uesugi Kenshin
}
1578.4.19={
	#liege="d_echigo" # 32032 Uesugi Kagekatsu
	holder=32032 # Uesugi Kagekatsu
}
