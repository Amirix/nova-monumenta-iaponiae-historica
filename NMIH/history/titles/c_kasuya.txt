###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kasuya.txt
# @param title c_kasuya
# @param title_name Kasuya
# @param d_chikuzen
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
240.1.1={
	holder=290040 # Tamo
	liege=d_chikuzen
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_chikuzen"
	holder=2750100 # Harada Taneto
}
1185.3.4={
	liege=0
	holder=0 # Battle of Ashiya-no-ura
}
##
## Kamakura shogunate
##


1330.1.1={
	holder=64011 # Otomo Sadatoshi
}
###
### Ashikaga shogunate [Muromachi shogunate]
###
1350.1.1={
	holder=64012 # Tachibana Munenori
}
1375.1.1={
	holder=67404 # Tachibana Chikanao
}
1400.1.1={
	liege=k_western_chugoku
}
1440.1.1={
	holder=67403 # Tachibana Chikamasa
}
1480.1.1={
	holder=67401 # Tachibana Munekatsu
}
1527.1.1={
	holder=67400 # Tachibana Akimitsu
}
1551.11.1={
	liege=0
}
1557.8.6={
	liege=d_bungo
	holder=67402 # Tachibana Akitoshi
}
1562.1.1={
	liege=d_aki # Mori Motonari
}
1563.1.1={
	liege=d_bungo # Otomo Yoshishige
}
1567.1.1={
	liege=d_aki # Mori Motonari
}
1568.9.5={
	liege=d_bungo
	holder=64002 # Otomo Yoshishige
}
1572.1.1={
	holder=67406 # Tachibana Akitsura
}
