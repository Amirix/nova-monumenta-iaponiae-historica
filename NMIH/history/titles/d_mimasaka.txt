###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_mimasaka.txt
# @param title d_mimasaka
# @param title_name Mimasaka
# @param k_sanyo
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="e_tenno"
}
##
## the Taira clan Government
##
1180.1.1={
	liege = "k_sanyo"
	holder=2630300 # Emi Morinobu
}
1181.3.20={
	liege = "e_japan"
}
1183.8.17={
	liege= "k_western_chugoku"
}
1184.3.20={
	liege=0
	holder=0 # Battle of Ichi-no-Tani
}
###
### Ashikaga shogunate [Muromachi shogunate]
###
1552.1.1={
	holder = 51508 # Amago Haruhisa
}
1561.1.9={
	holder = 0
}
