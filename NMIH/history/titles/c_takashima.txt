###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_takashima.txt
# @param title c_takashima
# @param title_name Takashima
# @param d_omi
# @param k_northern_kinai
# @param e_japan
#
###############################################################################

0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_omi"
}

#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

##
## the Taira clan Government
##
1180.1.1={
	liege="e_japan"
	holder=120213 # Minamoto Shigesada
}

1183.8.17={
	liege="d_omi" # Yamamoto Yoshitsuna
	holder=2515013 # Yamamoto [Nishigori] Yoshitaka / Taira's escape to the west
}

##
## Kamakura shogunate
##
1184.3.4={
	liege="k_western_kanto"
	holder=2510030 # Sasaki Hideyoshi / Battle of Awazu
}
1184.8.26={
	liege="d_omi"
	holder=2510060 # Sasaki Sadatsuna / the three-day rebellion of the Taira clan
}
1205.4.29={
#	liege="d_omi"
	holder=2510070 # Sasaki Hirotsuna
}
1221.7.22={
#	liege="d_omi"
	holder=2510073 # Sasaki Nobutsuna / the Jokyu War
}
1242.4.7={
#	liege="d_omi"
	holder=2510082 # Sasaki [Takashima] Takanobu
}
1270.1.1={
#	liege=
	holder=2510300 # Takashima Yasunobu
}
1290.1.1={
#	liege=
	holder=2510310 # Kutsuki Yoshitsuna
}
1310.1.1={
#	liege=
	holder=2510315 # Kutsuki Tokitsune
}
1330.1.1={
#	liege=
	holder=2510320 # Kutsuki Yoshiuji
}

###
### Ashikaga shogunate [Muromachi shogunate]
###

1340.1.1={
	liege="e_japan"
	holder=2510325 # Kutsuki Tsuneuji
}
1363.1.1={
#	liege="e_japan"
	holder=2510330 # Kutsuki Ujihide
}
1408.1.1={
#	liege="e_japan"
	holder=2510335 # Kutsuki Yoshitsuna
}
1420.1.1={
#	liege="e_japan"
	holder=2510340 # Kutsuki Tokitsuna
}
1445.1.1={
#	liege="e_japan"
	holder=2510345 # Kutsuki Sadataka
}
1475.1.1={
#	liege="e_japan"
	holder=45503 # Kutsuki Sadatsuna
}
1485.8.27={
#	liege="e_japan"
	holder=45501 # Kutsuki Sadakiyo
}
1508.7.28={
	liege="k_southern_kinai"
	holder=40012 # Ashikaga Yoshizumi, exiled from Otagi
}
1511.9.6={
	liege="e_japan"
	holder=45501 # Kutsuki Sadakiyo
}
1523.1.1={
	holder=45500 # Kutsuki Tanetsuna
}
1527.3.14={
	holder=40000 # Ashikaga Yoshiharu, exiled from Otagi
}
1534.1.1={
	holder=45500 # Kutsuki Tanetsuna
}
1549.1.1={
	holder=45502 # Kutsuki Harutsuna
}
1549.7.18={
	holder=40040 # Ashikaga Yoshiteru, exiled from Otagi
}
1552.1.1={
	holder=45504 # Kutsuki Mototsuna
}
1553.9.8={
	holder=40040 # Ashikaga Yoshiteru, exiled from Otagi, again.
}
1558.11.1={
	holder=45504 # Kutsuki Mototsuna
}
1565.6.17={
	liege=0
}
1571.2.24={
	liege="k_chubu" # 29120 Oda Nobunaga
	holder=43131 # Isono Kazumasa
}

###
### The Oda government 
###
1575.1.1={
	liege="d_omi" # 29120 Oda Nobunaga
}
1578.2.3={
	holder=29186 # Oda Nobuzumi
}
1582.6.21={
	liege=0
	holder=45504 # Kutsuki Mototsuna
}
1582.7.2={
	liege="k_northern_kinai"
#	holder=45504 # Kutsuki Mototsuna
}