###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_sawata.txt
# @param title c_sawata
# @param title_name Sawata
# @param d_sado
# @param k_hokuriku
#
###############################################################################

0697.1.1={
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_echigo"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1179.12.18={
	liege="d_sado"
	holder=110124 # Taira Nakamori
}
1181.7.25={
	liege="k_shinano"
	holder=120084 # Minamoto [Kiso] Yoshinaka / Battle of Yokota-gawara
}
1183.6.22={
	liege="k_hokuriku"
#	holder=120084 # Minamoto Yoshinaka / Battle of Shinohara
}
1183.8.17={
	liege="e_japan"
#	holder=120084 # Minamoto Yoshinaka / Taira's escape to the west
}
##
## Kamakura shogunate
##
1184.3.4={
	liege=0
	holder=0 # Battle of Awazu
}

1221.8.1={
#	liege=
	holder=2290001 # Honma Yoshihisa
}
1230.1.1={
#	liege=
	holder=2290005 # Honma Tadatsuna
}
1260.1.1={
#	liege=
	holder=2290012 # Honma Munetada
}
1285.1.1={
#	liege=
	holder=2290015 # Honma Sadatsura
}
1310.1.1={
#	liege=
	holder=2290020 # Honma Yasusada
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1335.1.1={
#	liege=
	holder=2290025 # Honma Yorinao
}
1360.1.1={
#	liege=
	holder=2290030 # Honma Yasunobu
}
1385.1.1={
#	liege=
	holder=2290035 # Honma Arinao
}
1410.1.1={
	liege="d_sado"
	holder=2290040 # Honma Naofuyu
}
1435.1.1={
#	liege="d_sado"
	holder=2290045 # Honma Arishige
}
1458.1.1={
	holder=34003 # Honma Shigenao
}
1474.1.1={
	holder=34002 # Honma Yasushige
}
1498.1.1={
	holder=34001 # Honma Yasunao
}
1527.1.1={
	holder=34000 # Honma Yasutoki
}
1537.1.1={
	holder=34004 # Honma Ariyasu
}
1552.1.1={
	holder=34005 # Honma Yasutaka
}