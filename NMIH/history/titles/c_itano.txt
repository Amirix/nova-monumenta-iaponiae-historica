###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_itano.txt
# @param title c_itano
# @param title_name Itano
# @param d_awa
# @param k_shikoku
#
###############################################################################

100.1.1={
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_awa"
}
#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege=0
	holder=2690104 # Kondo Hironaga
}
1182.1.1={
	holder=2690105 # Kondo Chikaie
}
##
## Kamakura shogunate
##
1210.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1300.1.1={
	liege="d_awa"
}
1392.3.25={
	holder = 40524 # Hosokawa Yoshiyuki
}
1402.1.1={
	holder = 40525 # Hosokawa Mitsuhisa
}

1430.10.15={
	holder = 40526 # Hosokawa Mochitsune
}

1450.1.28={
	holder = 40528 # Hosokawa Shigeyuki
}

1479.1.1={
	holder = 40530 # Hosokawa Masayuki
}

1488.9.4={
	holder = 40529 # Hosokawa Yoshiharu
}

1495.1.17={
	holder = 40532 # Hosokawa Yukimochi
}

1508.7.28={
	holder=40501 # Hosokawa Sumimoto
}
1520.6.24={
	holder=40500 # Hosokawa Harumoto
}
1534.1.1={
	holder=40534 # Hosokawa Mochitaka
}
1553.7.27={
	holder=59008 # Miyoshi Koretora
}
1562.4.8={
	holder=59018 # Miyoshi Nagaharu
}
1577.4.16={
	holder=59019 # Miyoshi Masayasu
}
1582.10.7={
	liege="d_tosa"
	holder=59104 # Chosokabe Motochika
}
