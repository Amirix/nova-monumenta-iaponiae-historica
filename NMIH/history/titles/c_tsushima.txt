###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_tsushima.txt
# @param title d_tsushima
# @param title_name Tsushima
# @param d_tsushima
# @param k_northern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
235.1.2={
	holder=290050 # Sayaku
	liege=d_chikuzen
}
764.11.6={
#	holder=41023 # Empress Koken/Shotoku
	liege=0
}
##
## the Taira clan Government
##
1180.1.1={
	liege="d_tsushima"
	holder=1004037 # Hino Chikamitsu
}
1185.2.2={
	liege=0
	holder=0 # Escape to Korea 
}
##
## Kamakura shogunate
##
1246.1.1={
	holder=69240 # So Shigehisa
}
1261.1.1={
	holder=69245 # So Sukekuni
}
1274.11.7={
	holder=69246 # So Moriaki
}
1300.1.1={
	holder=69248 # So Morikuni
}
1350.1.1={
	holder=69249 # So Tsuneshige
}
1380.1.1={
	holder=69205 # So Sadashige
}
1418.4.1={
	holder=69207 # So Yorishige
}
1452.6.22={
	holder=69208 # So Shigemoto
}
1468.1.1={
	holder=69203 # So Sadakuni
}
1494.8.18={
	holder=69212 # So Kimori
}
1508.1.1={
	holder=69213 # So Yoshimori
}
1520.1.1={
	holder=69215 # So Morinaga
}
1521.1.1={
	holder=69200 # So Masamori
}
1539.1.1={
	holder=69218 # So Hareyasu
}
1553.1.1={
	holder=69219 # So Yoshishige
}
1566.1.1={
	holder=69220 # So Shigehisa
}
1570.1.1={
	holder=69221 # So Yoshizumi
}
1579.1.1={
	holder=69222 # So Yoshitoshi
}