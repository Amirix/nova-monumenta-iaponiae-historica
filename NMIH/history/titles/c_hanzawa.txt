###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_hanzawa.txt
# @param title c_hanzawa
# @param title_name Hanzawa
# @param d_musashi
# @param k_western_kanto
#
###############################################################################
0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_musashi"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1035.1.1={
#	liege="d_musashi"
	holder=2206000 # Inomata Tokinori
}
1070.1.1={
#	liege="d_musashi"
	holder=2206005 # Inomata Tadakane
}
1100.1.1={
#	liege="d_musashi"
	holder=2206011 # Inomata Masaie
}
1130.1.1={
#	liege="d_musashi"
	holder=2206020 # Inomata Suketsuna
}
1160.1.1={
#	liege="d_musashi"
	holder=2206025 # Inomata Noritsuna
}

1180.1.1={
	liege=0
#	holder=2206025 # Inomata Noritsuna
}

##
## Kamakura shogunate
##
1180.10.26={
	liege="k_western_kanto" # served under Yoritomo
#	holder=2206025 # Inomata Noritsuna
}
1192.1.1={
#	liege="k_western_kanto"
	holder=2206027 # Inomata Noritaka
}
1192.7.12={
	liege="e_japan"
#	holder=2206027 # Inomata Noritaka
}
1207.1.1={
	liege=0
	holder=0
}

##
## the Ashikaga (Muromachi) shogunate
##
1404.9.6={
	liege="d_musashi"
	holder=17050 # Uesugi [Fukaya] Norimitsu
}
1420.1.1={
#	liege="d_musashi"
	holder=17051 # Uesugi Norinaga
}
1430.1.1={
#	liege="d_musashi"
	holder=17052 # Uesugi Norinobu
}
1457.1.1={
	liege="k_kanto_kanrei"
#	holder=17052 # Uesugi Norinobu
}
1470.1.1={
#	liege="k_kanto_kanrei"
	holder=17053 # Uesugi Fusanori
}
1500.1.1={
	holder=17054 # Uesugi Norikiyo, Fukaya castle
}
1530.1.1={
	holder=17055 # Uesugi Norikata, Fukaya castle
}
1546.5.19={
	liege="d_musashi" # Vassal of Hojo
}
1560.4.6={
	holder=17056 # Uesugi Norimori, Fukaya castle
}
1561.1.1={
	liege="d_echigo" # Vassal of Echigo=Uesugi
}
1561.5.10={
	liege="k_kanto_kanrei"
}
1563.1.1={
	liege="d_musashi" # Vassal of Hojo
}
1569.12.1={
	liege="k_kanto_kanrei" # Vassal of Echigo=Uesugi
}
1575.5.8={
	liege="d_musashi" # Vassal of Hojo
	holder=17057 # Uesugi Ujinori, Fukaya castle
}
