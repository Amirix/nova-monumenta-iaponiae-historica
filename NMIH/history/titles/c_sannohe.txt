###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_sannone.txt
# @param title c_sannohe
# @param title_name Sannohe
# @param d_mutsu
# @param k_northern_oshu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1160.1.1={
	liege="d_mutsu"
	holder=150108 # Akarogu
}
##
## Kamakura shogunate
##
1189.10.3={
	liege=0 # 
	holder=0 # Fall of Hiraizumi
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1334.1.5={
	liege="d_mutsu"
	holder=2040162 # Nanbu Nobunaga
}
1350.1.1={
#	liege="d_mutsu"
	holder=2040180 # Nanbu Masayuki
}
1388.11.17={
#	liege="d_mutsu"
	holder=2040190 # Nanbu Moriyuki
}
1437.5.13={
#	liege="d_mutsu"
	holder=2040200 # Nanbu Yoshimasa
}
1440.8.9={
#	liege="d_mutsu"
	holder=2040201 # Nanbu Masamori
}
1445.1.1={
#	liege="d_mutsu"
	holder=3130 # Nanbu Mitsumasa
}
1449.1.1={
#	liege="d_mutsu"
	holder=3131 # Nanbu Tokimasa
}
1473.4.2={
	holder=3132 # Nanbu Michitsugu
}
1490.1.1={
	holder=3133 # Nanbu Nobutoki
}
1502.1.11={
	holder=3134 # Nanbu Nobuyoshi
}
1503.6.18={
	holder=3135 # Nanbu Masayasu
}
1507.3.23={
	holder=3101 # Nanbu Yasunobu
}
1541.1.1={
	holder=3102 # Nanbu Harumasa
}
1582.1.27={
	holder=3154 # Nanbu Nobunao
}