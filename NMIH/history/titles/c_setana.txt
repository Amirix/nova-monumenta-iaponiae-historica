###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_satana.txt
# @param title c_setana
# @param title_name Setana
# @param d_shiribeshi
# @param k_wajinchi
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1170.1.1={
	liege="d_sum_kur"
	holder=150104 # Kurimain
}
1210.1.1={
	liege=0
	holder=0 # 
}

1270.1.1={
	holder=157200
}
1310.1.1={
	liege=0
	holder=0 # 
}

1450.1.1={
	holder=150005
}
1490.1.1={
	holder=150000
}
1529.3.26={
	holder=150002
}
1536.6.23={
	holder=150003 #Hashitayn
}
1570.1.1={
	holder=150004 #Totake
}