###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_shimane.txt
# @param title c_shimane
# @param title_name Shimane
# @param d_izumo
# @param k_sanin
#
###############################################################################

0701.1.1={
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_izumo"
}

##
## the Taira clan Government
##
1180.1.1={
	liege="d_izumo"
	holder=2600900 # Ono Sueaki
}
1183.8.17={
	liege="k_western_chugoku" # Taira's escape to the west
}

##
## Kamakura shogunate
##
1190.1.1={
	liege=0
	holder=0 # 
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1440.1.1={
	liege="d_izumo" # Kyogoku Mochikiyo
	holder=2601312 # Matsuda Kimiyori
}
1468.6.20={
	liege=0 # rebellion
	holder=2601312 # Matsuda Kimiyori
}
1468.10.7={
	#liege=0
	holder=2601313 # Matsuda Mikawa no kami
}
1471.9.30={
	liege="d_izumo"
	holder=51503 # Amago Kiyosada
}
1479.1.1={
	#liege="d_izumo"
	holder=51501 # Amago Tsunehisa, Kiyosada in retirement
}
#1486.8.1={
#	liege="d_izumo" # Amago Tsunehisa
#	holder=51501 # Amago Tsunehisa
#}
1500.1.1={
	liege="k_sanin"
}
1541.9.30={
	holder = 51508 # Amago Haruhisa
	liege="d_izumo"
}
1561.1.9={
	holder = 51525 # Amago Yoshihisa
}
1563.1.1={
	liege = "k_western_chugoku"
	holder=56004 # Mori Motonari
}
1569.1.1={
	holder = 56022 # Mori(Suetsugu) Motoyasu
}

