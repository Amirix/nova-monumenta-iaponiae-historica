###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_yamagata.txt
# @param title c_yamagata
# @param title_name Yamagata
# @param d_aki
# @param k_western_chugoku
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_aki"
}

1180.1.1={
	liege=0
	holder=2660600 # Yamagata Tametsuna
}
##
## Kamakura shogunate
##
1200.1.1={
	liege=0
	holder=0 #
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1456.3.16={
	liege="d_aki"
	holder=56204 # Kikkawa Yukitsune
}
1477.1.21={
	liege="k_western_chugoku"
	holder = 56203 # Kikkawa Tsunemoto
}
1509.1.1={
	holder = 56202 # KIkkawa Kunitsune
}
1517.1.1={
	liege="k_sanin"
}
1531.5.4={
	holder = 56200 # Kikkawa Okitsune
}
1550.11.5={
	holder=56015 # Kikkawa Motoharu
	liege="d_aki"
}
