###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_echi.txt
# @param title c_echi
# @param title_name Echi
# @param d_omi
# @param k_northern_kinai
#
###############################################################################

0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_omi"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1180.1.1={
#	liege="d_omi" # Yamamoto Yoshitsune
	holder=2515000 # Yamamoto Yoshitsune
}

##
## the Taira clan Government
##
1180.12.24={
	liege="e_japan"
	holder=110025 # Taira Kiyomori / Battle of Omi
}
1181.3.20={
#	liege="e_japan"
	holder=110042 # Taira Munemori
}

1183.8.17={
	liege="d_omi" # Yamamoto Yoshitsune
	holder=2515000 # Yamamoto Yoshitsune / Taira's escape to the west
}

##
## Kamakura shogunate
##
1184.3.4={
	liege="k_western_kanto"
	holder=2510030 # Sasaki Hideyoshi / Battle of Awazu
}
1184.8.26={
	liege="d_omi"
	holder=2510060 # Sasaki Sadatsuna / the three-day rebellion of the Taira clan
}
1205.4.29={
#	liege="d_omi"
	holder=2510070 # Sasaki Hirotsuna
}
1221.7.22={
#	liege="d_omi"
	holder=2510073 # Sasaki Nobutsuna / the Jokyu War
}
1242.4.7={
#	liege="d_omi"
	holder=2510083 # Sasaki [Kyogoku] Ujinobu
}
1295.1.1={
#	liege="d_omi"
	holder=2510090 # Sasaki [Kyogoku] Munetsuna
}
1297.9.27={
#	liege="d_omi"
	holder=2510100 # Sasaki [Kyogoku] Sadamune
}
1305.5.31={
#	liege="d_omi"
	holder=44201 # Sasaki [Kyogoku] Muneuji
}
1329.8.11={
#	liege="d_omi"
	holder=44203 # Sasaki [Kyogoku] Takauji
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1440.1.1={
	liege="d_southern_omi"
	holder=44660 # Iba Mitsutaka
}
1460.1.1={
#	liege="d_southern_omi"
	holder=44661 # Iba Sadataka
}
1467.1.1={
	liege="e_japan"
	holder=44500 # Gamo Sadahide
}
1493.4.23={
	liege="d_southern_omi" # vassal of Rokkaku
}
1514.3.30={
	holder=44503 # Gamo Hidenori
}
1523.4.23={
	holder=44504 # Gamo Sadahide
}
1568.10.12={
	liege="k_chubu" # vassal of Oda
	holder=44505 # Gamo Katahide
}
1575.1.1={
	liege="d_omi" # 29120 Oda Nobunaga
}
1582.6.21={
	liege="d_harima"
	holder=44508 # Gamo Ujisato
}
1582.7.2={
	liege="k_northern_kinai"
#	holder=44508 # Gamo Ujisato
}
