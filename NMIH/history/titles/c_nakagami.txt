###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_nakagami.txt
# @param title c_nakagami
# @param title_name Nakagami
# @param d_okinawa
# @param k_ryukyu
#
###############################################################################

0100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
##
## Tenson Dynasty
##
1140.1.1={
	liege="k_chuzan"
	holder=142003 # Tenson Umikanimatsu
}
1186.1.1={
	holder=142090 # Ri Yu
}

##
## Shunten Dynasty
##
1187.1.1={
	liege="k_chuzan"
	holder=142100 # Shunten Shunten
}
1237.1.1={
	holder=142110 # Shunten Shunbajunki
}
1248.1.1={
	holder=142120 # Shunten Gihon
}

##
## Eiso Dynasty
##
1260.1.1={
	liege="k_chuzan"
	holder=142200 # Eiso Eiso
}
1299.8.31={
	holder=142201 # Eiso Taisei
}
1309.1.19={
	holder=142211 # Eiso Eiji
}
1313.10.10={
	holder=142223 # Eiso Tamagushiku
}
1336.4.22={
	holder=142230 # Eiso Seii
}

##
## Satto Dynasty
##
1349.4.30={
	liege="k_chuzan"
	holder=142301 # Satto Satto
}
1395.11.17={
	holder=142305 # Satto Bunei
}

##
## 1st Sho Dynasty
##
1406.1.1={
	liege="k_chuzan"
	holder=141002 # Sho Shisho
}
1421.1.1={
	holder=141005 # Sho Hashi
}
1430.1.1={
	holder=141232 # Yuntanza Gusamaru
}
1458.8.1={
	holder=141015 # Sho Taikyu
}
1459.1.1={
	holder=140004 # Sho En
}

###
### 2nd Sho Dynasty
###
1476.1.1={
	holder=140005
}
1477.1.1={
	holder=140007
}
1527.1.12={
	holder=140000
}
1555.7.13={
	holder=140027
}
1572.5.12={
	holder=140045
}
1589.1.11={
	holder=140061
}
