###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_iyo.txt
# @param title d_iyo
# @param title_name Iyo
# @param k_shikoku
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="e_tenno"
}
1180.1.1={
	liege=0
	holder=2691005 # Kono Michikiyo
}
1181.2.27={
	holder=2691010 # Kono Michinobu
}
##
## Kamakura shogunate
##
1203.1.1={
	holder=0 #
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1379.12.23={
	liege="e_japan"
	holder=58616 # Kono Michiyoshi
}
1394.12.9={
	holder=58617 # Kono Michiyuki
}
1414.9.1={
	holder=58615 # Kono Michihisa
}
1435.7.24={
	holder=58604 # Kono Norimichi
}
1449.1.1={
	liege=0
#	holder=58604 # Kono Norimichi
}
1467.6.27={
	liege="e_japan"
#	holder=58604 # Kono Norimichi
}
1477.12.16={
	liege=0
#	holder=58604 # Kono Norimichi
}
1500.2.19={
	holder=58601 # Kono Michinobu
}
1519.1.1={
	holder=58600 # Kono Michinao
}
1553.1.1={
	holder=58603 # Kono Michinobu
}
1581.1.1={
	holder=58605 # Kono Michinao
}
