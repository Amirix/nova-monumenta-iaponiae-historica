###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_morokata.txt
# @param title c_morokata
# @param title_name Morokata
# @param d_hyuga
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=41352 # Takekukuchi a.k.a Kukuchihiko
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1180.1.1={
	liege=0
	holder=2810104 # Tomo Nobuaki
}
##
## Kamakura shogunate
##
1185.4.25={
	liege=0
	holder=0 # Battle of Dan-no-ura
}

###
### Ashikaga shogunate [Muromachi shogunate]
###

# 1375: Hongo Yoshihisa
1300.1.1={
	liege="d_hyuga"
}
1352.1.1={
	holder=60258 # Hongo Suketada
}
1360.1.1={
	holder=60257 # Hongo Yoshihisa
}
1370.1.1={
	holder=60256 # Hongo Tomohisa
}
1410.1.1={
	holder=60255 # Hongo Mochihisa
}
1450.1.1={
	holder=60254 # Hongo Toshihisa
}
1480.1.1={
	holder=60253 # Hongo Yoshihisa
}
1500.1.1={
	liege=0
}
1500.1.1={
	holder=60251 # Hongo Kazuhisa
}
1521.1.1={
	holder=60250 # Hongo Tadasuke
}
1543.1.1={
	liege="d_hyuga"
}
1559.12.14={
	holder=60252 # Hongo Tokihisa
}