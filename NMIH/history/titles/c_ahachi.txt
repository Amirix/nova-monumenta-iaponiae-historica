###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_ahachi.txt
# @param title c_ahachi
# @param title_name Ahachi
# @param d_mino
# @param k_chubu
# @param e_japan
#
###############################################################################

0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_mino"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1179.12.18={
	liege="e_japan"
	holder=120425 # Minamoto Norikiyo
}

1183.8.17={
	liege="d_mino"
	holder=2420102 # Toki Mitsunaga / Taira's escape to the west
}
1184.1.3={
	liege=0
	holder=0 # Siege of Hojujidono
}

##
## Kamakura shogunate
##
1185.4.25={
	liege="d_mino"
	holder=2370010 # Ouchi Koreyoshi
}
1220.1.1={
	holder=2370020 # Ouchi Korenobu
}
1221.7.6={ # Jokyu War
#	liege=
	holder=2186004 # Satomi Yoshinao
}
1240.1.1 ={
#	liege=
	holder=2186081 # Satomi Yoshinari
}
1250.1.1 ={
#	liege=
	holder=2186082 # Satomi Yoshisada
}
1270.1.1 ={
#	liege=
	holder=2186083 # Satomi Tamesada
}
1280.1.1 ={
#	liege=
	holder=2186084 # Satomi Tamemune
}
1310.1.1 ={
#	liege=
	holder=2186085 # Satomi Yoshikage
}
1330.1.1 ={
#	liege=
	holder=2186086 # Satomi Yoshimune
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1350.2.12={
	liege="d_mino"
	holder=2131015 # Ujiie Shigekuni
}
1360.1.1={
	holder=2131020 # Ujiie Sadakuni
}
1410.1.1={
	holder=2131025 # Ujiie Shigeoki
}
1440.1.1={
	holder=25755 # Ujiie Morikuni
}
1480.1.1={
	liege="d_mino"
	holder=25754 # Ujiie Yasukuni
}
1500.1.1={
	holder=25752 # Ujiie Yukikuni
}
1527.1.1={
	holder=25751 # Ujiie Yukitaka
}
1549.1.1={
	holder=25753 # Ujiie Bokuzen
}
1567.9.1={
	liege="d_owari" # Oda Nobunaga
#	holder=25753 # Ujiie Bokuzen
}
1567.10.1={
	liege="d_mino" # Oda Nobunaga
#	holder=25753 # Ujiie Bokuzen
}
1571.6.4={
	holder=25756 # Ujiie Naomasa
}
1582.6.21={ # Honno-ji Incident
	liege=0
#	holder=25756 # Ujiie Naomasa
}
1582.7.16={
	liege="k_northern_kinai"
#	holder=25756 # Ujiie Naomasa
}