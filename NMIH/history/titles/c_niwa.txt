###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_niwa.txt
# @param title c_niwa
# @param title_name Niwa
# @param d_owari
# @param k_chubu
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_owari"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

0870.1.1={
#	liege="d_owari"
	holder=2431000 # Mukuhashi Kuromaro {Dairyo}
}
0890.1.1={
#	liege="d_owari"	
	holder=2431003 # Mukuhashi Tsunenori {Dairyo}
}
0918.1.29={
#	liege="d_owari"	
	holder=2431005 # Mukuhashi Yoshiyo {Shoryo}
}
0926.1.1={
#	liege="d_owari"
	holder=2431006 # Mukuhashi Minami {Dairyo}
}
0955.1.1={
#	liege="d_owari"
	holder=0
}



1180.1.1={
	liege="d_owari"
	holder=2431087 # Tachikida Takamitsu
}

##
## Kamakura shogunate
##
1183.8.17={
	liege="k_western_kanto" # Taira's escape to the west
#	holder=2431087 # Tachikida Takamitsu
}
1216.1.1={
	liege=0
	holder=0
}

##
## the Ashikaga (Muromachi) shogunate
##
1400.6.1={
	liege="d_owari"
	holder=2320001 # Kai Yukinori
}
1402.1.1={
	holder=29104 # Oda Josho
}
1429.1.1={
	holder=29106 # Oda Norinaga
}
1431.1.1={
	holder=29107 # Oda Satohiro
}
1451.1.1={
	holder=29108 # Oda Toshihiro
}
1478.1.1={
	liege=0
#	holder=29108 # Oda Toshihiro
}
1479.1.1={
	liege="d_owari"
#	holder=29108 # Oda Toshihiro
}
1481.3.1={
	holder=29110 # Oda Tohiro
}
1504.1.1={
	holder=29112 # Oda Hirotaka
}
1517.2.16={
	holder=29102 # Oda Nobuyasu
}
1558.01.01={
	liege=0 #
	holder=29123 # Oda Nobukata
}
1559.03.01={
	liege="d_owari" # 29120 Oda Nobunaga
	holder=29135 # Oda Nobukiyo
}
1562.01.01={
	liege=0 #
	holder=29135 # Oda Nobukiyo
}
1564.08.07={
	liege="d_owari" # 29120 Oda Nobunaga
	holder=29120 # Oda Nobunaga
}
1570.9.1={
#	liege="d_owari" # 29120 Oda Nobunaga
	holder=29552 # Ikeda Tsuneoki
}
1580.9.1={
#	liege="d_owari"
	holder=29160 # Oda Nobutada
}
1581.12.1={
#	liege="d_owari"
	holder=29164 # Oda Nobufusa
}
1582.6.21={ # Honno-ji Incident
	liege="d_owari"
	holder=29184 # Oda Hidenobu
}
1582.7.16={
	liege="d_owari"
	holder=29161 # Oda Nobukatsu
}
