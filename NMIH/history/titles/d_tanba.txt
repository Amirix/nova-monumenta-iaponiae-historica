###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_tanba.txt
# @param title d_tanba
# @param title_name Tanba
# @param k_northern_kinai
#
###############################################################################
0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="e_tenno"
}
0841.6.6={
	holder=1004530 # Fujiwara Miyafusa {Tanba no Kami}
}
0845.2.24={
	holder=0 #  
}
0957.3.6={
	holder=1005250 # Fujiwara Moriyoshi {Tanba no Kami}
}
0961.2.18={
	holder=1003328 # Fujiwara Tamesuke {Tanba no Kami}
}
0962.3.5={
	holder=0 #  
}
0972.1.24={
	holder=110009 # Taira Sadamori
}
1020.3.3={
	holder=1003607 # Fujiwara Sukenari {Tanba no Kami}
}
1024.3.11={
	holder=0 #  
}
1108.11.25={
	holder=1003810 # Fujiwara Atsumune {Tanba no Kami}
}
1111.10.20={
	holder=0 #  
}
1111.12.4={
	holder=1006301 # Fujiwara Tadataka {Tanba no Kami}
}
1119.1.19={
	holder=1006701 # Fujiwara Ieyasu {Tanba no Kami}
}
1121.8.17={
	holder=0
}
1123.5.9={
	holder=1006010 # Fujiwara Akitaka {Tanba no Kami}
}
1127.3.10={
	holder=1001095 # Fujiwara Kinmichi {Tanba no Kami}
}
1132.1.1={
	holder=0 #  
}
1149.10.8={
	holder=1001068 # Fujiwara Narikane {Tanba no Kami}
}
1155.1.1={
	holder=0
}
1158.1.26={
	holder=1006480 # Ichijo Yoshiyasu {Tanba no Kami}
}
	
##
## the Taira clan Government
##
1179.12.18={
	liege="e_japan"
	holder=110141 # Taira Kiyokuni
}
1183.8.17={
	liege=0
	holder=0 # Taira's escape to the west
}

##
## Kamakura shogunate
##
1221.9.1={
	liege="e_japan"
	holder=131655 # Hojo Tokifusa
}
1222.10.1={
	holder=131680 # Hojo Tokimori
}
1277.6.11={
	holder=131978 # Hojo Tokikuni
}
1284.6.1={
	holder=132162 # Hojo Kanetoki
}
1288.2.1={
	holder=131985 # Hojo Morifusa
}
1297.1.1={
	holder=131949 # Hojo Munenobu
}
1302.1.1={
	holder=132093 # Hojo Sadaaki
}
1307.9.11={
	holder=131950 # Hojo Sadafusa
}
1309.12.2={
	holder=131921 # Hojo Tokiatsu
}
1314.12.1={
	holder=132114 # Hojo Koresada
}
1320.5.24={
	holder=132229 # Hojo Sadayuki
}
1330.7.11={
	holder=132083 # Hojo Tokimasu
}
1333.6.21={
	holder=0 #  
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1343.12.1={
	holder=2186256 # Yamana Tokiuji
}
1351.8.1={
	holder=0 #  
}
1363.9.1={
	holder=40036 # Ashikaga Naofuyu
}
1363.12.1={
	holder=2186256 # Yamana Tokiuji
}
1374.4.14={
	holder=50010 # Yamana Ujikiyo
}
1392.1.24={
	holder=40536 # Hosokawa Yorimoto
}
1397.6.2={
	holder=40538 # Hosokawa Mitsumoto
}
1426.11.15={
	holder=40541 # Hosokawa Mochimoto
}
1429.8.14={
	holder=40539 # Hosokawa Mochiyuki
}
1442.9.8={
	liege="e_japan"
	holder=40543 # Hosokawa Katsumoto
}
1473.6.6={
#	liege="e_japan"
	holder=40544 # Hosokawa Masamoto
}
1506.1.1={
	liege="k_kanrei"
	holder=40567 # Hosokawa Sumiyuki
}
1507.9.7={
#	liege="k_kanrei"
	holder=40501 # Hosokawa Sumimoto
}
1508.7.18={
#	liege="k_kanrei"
	holder=40566 # Hosokata Takakuni
}
1527.3.14={
#	liege="k_kanrei"
	holder=40500 # Hosokawa Harumoto
}
1549.7.18 ={
	liege="e_japan"
#	holder=40500 # Hosokawa Harumoto
}
1557.1.1={
	liege="k_southern_kinai"
	holder=45402 # Naito Munekatsu, De facto
}
1565.8.27={
	liege=0
	holder=0 # 
}
1576.2.10={
	liege=0
	holder=42086 # Hatano Hideharu
}

###
### The Oda government 
###
1579.6.24={
	liege="e_japan"
	holder=25632 # Akechi Mitsuhide
}
1582.6.21={
	liege="k_northern_kinai"
#	holder=25632 # Akechi Mitsuhide
}
1582.7.2={
	liege="k_northern_kinai"
	holder=29400 # Hashiba Hideyoshi
}
1582.7.16={
#	liege="k_northern_kinai"
	holder=29163 # Hashiba Hidekatsu
}
