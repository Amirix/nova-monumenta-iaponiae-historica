###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_shisuka.txt
# @param title d_shisuka
# @param title_name Shisuka (Kotan-kesh-un-kur)
# @param k_kita_ezochi
#
###############################################################################

1170.1.1={
	holder=155600
}
1210.1.1={
	holder=0
}

1270.1.1={
	holder=157221
}
1310.1.1={
	liege=0
	holder=0 # 
}

1450.1.1={
#	liege="d_shisuka"
	holder=155500
}
1510.1.1={
	holder=155501
}
1550.1.1={
	holder=155502
}