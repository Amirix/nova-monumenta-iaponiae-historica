1000.1.1={
law = agnatic_succession
law = succ_primogeniture
}
1349.10.21={
#	liege= # ToDo
	holder=40067 # Ashikaga Motouji
}
1367.5.25={
#	liege= # ToDo
	holder=40066 # Ashikaga Ujimitsu
}
1398.12.12={
#	liege= # ToDo
	holder=40064 # Ashikaga Mitsukane
}
1409.9.1={
#	liege= # ToDo
	holder=40061 # Ashikaga Mochiuji
}
1439.3.24={
#	liege= # ToDo
	holder=40006 # Ashikaga Yoshinori
}
1441.7.12={
#	liege= # ToDo
	holder=40007 # Ashikaga Yoshikatsu
}
1443.8.16={
#	liege= # ToDo
	holder=40008 # Ashikaga Yoshimasa
}
1447.3.1={
#	liege= # ToDo
	holder=40059 # Ashikaga Shigeuji
}
1455.7.30={
	name = KOGA_KUBO
	adjective = KOGA_KUBO_adj
	liege=0
	holder=40059 # Ashikaga Shigeuji
}
1497.10.25={
	holder=40058
}
1512.1.1={
	holder=40050
}
1535.11.3={
	holder=40052
}
1546.5.19={
	holder=0 # After the Battle of Kawagoe, the Kamakura Kubo lost his power
}