###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name d_awa.txt
# @param title d_awa
# @param title_name Awa
# @param k_shikoku
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="e_tenno"
}
##
## the Taira clan Government
##
1180.1.1={
	liege = "k_sanyo"
	holder=2690001 # Awata Shigeyoshi
}
1181.3.20={
	liege = "e_japan"
}
1183.8.17={
	liege= "k_western_chugoku"
}
1185.4.25={
	liege=0
	holder=0 # Battle of Dan-no-ura
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1336.1.1={
	liege="e_japan"
}
1392.3.25={
	holder = 40524 # Hosokawa Yoshiyuki
}
1402.1.1={
	holder = 40525 # Hosokawa Mitsuhisa
}
1430.10.15={
	holder = 40526 # Hosokawa Mochitsune
}
1450.1.28={
	liege="k_kanrei"
	holder = 40528 # Hosokawa Shigeyuki
}
1479.1.1={
	holder = 40530 # Hosokawa Masayuki
}
1488.9.4={
	holder = 40529 # Hosokawa Yoshiharu
}
1495.1.17={
	holder = 40532 # Hosokawa Yukimochi
}
1508.7.28={
	liege="k_southern_kinai"
	holder=40501 # Hosokawa Sumimoto
}
1520.6.24={
	holder=40500 # Hosokawa Harumoto
}
1527.3.14={
	liege="k_kanrei"
#	holder=40500 # Hosokawa Harumoto
}
1534.1.1={
#	liege="k_kanrei"
	holder=40534 # Hosokawa Mochitaka
}
1548.10.28={
	liege="k_southern_kinai"
	holder=59008 # Miyoshi Koretora, De facto
}
1562.4.8={
#	liege="k_southern_kinai"
	holder=59018 # Miyoshi Nagaharu
}
1567.5.24={
	liege=0
#	holder=59018 # Miyoshi Nagaharu
}
1577.4.16={
#	liege=0
	holder=59019 # Miyoshi Masayasu
}
1582.10.7={
#	liege=0
	holder=59104 # Chosokabe Motochika
}