###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kikuchi.txt
# @param title c_kikuchi
# @param title_name Kikuchi
# @param d_higo
# @param k_southern_kyushu
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
220.1.1={
	holder=41352 # Takekukuchi a.k.a Kukuchihiko
}
250.1.1={
	holder=41353 # Takemorokumi
}
290.1.1={
	holder=41357 # Takekunitake
}
330.1.1={
	holder=41358 # Takeoshihiko
}
350.1.1={
	holder=41359 # Takekawana
}
390.1.1={
	holder=41360 # Takekuwae
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
1050.1.1={
	holder=2780600 # Kikuchi Masanori
}
1064.10.5={
	holder=2780605 # Kikuchi Noritaka
}
1101.1.1={
	holder=2780610 # Kikuchi Tsunetaka
}
1105.1.1={
	holder=2780620 # Kikuchi Tsuneyori
}
1123.1.1={
	holder=2780630 # Kikuchi Tsunemune
}
1145.1.1={
	holder=2780640 # Kikuchi Tsunenao
}
1170.1.1={
	liege="d_higo"
	holder=2780645 # Kikuchi Takanao
}
##
## Kamakura shogunate
##
1185.4.25={
	liege=0
	holder=2780651 # Kikuchi Takasada  # Battle of Dan-no-ura
}
1222.2.13={
	holder=2780670 # Kikuchi Yoshitaka
}
1258.9.8={
	holder=2780675 # Kikuchi Takayasu
}
1276.3.28={
	holder=2780680 # Kikuchi Takefusa
}
1285.5.2={
	holder=2780685 # Kikuchi Takamori
}
1293.1.1={
	holder=2780690 # Kikuchi Tokitaka
}
1304.10.12={
	holder=66032 # Kikuchi Taketoki
}
1333.4.27={
	holder=66023 # Kikuchi Shigetoki
}
1338.1.1={
	holder=66024 # Kikuchi Takehito
}
1343.1.1={
	holder=66022 # Kikuchi Takemitsu
}
1373.12.29={
	holder=66021 # Kikuchi Takemasa
}
1374.7.6={
	holder=66020 # Kikuchi Taketomo
}
1407.4.25={
	holder=66019 # Kikuchi Kanetomo
}
1431.1.1={
	holder=66007 # Kikuchi Mochitomo
}
1446.8.20={
	holder=66006 # Kikuchi Tamekuni
}
1488.11.26={
	holder=66005 # Kikuchi Shigetomo
}
1493.12.7={
	holder=66004 # Kikuchi Yoshiyuki
}
1504.3.1={
	holder=66003 # Kikuchi Masataka
}
1507.1.1={
	holder=66203 # Kikuchi Taketsune
}
1513.1.1={
	holder=66001 # Kikuchi Takekane
}
1532.1.1={
	holder=66000 # Kikuchi Yoshitake
}
1550.1.1={
	holder=64002 # Otomo Yoshishige
}
1578.1.1={
	holder=68320 # Ryuzoji Takanobu
}