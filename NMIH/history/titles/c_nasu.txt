###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_nasu.txt
# @param title c_nasu
# @param title_name Nasu
# @param d_shimotsuke
# @param k_western_kanto
# @param e_japan
#
###############################################################################

0701.1.1={
	# Taiho Code
	law=agnatic_succession
	law=succ_primogeniture
	liege="d_shimotsuke"
}

#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

1165.1.1={
	liege="d_shimotsuke"
	holder=2140100 # Nasu Suketaka
}

1180.1.1={
	liege=0
#	holder=2140100 # Nasu Suketaka
}

##
## Kamakura shogunate
##
1180.10.26={
	liege="k_western_kanto" # Minamoto Yoritomo
#	holder=2140100 # Nasu Suketaka / Yoritomo's conquest of Kamakura
}
1185.1.1={
#	liege="k_western_kanto" # Minamoto Yoritomo
	holder=2140117 # Nasu Munetaka
}
1189.9.19={
#	liege="k_western_kanto" # Minamoto Yoritomo
	holder=2140110 # Nasu Sukeyuki
}
1192.7.12={
	liege="e_japan" # Minamoto Yoritomo
#	holder=2140110 # Nasu Sukeyuki
}
1193.1.1={
#	liege="d_shimotsuke"
	holder=2140120 # Nasu Mitsusuke
}
1237.1.1={
#	liege="d_shimotsuke"
	holder=2140130 # Nasu Sukemura
}
1268.1.1={
#	liege="d_shimotsuke"
	holder=2140140 # Nasu Sukeie
}
1275.1.1={
#	liege="d_shimotsuke"
	holder=2140150 # Nasu Suketada
}
1300.1.1={
#	liege="d_shimotsuke"
	holder=2140160 # Nasu Sukefuji
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1355.1.1={
#	liege="d_shimotsuke"
	holder=2140180 # Nasu Sukeuji
}
1408.1.1={
	liege=0
	holder=2140190 # Nasu Sukeyuki
}
1428.1.1={
	holder=2140192 # Nasu Sukeshige
}
1431.1.1={
	holder=11010
}
1467.10.11={
	holder=11007
}
1510.1.1={
	holder=11001
}
1546.8.19={
	holder=11003 # Nasu Takasuke
}
1551.2.27={
	holder=11005 # Nasu Sukekuni
}
1583.4.3={
	holder=11019 # Nasu Sukeharu
}