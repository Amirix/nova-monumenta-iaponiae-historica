###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_hida_ono.txt
# @param title c_hida_ono
# @param title_name Hida=Ono
# @param d_hida
# @param k_chubu
# @param e_japan
#
###############################################################################

0701.1.1={
	# Taiho Code
	law = agnatic_succession
	law = succ_primogeniture
	liege="d_hida"
}

#0764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}

##
## the Taira clan Government
##
1180.1.1={
	liege="d_hida"
	holder=131579 # Ito Kagekiyo
}

1183.6.22={
	liege="k_hokuriku"
	holder=120084 # Minamoto Yoshinaka / Battle of Shinohara
}
1183.8.17={
	liege="e_japan"
	holder=120084 # Minamoto Yoshinaka / Taira's escape to the west
}

1184.3.4={
	liege=0
	holder=0 # Battle of Awazu
}

###
### Ashikaga shogunate [Muromachi shogunate]
###
1334.1.1={
#	liege=
	holder=1003176 # Anekoji Takamoto
}
1358.4.10={
#	liege=
	holder=1003178 # Anekoji Ietsuna
}
1389.1.1={
#	liege=
	holder=1003179 # Anekoji Yoritoki
}
1411.9.20={
#	liege=
	holder=1003181 # Anekoji Morotoki
}
1440.1.1={
#	liege=
	holder=1003185 # Anekoji Mochitoki
}
1460.1.1={
	liege=0
	holder=1003187 # Anekoji Katsutoki
}
1481.1.1={
	holder=1003202 # Anekoji Mototsuna
}
1504.6.5={
	holder=1003205 # Anekoji Naritsugu
}
1518.7.7={
	holder=1003208 # Anekoji Naritoshi
}
1527.10.26={
	liege=0
	holder=25101 # Miki Naoyori
}
1554.7.13={
	holder=25103 # Miki Yoshiyori
}
1570.2.1={
	liege="k_chubu"
	holder=25109 # Anegakoji(Miki) Yoritsuna
}
1582.6.21={ # Honno-ji Incident
	liege=0
#	holder=25109 # Anegakoji(Miki) Yoritsuna
}
1582.11.22={
	liege="d_hida"
#	holder=25109 # Anegakoji(Miki) Yoritsuna
}

