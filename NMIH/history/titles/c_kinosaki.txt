###############################################################################
#
# Crusader Kings II (developed by Paradox Development Studio) MOD
# Nova Monumenta Iaponiae Historica (NMIH)
#
# Title history data file.
# 
# @param file_name c_kinosaki.txt
# @param title c_kinosaki
# @param title_name Kinosaki
# @param d_tajima
# @param k_sanin
#
###############################################################################

100.1.1={
law = agnatic_succession
law = succ_primogeniture
}
#764.11.6={
#	holder=41023 # Empress Koken/Shotoku
#}
##
## the Taira clan Government
##
1179.12.18={
	liege="d_tajima"
	holder=110031 # Taira Tsunemori
}
1183.8.17={
	liege=0
	holder=0 # Taira's escape to the west
}
##
## Kamakura shogunate
##


###
### Ashikaga shogunate [Muromachi shogunate]
###
1433.1.1={
	holder=50039 # Yamana Mochitoyo
}
1454.1.1={
	holder=50040 # Yamana Noritoyo
}
1458.1.1={
	holder=50039 # Yamana Mochitoyo
}
1472.8.1={
	holder=50044 # Yamana Masatoyo
}
1499.3.13={
	holder=50045 # Yamana Munetoyo
}
1512.1.1={
	holder=50046 # Yamana Nobutoyo
}
1528.3.4={
	holder = 50005 # Yamana Suketoyo
}
1569.8.1={
	liege="k_chubu"
	holder=29400 # Hashiba Hideyoshi
}
1570.1.1={
	holder = 50005 # Yamana Suketoyo
}
1575.5.1={
	liege=0
}
1580.7.2={
	holder=29401 # Hashiba Hidenaga
	liege="d_harima"
}