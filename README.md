
# Nova Monumenta Iaponiae Historica Mod


![NMIH mod Title iamge](https://gitlab.com/nmihteam/nova-monumenta-iaponiae-historica/wikis/images/NMIH_GitLab_README_Title.svg)

**[Nova Monumenta Iaponiae Historica](https://forum.paradoxplaza.com/forum/index.php?threads/nova-monumenta-iaponiae-historica.656239/)** Mod (a.k.a. **NMIH** Mod) is a [Crusader Kings 2](https://www.paradoxplaza.com/crusader-kings-ii/CKCK02GSK-MASTER.html) Mod about Japanese medieval era, called [Sengoku Jidai](https://en.wikipedia.org/wiki/Sengoku_period).

## Description

This mod will contain following features.

* Two Genpei scenarios (1180) and the Sengoku era where you can start the game on any date between 1493 and 1582.
* Map based on the historical provinces. Similar to the Sengoku map, but more historically accurate for the 16th century.
* historical settlements such as castles, cities and manors. The starting and max. number of baronies in each province is based on surveys in the 16th and 19th centuries.
* Historical buddhist revolters such as Ikko Shu and Hokke Shu.
* Interaction with the Imperial Court and the Shogunate.
* Events with Westerners.
* Japanese localization of vanilla features.
* Portraits of historical pictures.
* Original 3D unit models.

## Requirement

1. :u6709: *[Crusader Kings II](https://www.paradoxplaza.com/crusader-kings-ii/CKCK02GSK-MASTER.html)*
2. :u6709: *[Rajas of India](https://www.paradoxplaza.com/crusader-kings-ii-rajas-of-india/CKCK02ESK0000005.html) DLC*

## Installation

### Steam Workshop install

1.  access NMIH Mod Steam Workshop page [[Link](http://steamcommunity.com/sharedfiles/filedetails/?id=333442855)].
1.  Click **Subscribe**.

### Manual install

1.  access Paradox Interactive Forums [NMIH mod Thread](https://forum.paradoxplaza.com/forum/index.php?threads/nova-monumenta-iaponiae-historica.656239/) [[Link](https://forum.paradoxplaza.com/forum/index.php?threads/nova-monumenta-iaponiae-historica.656239/)].
1.  Click *mediafire.com link* under subentry "**Alterantive Manual Download**".
1.  Download zip file from MediaFire.

<!--## Demo-->

## NMIH Mod VS. Other Games

||Paradox/[Sengoku](http://store.steampowered.com/app/73210/Sengoku/)|KOEI TECMO/[Nobunaga's Ambition: Taishi](http://store.steampowered.com/app/628890/Nobunagas_Ambition_Taishi/)|NMIH Mod|
|:--|:--|:--|:--|
|Scenario|2|max 15|est. 32048|
|Provinces|360||415|
|Characters|1028|over 2000|19173|
|Style|Single-player, Multi-player|Single-player|Single-player, Multi-player|
|Platform|PC|PC, PS4, Nintendo Switch, Android, iOS|PC, Mac|
|Release|9/16/2011|11/30/2017|10/29/2014|
|Language|English, French, German, Spanish, Japanese*|Japanese, Traditional Chinese, English|English, Spanish|
|Last Update|v1.03 - 10/21/2011|6/28/2018|v0.9.0 - 10/22/2019|

\* CyberFront Corporation edition
<!--
## Usage

## Contribution
-->
## Licence

* [Rules for User Made Mods & Edits](https://forum.paradoxplaza.com/forum/index.php?threads/rules-for-user-made-mods-edits.779854/)
* :copyright: Paradox Interactive. All rights reserved.

## Author
### NMIH Team Members
* awak(1180) [![twitter](https://img.shields.io/twitter/follow/awak1180.svg?style=social)](https://twitter.com/awak1180)
* aycone
* chatnoir17  [![twitter](https://img.shields.io/twitter/follow/NMIHCK2.svg?style=social)](https://twitter.com/NMIHCK2) : *The current lead maintainer*
* hamane [![twitter](https://img.shields.io/twitter/follow/augsburg14.svg?style=social)](https://twitter.com/augsburg14)
* kaipain
* Khorney
* kk0192
* kzmax
* Lunawolf242
* Remitonov
* seishinouya
* Yamana Tokiuji [![twitter](https://img.shields.io/twitter/follow/tokiuji.svg?style=social)](https://twitter.com/tokiuji)
* ys_sthlm [![twitter](https://img.shields.io/twitter/follow/ys_sthlm.svg?style=social)](https://twitter.com/ys_sthlm)
* Zabara [![twitter](https://img.shields.io/twitter/follow/zabarann.svg?style=social)](https://twitter.com/zabarann) : *Character Portrait*

### Collaborator
* [Miroslav Milosevic](https://forum.paradoxplaza.com/forum/index.php?members/miroslav.1041188/): *3D models*
* SchwarzKatze: *suggestions and bug reports*

### Special Thanks
* [Crusader Kings2 Wiki Editor](http://ck2.paradwiki.org/index.php?%E6%88%A6%E5%9B%BDMOD%EF%BC%9ANMIH): *Settlement research*
* Camara: *map adjustment for patch 1.10-*
* Your Personal Castle Team (=F4D=Nuubialainen & dangitsdaaaaang)

---


Nova Monumenta Iaponiae Historica Mod
==== 
![NMIH mod Title iamge](https://gitlab.com/nmihteam/nova-monumenta-iaponiae-historica/wikis/images/NMIH_GitLab_README_Title.svg)

この **[Nova Monumenta Iaponiae Historica](https://forum.paradoxplaza.com/forum/index.php?threads/nova-monumenta-iaponiae-historica.656239/)** MOD（略して **NMIH** MOD）は、[Crusader Kings 2](https://www.paradoxplaza.com/crusader-kings-ii/CKCK02GSK-MASTER.html)で[戦国時代](https://ja.wikipedia.org/wiki/%E6%88%A6%E5%9B%BD%E6%99%82%E4%BB%A3_(%E6%97%A5%E6%9C%AC))を含む日本の中世史を再現するMODです。

## 説明
以下の項目は既に実装済みです

* [源平合戦](https://ja.wikipedia.org/wiki/%E6%B2%BB%E6%89%BF%E3%83%BB%E5%AF%BF%E6%B0%B8%E3%81%AE%E4%B9%B1)シナリオおよび[明応の政変](https://ja.wikipedia.org/wiki/%E6%98%8E%E5%BF%9C%E3%81%AE%E6%94%BF%E5%A4%89)（1493年）から[本能寺の変](https://ja.wikipedia.org/wiki/%E6%9C%AC%E8%83%BD%E5%AF%BA%E3%81%AE%E5%A4%89)が起こる（1582年）までの戦国時代。後者では「全ての年月日」でプレイが開始可能
* 樺太・千島から琉球までを含むマップ
* 史実の石高や地名に基づくセツルメント
* 膨大な数の史実の人物
* 法華宗と一向宗
* 南蛮貿易、鉄砲伝来、キリスト教布教イベント
* 当時の人物に基づくポートレート
* 五街道その他の交易路
* 独自の3Dユニットモデル

## 依存関係

NMIH MODを利用するには、以下のソフトウェア・ダウンロードコンテンツ（DLC）・MODが必要です

1. :u6709: *[Crusader Kings II](https://www.paradoxplaza.com/crusader-kings-ii/CKCK02GSK-MASTER.html)*
2. :u6709: *[Rajas of India](https://www.paradoxplaza.com/crusader-kings-ii-rajas-of-india/CKCK02ESK0000005.html) DLC*

## インストール方法
### Steam Workshopから導入する手順

※ あらかじめ[Stearmクライアント](http://store.steampowered.com/about/)をインストールしておく必要があります。もちろん「Crusader Kings II」本体と「Rajas of India」DLCも必要です。

1.  Stearmから「Nova Monumenta Iaponiae Historica」MODのSteam ワークショップにアクセスします [[リンク](http://steamcommunity.com/sharedfiles/filedetails/?id=333442855)]。
1.  **:heavy_plus_sign: サブスクライブ** ボタンをクリックします。自動的にダウンロードされます。

### 手動インストール手順

※ 手動インストールするには、[PARADOXアカウント](https://forum.paradoxplaza.com/forum/index.php?register)をサインアップしておく必要があります。

1. パラドックス公式フォーラムにある[NMIH MOD スレッド](https://forum.paradoxplaza.com/forum/index.php?threads/nova-monumenta-iaponiae-historica.656239/) [[リンク](https://forum.paradoxplaza.com/forum/index.php?threads/nova-monumenta-iaponiae-historica.656239/)]をブラウザで開きます。
1. スレッド1ページ目にある"**Alterantive Manual Download**"と書かれてあるところ下にある *mediafire.com* へのリンクをクリックします。
1. アップローダサイトへ移動するのでZIPファイルをダウンロードします。
1. ダウンロードしたZIPファイルをMODフォルダに展開します。

## 比較

||パラドックス/[Sengoku](http://store.steampowered.com/app/73210/Sengoku/)|コーエーテクモ/[信長の野望・大志](http://store.steampowered.com/app/628890/Nobunagas_Ambition_Taishi/)|NMIH Mod|
|:--|:--|:--|:--|
|シナリオ数|2|最大15|約32048|
|プロヴィンス数|360||415|
|キャラクター数|1028人|2000人以上|19173人|
|プレイスタイル|シングル、マルチ|シングル|シングル、マルチ|
|プラットフォーム|PC|PC, PS4, 任天堂 Switch, Android, iOS|PC, Mac|
|公開日時|2011/9/16|2017/11/30|2014/10/29|
|対応言語|英語、フランス語、ドイツ語、スペイン語、日本語*|日本語、中国語（繁体字）、英語|英語、スペイン語|
|最終更新日|2011/10/21 - v1.03|2018/11/30|2019/10/22 - v0.9.0|

\* サイバーフロント社発売の「戦国」

## デモ

## 使い方

## 貢献するには

1. 高評価とお気に入りへの追加をよろしくお願いします
1. 不具合を発見したらぜひご報告ください
1. 協力者募集中です。以下の作業に関心のある方のご参加をお待ちしています
   - イベント作成（既存イベントの日本風アレンジも）
   - バニラのシステムを日本史用に改変
   - グラフィクスの強化
   - キャラクターや年代など、歴史データの追加
   - 日本語化サポート

こちらもご覧ください

* [chatnoir17のツイッター](https://twitter.com/CK2NMIH)
* [NMIH Discord チャンネル](https://discord.gg/ejR5svN) (日本語/英語)
* [パラドックス公式フォーラムのスレッド](https://forum.paradoxplaza.com)

## ライセンス

* このMODは[Rules for User Made Mods & Edits](https://forum.paradoxplaza.com/forum/index.php?threads/rules-for-user-made-mods-edits.779854/)ページにある規定に準じます。
* :copyright: Paradox Interactive. All rights reserved.

## 製作者
### NMIHチーム
* awak(1180) [![twitter](https://img.shields.io/twitter/follow/awak1180.svg?style=social)](https://twitter.com/awak1180)
* aycone
* chatnoir17  [![twitter](https://img.shields.io/twitter/follow/NMIHCK2.svg?style=social)](https://twitter.com/NMIHCK2) : *チームリーダ*
* hamane [![twitter](https://img.shields.io/twitter/follow/augsburg14.svg?style=social)](https://twitter.com/augsburg14)
* kaipain
* Khorney
* kk0192
* kzmax
* Lunawolf242
* Remitonov
* seishinouya
* Yamana Tokiuji [![twitter](https://img.shields.io/twitter/follow/tokiuji.svg?style=social)](https://twitter.com/tokiuji)
* ys_sthlm [![twitter](https://img.shields.io/twitter/follow/ys_sthlm.svg?style=social)](https://twitter.com/ys_sthlm)
* Zabara [![twitter](https://img.shields.io/twitter/follow/zabarann.svg?style=social)](https://twitter.com/zabarann) : *キャラクター肖像画*

### 協力者
* Camara: パッチ 1.10- 対応
* [Miroslav Milosevic](https://forum.paradoxplaza.com/forum/index.php?members/miroslav.1041188/): *3Dモデル*
* SchwarzKatze - 提案＆バグレポート
* Mitsusonator: 北海道文化建物
* Drakon: 英語の改善
* Jo: スペイン語
* The Legendary Carmine: 英語の改善
* Grand Historian: 英語の改善

### スペシャルサンクス
* [Crusader Kings2 Wiki Editor](http://ck2.paradwiki.org/index.php?%E6%88%A6%E5%9B%BDMOD%EF%BC%9ANMIH): *セツルメント調査*
* Camara: *パッチ1.10以降のためのマップ調整*
* Your Personal Castle Team (=F4D=Nuubialainen & dangitsdaaaaang)
* AGOT Modチーム